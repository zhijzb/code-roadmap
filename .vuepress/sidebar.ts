import {SidebarConfig4Multiple} from "vuepress/config";

import RoadmapSideBar from "./sidebars/RoadmapSideBar";
import SpringMVCSideBar from "./sidebars/SpringMVCSideBar";
import SpringBootSideBar from "./sidebars/SpringBootSideBar";
import LinuxSideBar from "./sidebars/LinuxSideBar";
import DockerDevOpsSideBar from "./sidebars/Docker&DevOpsSideBar";
// @ts-ignore
export default {
    "/学习路线/": RoadmapSideBar,
    "/Linux/": LinuxSideBar,
    "/SpringMVC/": SpringMVCSideBar,
    "/SpringBoot/": SpringBootSideBar,
    "/容器化&DevOps/": DockerDevOpsSideBar,
    // 降级，默认根据文章标题渲染侧边栏
    "/": "auto",
} as SidebarConfig4Multiple;
