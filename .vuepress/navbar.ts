import {NavItem} from "vuepress/config";

export default [
    {
        text: "首页",
        link: '/'
    },
    {
        text: "学习路线",
        link: '/学习路线/'
    },
    {
        text: "Linux",
        link: '/Linux/'
    },
    {
        text: "SpringMVC",
        link: '/SpringMVC/'
    },
    {
        text: "SpringBoot",
        link: '/SpringBoot/'
    },
    {
        text: "容器化&DevOps",
        link: '/容器化&DevOps/'
    },

] as NavItem[];
