export default [
    "",
    {
        title: "Linux",
        collapsable: false,
        children: [
            "Linux 基础知识、常用命令.md",
            "Linux 管理 - Vi 编辑器、权限管理、系统服务管理、网络管理.md",
            "Linux 应用 - 软件包管理、软件安装.md",
        ],
    },
];
