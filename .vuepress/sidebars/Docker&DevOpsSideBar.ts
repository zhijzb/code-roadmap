export default [
    "",
    {
        title: "容器化&DevOps",
        collapsable: false,
        children: [
            "安装Docker.md",
            "安装Docker-Compose.md",
            "Docker 实战.md",
        ],
    },
];
