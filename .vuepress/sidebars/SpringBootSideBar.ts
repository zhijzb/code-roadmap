export default [
    "",
    {
        title: "SpringBoot",
        collapsable: false,
        children: [
            "SpringBoot - 入门_配置文件_YAML.md",
            "SpringBoot - 页面模版_Thymeleaf.md",
            "SpringBoot - MyBatis.md",
            "SpringBoot - SpringMVC.md"
        ],
    },
];
