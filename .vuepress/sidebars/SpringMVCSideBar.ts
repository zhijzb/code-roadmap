export default [
  "",
  {
    title: "SpringMVC",
    collapsable: false,
    children: [
      "SpringMVC - 基础.md",
      "SpringMVC - 异常处理_拦截器.md",
      "SpringMVC - 特殊的请求参数.md",
      "SpringMVC - 返回值_自定义.md",
    ],
  },
];
