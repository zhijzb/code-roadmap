## Vi 编辑器

### **Vi 编辑器的三种模式**：

*   **编辑模式（命令模式）**：所有输入的字符都会理解为编辑整个文档的操作，默认为编辑模式
*   **输入模式**：大部分输入的字符都会理解为输入文本字符
*   **末行模式**：可以输入很多文件管理命令，例如保存退出

模式之间的相互转换：

*   编辑 —> 输入  
    `i`：在[光标](https://so.csdn.net/so/search?q=%E5%85%89%E6%A0%87&spm=1001.2101.3001.7020)所在字符前开始插入  
    `a`：在光标所在字符后开始插入  
    `o`：在光标所在行的下面另起一新行插入  
    `s`：删除光标所在的字符并开始插入
    
    `I`：在光标所在行的行首开始插，如果行首有空格则在空格之后插入  
    `A`：在光标所在你行的行尾开始插入  
    `O`：在光标所在行的上面另起一行开始插入  
    `S`：删除光标所在行并开始插入
    
*   输入 / 末行—> 编辑：ESC
    
*   编辑 —> 末行：:  
    ![](http://121.41.87.138:9001/picgo/img/4556a1054da84c399772aa05dc9b117d.png)

打开文件：`vi hello.txt`  
关闭文件：在末行模式下输入 `wq`（保存退出） 或 `q!`（强制退出）

### 光标移动

```shell
逐个字符移动:
	h: 左移
	l: 右移
	j: 下移(记住'下贱','下j')
	k: 上移

行内跳转:
	0: 跳转到行首
	$: 跳转到行尾

行间跳转：
	:'num': 跳转到num行
	G: 跳转到最后一行
	gg: 跳转到第一行

翻屏: 
	ctrl + f: 向下翻页
	ctrl + b: 向上翻页
```

在末行模式下执行以下代码可以在编辑器内显示行号：

```shell
set nu
set number
```

### 文本操作指令

```shell
撤回:
	u: 撤回上一步
	ctrl + R: 前进下一步

删除: 
	dd: 删除光标所在行
	3dd: 从光标所在行开始删除3行
	:1,3d: 删除第1行到第3行
	:1,$d: 删除第1行到最后一行
	x: 删除当前光标的字符
	X: 删除当前光标前面的字符

复制粘贴:
	yy: 复制光标所在行
	2yy: 从光标所在行开始复制2行
	D: 从光标开始剪切到行末
	p: 粘贴复制的内容

文本查找: 
	/pattern: 从前往后查找
	?pattern: 从后往前查找
	n: 下一个匹配的字符串
	N: 上一个匹配的字符串

文本替换: 注意下面命令中的's'容易被忽略,代表替换的意思
	:1,2s/pattern/string/gi # 第1行到第2行的内容进行替换
	:1,$s/pattern/string/gi # 第1行到最后一行的内容进行替换
	:%s/pattern/string/gi # 所有行的内容忽略大小写进行替换
		% - 所有行
		s - 替换
		g - 全局替换,不加这个只会替换每行找到的第一个
		i - 忽略大小写
```

权限管理
----

**权限**：用来描述用户对资源的访问能力，包括读权限 (r)、写权限 (w)、执行权限 (x)

> root 用户不受权限的约束

### 用户管理

```shell
# 创建用户
useradd [用户名]

# 创建用户并分配一个组
useradd -G [组名] [用户名]

# 添加用户组
groupadd [组名]

# 修改用户组属性
usermod -G [组名] [用户名]

# 删除用户
userdel [用户名]
	-f: 强制删除用户，即使用户已登录 
	-r: 删除与用户相关的所有文件

# 删除用户组
groupdel [组名]
```

```shell
# 查看系统用户
cat /etc/passwd

# 查看系统用户组
cat /etc/group
```

### * 文件基本权限

**拥有权限的对象**：

*   `u` - 用户（文件所有者，属主）
*   `g` - 用户组（属组）
*   `o` - 其他用户（既不是属主，也不是属组）

**文件的权限**：

*   `r` - 可读，可以执行类似 cat 命令的操作
*   `w` - 可写，可以编辑或者删除此文件
*   `x` - 可执行

**目录的权限**：

*   `r` - 查询目录下面的文件数据列表
*   `w` - 创建目录 / 文件，删除目录 / 文件
*   `x` - 具有进入目录的权限，例如执行 cd

**文件权限解释**  
![](http://121.41.87.138:9001/picgo/img/cc33fb13227f46ffbf441b3495e35360.png)

**权限的数字表示**  
![](https://img-blog.csdnimg.cn/57ca7b852acd4bc2a672606be29c0b87.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_1,color_FFFFFF,t_70,g_se,x_16)

```shell
权限的数字表示
r		4
w		2
x		1

rwx
111 --> 7  --> rwx
110 --> 6  --> rw-
100 --> 4  --> r--

示例:
rwx r-x r-x
111 101 101
 7	 5   5
```

```shell
-rwxr-x---	700
-rwxr-xr-x	755
-rw-r--r--	644
-rwx------	600
```

> 常用权限：
>
> 777 - 所有权限
>
> 774 - 其他用户只读

**修改权限的方式**

```shell
chmod [选项] 模式 文件名
	选项
		-R		递归
	模式
		[guoa] [+-=] [rwx]
		[mode=421]

示例: chmod u+x
```

```shell
# 给当前用户添加指定文件的x执行权限
chmod u+x 文件

# 给该文件用户组,其他人添加指定文件的w写的权限
chmod g+w,o+w 文件

# 给该文件的当前用户,当前组,其他人, 添加rwx可读可写可执行的权限
chmod a=rwx 文件
chmod 777 文件

# 给该文件用户组-可读可写可执行,其他用户-可读可执行
chmod g+wx,o+x 文件
chmod 675 文件

# 给所有用户添加该文件的可读权限
chmod a+w 文件
```

```shell
# 修改文件的所有者
chown 用户名 文件名

# 修改文件的所属组
chgrp 组名 文件名
```

### sudo

sudo 的操作对象是系统命令

通过 `visudo` 编辑 /etc/sudoers 文件中的内容：（使用 sudo 的前提）  
![](http://121.41.87.138:9001/picgo/img/4b80427b8b5f4209988b2fc523dee7a3.png)  
表示 zhangsan 用户可以使用 sudo 执行超级用户才能执行的命令

```shell
# 假设 hello.txt 的权限是: ----------.
cat hello.txt # 无权限
sudo hello.txt # 可以查看,第一次使用sudo需要输入密码确认身份
```

系统服务管理 - systemctl
------------------

> systemctl 的全名是：system control

```shell
启动服务: systemctl start <服务名>
关闭服务: systemctl stop <服务名>
重启服务: systemctl restart <服务名>
查看服务状态: systemctl status <服务名>
添加开机启动项: systemctl enable <服务名>
禁止开机启动项: systemctl disable <服务名>
查看开机启动项: sysetmctl list-unit-files
```

进程查看 ps：用于报告当前系统的进程状态，可以搭配 kill 随时中断不必要的程序

```shell
# 查看所有进程信息,并格式化显示
ps -ef
	显示
	UID		用户ID
	PID		进程ID
	PPID	父进程ID
	C		CPU占用率
	STIME	开始时间
	TTY		开始此进程的TTY---终端设备
	TIME	此进程运行的总时间
	CMD 	命令名

# 显示网络相关信息, 也可查看 PID
netstat -ntlp

# 杀死进程
kill -9 pid
```

网络管理
----

网络相关知识可以参考 [《网络协议从入门到底层原理》笔记](https://blog.csdn.net/weixin_43734095/article/details/112449428)

### 网络的基本概念

*   **IP 地址**：在网络通信中主机的标识符（好比手机号码）
*   **mac 地址**：主机的物理网卡的唯一标识符（好比身份证号码）
*   **子网掩码**：用于区分主机的 IP 地址中的网络地址和主机地址，并确定该主机的 IP 地址的网段
*   **网关**：就是一个网络中的主机连接到另一个网络的主机的关口
*   **DNS**：域名解析服务器，把域名解析成对应的 IP 地址

查看本机地址：

```shell
ip addr
```

![](https://img-blog.csdnimg.cn/22823da92c3d446991a782f1de5e8f09.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

1.  机器 A 给 机器 B 发送消息
2.  检查是否在同一网段，通过目标 IP 192.168.48.129 和子网掩码 255.255.255.0 进行与运算
3.  在同一网段（网络号：192.168.48.0）直接使用 MAC 地址进行消息通信

![](https://img-blog.csdnimg.cn/d64e03e8f7054edfa259a72c9e9a5845.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

1.  机器 A 给 机器 D 发送消息
2.  检查是否在同一网段，通过目标 IP 192.168.38.129 和子网掩码 255.255.255.0 进行与运算
3.  不在同一网段（网络号 192.168.48.0 和 192.168.38.0）把数据发送到网关 (192.168.48.1)
4.  网关接收到数据，根据路由表信息找到对应的目标网关 (192.168.3.1)
5.  目标网关接收到数据，判断是否在同一个网段
6.  如果在同一个网段，使用 MAC 地址进行数据发送
7.  如果不在同一个网段，发送到下一个路由

### ifcfg-eth 概念与配置

**ifcfg-eth 解释** ：  
`/etc/sysconfig/network-script` 这个目录下存放的是网络接口（网卡）的脚 j 本文件（控制文件），其中 `ifcfg-eth0` 是默认的第一个网络接口，如果机器中有多网络接口，那么名字就将依此类推 `ifcfg-eth1`、`ifcfg-eth2`、`ifcfg-eth3` …（这里的文件相当重要，涉及到网络能否正常工作）

**ifcfg-eth 文件配置**：

```shell
TYPE=Ethernet # 网卡类型
DEVICE=eth0 # 网卡接口名称
ONBOOT=yes # 系统启动时是否自动加载
BOOTPROTO=static # 启用地址协议 --static:静态协议 --bootp协议 --dhcp协议
IPADDR=192.168.1.11 # 网卡IP地址
NETMASK=255.255.255.0 # 网卡网络地址
GATEWAY=192.168.1.1 # 网卡网关地址
DNS1=8.8.8.8 # 网卡DNS地址
BROADCAST=192.168.1.255 # 网卡广播地址,可不配
```

VMware -> 编辑 -> 虚拟网络编辑器：  
![](http://121.41.87.138:9001/picgo/img/af0b4147871a4bb6a86c17262a692eb7.png)

### 防火墙

> 使用云服务以后，防火墙相关命令基本可以不用管了，这里不记录相关指令了，用到时再查阅资料

如果是本机测试环境，建议直接关闭防火墙（会拦截某些端口）

```shell
# 停止防火墙服务
systemctl stop firewalld

# 禁止防火墙开机自启
systemctl disable firewalld

# 查看防火墙状态
firewall-cmd --status
```