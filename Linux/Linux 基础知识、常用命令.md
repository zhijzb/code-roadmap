Linux 基础知识
----------

### 内核和发行版

**内核**：只提供操作系统的基本功能和特性，如内存管理，进程调度，文件管理等等

> Linux 内核：[https://www.kernel.org/](https://www.kernel.org/)

**发行版**：基于 linus 维护的 linux 内核，由一些厂商集成了漂亮易用的桌面和常用的软件而发布的商品

![发行版分支图](http://121.41.87.138:9001/picgo/img/发行版分支图.png)

### 常见的 Linux 发行版

*   **RedHat**：国外使用人群最多的 Linux 版本（有收费版本和免费版本），社区活跃  
    ![](http://121.41.87.138:9001/picgo/img/b0877cf67127497db6b78041b908c9e7.png)

> 收费版本和免费版本，其实功能都是一样的，主要是指厂商的服务

*   **Ubuntu**：全球热门的 Linux 版本  
    特点：安装简单、有华丽的图形界面、对一些驱动支持较好、社区活跃  
    ![Ubuntu](http://121.41.87.138:9001/picgo/img/Ubuntu.jpg)
*   **Debian**：几大基础发行版之一，Ubuntu 就是基于 Debian  
    特点：免费、软件包版本较稳定（求稳）、APT 软件包管理、系统开发维护完全由社区驱动  
    ![debian](http://121.41.87.138:9001/picgo/img/debian.jpg)
*   **Fedora**：经常与 Ubuntu 进行比较的发行版，两者的区别主要是包管理不一样；由红帽赞助  
    特点：YUM 包管理、软件包版本都比较新（求新）、迭代快（半年一个新版本）、提供多种桌面环境  
    ![](http://121.41.87.138:9001/picgo/img/9f5ebd4ff5bb4831986471eaa80b80f4.png)
*   **openSUSE**：SUSE Linux 的 open 版本，来自德国的发行版  
    特点：基于企业级的 SUSE Linux + 德国制造，非常稳定、YaST 包管理、桌面华丽  
    ![](http://121.41.87.138:9001/picgo/img/0b8b011f4b0f404eb1b953312e8d5e05.png)
*   **CentOS**：RedHat Linux 的社区版（剔除了专有代码的 RedHat）  
    特点：相当稳定，版本更新紧跟 RedHat，非常适合作为服务器的操作系统使用  
    ![CentOS](http://121.41.87.138:9001/picgo/img/CentOS.jpg)

### Linux 的应用领域

> 网站服务器信息查询：[https://www.netcraft.com/](https://www.netcraft.com/)

**Liunx 的应用领域**：

1.  基于 Linux 的企业服务器
2.  嵌入式应用（Android 就是基于 Linux 的嵌入式操作系统）
3.  物联网的设备

### Linux 与 Windows 的区别

**Linux 与 Windows 的区别**：

1.  Linux 严格区分大小写
2.  Linux 中所有内容都以文件形式保存（包括硬件），一切皆文件
3.  Linux 不依靠拓展名来区分文件类型（但是会有一些默认的扩展名）
    
    > 压缩包：`.gz`、`.bz2`、`.tar.bz2`、`.tgz` 等  
    > 二进制软件包：`.rpm`  
    > 网页文件：`.html`、`.php`  
    > 脚本文件：`.sh`、`.py`  
    > 配置文件：`.conf`
    
4.  Windows 下的程序不能直接在 Linux 中安装和运行

**字符界面的优势**：占用的系统资源更少；减少了出错、被攻击的可能性

Linux 常用命令
----------

**命令提示符**：**[root@localhost ~]#**

*   `root` 当前登陆用户
*   `@` 分隔符
*   `localhost` 主机名
*   `~` 当前所在目录（家目录）  
    如果是 root 代表 /root  
    如果是普通用户 user1 代表 /home/user1
*   `#` 超级用户的提示符  
    `$` 是普通用户的提示符

**命令格式** ：`命令 [选项] [参数]`

*   当有多个选项时，可以写在一起
*   简化选项 `-a` 等价于 完整选项 `--all`

> 注意：个别命令的使用不遵循次格式

**查询目录中的内容**：ls

```
ls [选项] [文件或目录]
选项:
	-a 显示所有文件,包括隐藏文件
	-l 显示详细信息
	-h 人性化显示文件大小
```

**查看一些命令的别名**：`alias`  
![](https://img-blog.csdnimg.cn/a70eccbfd3734ac0949c276ce26bd308.png)

Linux 中用户态和内核态：  
![](http://121.41.87.138:9001/picgo/img/da2f71aac0454cce99e9ce3a8831505f.png)

### * 系统目录结构

![](http://121.41.87.138:9001/picgo/img/88b50cd6ec4a4eb2aecbbcd54b105cb5.png)

*   **/** - 根目录，每一个文件和目录都从根目录开始，只有 root 用户具有该目录下的写权限
    
    > 注意，`/root` 是 root 用户的主目录，与 `/` 不一样
    
*   **/bin** - **用户二进制文件**，包含二进制可执行文件，普通用户使用的 Linux 命令都存放在这里  
    例如：ps、ls、ping、grep、cp
    
*   **/sbin** - **系统二进制文件**，包含二进制可执行文件这个目录下的 Linux 命令通常由系统管理员使用，对系统进行维护  
    例如：iptables、reboot、fdisk、ifconfig、swapon
    
*   **/etc** - **配置文件**，包含所有程序所需的配置文件，以及用于启动 / 停止单个程序的 shell 脚本  
    例如：/etc/resolv.conf、/etc/logrotate.conf、/etc/passwd、/etc/group
    

> hosts：设备名称（或域名）到 ip 地址的解析，相当于本地存在的 dns 功能

*   **/dev** - **设备文件**，包括终端设备、USB 或连接到系统的任何设备  
    例如：/dev/tty1、/dev/usbmon0
    
*   **/proc** - **进程信息**，包含系统进程的相关信息，这是一个虚拟的文件系统  
    包含有关正在运行的进程的信息；例如：/proc/{pid} 目录中包含的与特定 pid 相关的信息  
    系统资源以文本信息形式存；例如：/proc/uptime
    
*   **/var** - **变量文件**，这个目录下可以找到内容可能增长的文件  
    例如：系统日志文件 /var/log、包和数据库文件 /var/lib、电子邮件 /var/mail、打印队列 /var/spool、锁文件 var/lock、多次重新启动需要的临时文件 /var/tmp
    
*   **/tmp** - **临时文件**，包含系统和用户创建的临时文件，当系统重新启动时，这个目录下的文件都将被删除
    
*   **/usr** - **用户程序**，包含二进制文件、库文件、文档和二级程序的源代码。
    
    *   /usr/bin 中包含用户程序的二进制文件；如果在 /bin 中找不到用户二进制文件，到 /usr/bin 目录看看  
        例如：at、awk、cc、less、scp
    *   /usr/sbin 中包含系统管理员的二进制文件；如果 /bin 中找不到系统二进制文件，到 /usr/bin 目录看看  
        例如：atd、cron、sshd、useradd、userdel
    *   /usr/lib 中包含了 /usr/bin 和 /usr/sbin 用到的库
    *   /usr/local 中包含了从源安装的用户程序  
        例如，当你从源安装 Apache，它会在 /usr/local/apache2 中
*   **/home** - HOME 目录，所有用户用 home 目录来存储个人档案  
    例如：/home/hesj、/home/maoge
    
*   **/boot** - **引导加载程序文件**，包含引导加载程序相关的文件，内核的 initrd、vmlinux、grub 文件位于该目录下  
    例如：initrd.img-2.6.32-24-generic、vmlinuz-2.6.32-24-generic
    
*   **/lib** - **系统库**，包含支持位于 /bin 和 /sbin 下的二进制文件的库文件  
    库文件名格式为 `ld*` 或 `lib*.so.*`，例如：ld-2.11.1.so，libncurses.so.5.7
    
*   **/opt** - **可选的附加应用程序**，包含从个别厂商的附加应用程序；附加应用程序应该安装在 /opt/ 及其子目录下
    
*   **/mnt** - **挂载目录，临时安装目录**，系统管理员可以挂载文件系统
    
*   **/media** - **可移动媒体设备**，用于挂载可移动设备的临时目录  
    例如：挂载 CD-ROM 的 /media/cdrom，挂载软盘驱动器的 /media/floppy
    
*   **/srv** - **服务数据**，包含服务器特定服务相关的数据  
    例如，/srv/cvs 包含 cvs 相关的数据
    

### ssh 协议

ssh（Secure Shell，安全外壳协议）：由 IETF 的网络小组（Network Working Group）所制定

*   SSH 为建立在**应用层**基础上的安全协议，专为远程登录会话和其他网络服务提供安全性  
    利用 SSH 协议可以有效防止远程管理过程中的信息泄露问题
*   SSH 客户端适用于多种平台，几乎所有 UNIX 平台都可运行 SSH

> SSH 最初是 UNIX 系统上的一个程序，后来又迅速扩展到其他操作平台

其实使用 Windows 的命令行工具（PowerShell、cmd）也可以连接到 Linux 远程服务器：

```shell
ssh root@192.168.52.128 # 使用root账号登录
```

### * 文件处理命令

> Linux 两大思想：
> 
> *   **一切皆文件**
> *   **没有消息就是最好的消息**

*   **查询所在目录位置**：pwd
    
    ```shell
    pwd
    ```
    
    > pwd - print working directory
    
*   **创建文件夹**：touch
    
    ```shell
    touch [文件名]
    ```
    
*   **建立目录**：mkdir
    
    ```shell
    mkdir -p [目录名]
    	-p 递归创建
    ```
    
    > mkdir - make directories
    
*   **切换所在目录**：cd
    
    ```shell
    cd [目录]
    ```
    
    ```shell
    cd ~ #进入当前用户的家目录
    cd # 同上
    cd .. # 进入上一级目录
    cd ~ # 进入上次目录
    ```
    
    > cd - change directory
    
*   **删除空目录**：rmdir
    
    ```shell
    rmdir [目录名]
    ```
    
    > rmdir - remove empty directory
    
*   **删除文件或目录**：rm
    
    ```shell
    rm -rf [文件或目录]
    	-r 递归删除目录
    	-f 强制
    ```
    
    > rm - remove
    
*   **复制命令**：cp
    
    ```shell
    cp [选项] [源文件或目录] [目标目录]
    	-r 复制目录
    ```
    
    > cp - copy
    
*   **剪切或改名命令**：mv
    
    ```shell
    mv [原文件或目录] [目标目录]
    ```
    
    > mv - move
    

### 文件搜索命令 - find、grep

**命令搜索命令**

*   whereis：搜索命令所在路径及帮助文档所在位置
    
    ```shell
    whereis [命令名]
    	-b 只查找可执行文件
    	-m 只查找帮助文件
    ```
    
*   which：搜索命令所在路径及别名
*   PATH 环境变量：定义的是系统搜索命令的路径，相当于 window 下的 path

**文件搜索命令**：find

```shell
find [搜索范围] [搜索条件］
```

```shell
# 尽量避免大范围的搜索,会非常耗费系统资源
# 需要匹配使用通配符,通配符是完全普配,可以使用*等通配符

# 从根目录开始全盘搜索
find / -name install.log

# -i 表示不区分大小写
# 从 /root 目录开始不区分大小写的搜索
find /root -iname install.log

# 查找所有者是root的文件
find /root -user root

# 查找没有所有者的文件
find /root -nouser
```

**字符串搜索命令**：grep

```shell
grep [选项] 字符串 文件名
	-i 忽略大小写
	-v 排除指定字符串
	-n 显示行号
```

示例：

```shell
# 在test.txt中搜索hello字符串,并显示行号
grep -n hello test.txt

# 查看zoo_sample.cfg的内容(不包含有#的行)
cat zoo_sample.cfg | grep -v "#"
```

find 命令 和 grep 命令的区别：

*   find：在**系统中**搜索符合条件的文件名
*   grep：在**文件中**搜索符合条件的字符串

### 帮助命令 - man、help

```shell
man ls # 查看ls的帮助
```

```shell
ls --help 
# --help 不适用内部命令, 如cd

help cd
```

> 建议：统一使用 man 即可

### 压缩与解压缩命令 - zip、gzip、bz2

总结个规律：Linux 命令中基本上要指定参数都是 目标在前，源在后

**.zip**：使用 zip 前必须先安装 `yum install -y zip`，unzip 同理 `yum install -y unzip`

*   压缩
    
    ```shell
    # 适用于单个文件
    zip 压缩文件名 源文件
    
    # 适用于文件夹(-r表示递归)
    zip -r 压缩文件名 源文件
    ```
    
*   解压
    
    ```shell
    unzip 压缩文件名
    ```
    

**.gz**

*   压缩
    
    ```shell
    # 压缩为.gz格式的压缩文件(不保留源文件)
    gzip 源文件
     # 压缩为.gz格式的压缩文件(保留源文件)
    gzip -c 源文件 > 压缩文件
    # -c 的意思并不是保留原文件,而是结合 > 把压缩结果输出到控制台
    gzip -r 目录 # 压缩目录下的所有子文件,但是不能压缩目录
    ```
    
*   解压
    
    ```shell
    gzip -d 压缩文件 # 解压缩文件
    ```
    

**.bz2**

*   压缩：bzip2 命令无法压缩目录
    
    ```shell
    bzip2 源文件 # 压缩为.bz2格式(不保留源文件)
    bzip2 -k 源文件 # 压缩为.bz2格式(保留源文件)
    ```
    
*   解压：
    
    ```shell
    bzip2 -d 压缩文件 # 解压(-k保留压缩文件)
    bzip2 压缩文件 # 解压(-k保留压缩文件)
    ```
    

### * 打包 - .tar、打包压缩 - .tar.gz、.tar.bz2

**.tar**：只用 tar 命令进行打包不会进行压缩

*   打包
    
    ```shell
    tar -cvf 打包文件名 源文件
    	-c 打包
    	-v 显示过程
    	-f 指定打包后的文件名
    ```
    
*   解包：
    
    ```shell
    tar -xvf 打包文件名
    	-x 解包(抽取)
    ```
    

**.tar.gz**：先打包为 .tar 格式，再压缩为 .gz 格式

*   压缩
    
    ```shell
    tar -zcvf 压缩包名.tar.gz 源文件
    	-z 压缩为.tar.gz格式
    ```
    
*   解压
    
    ```shell
    tar -zxvf 压缩包名.tar.gz
    	-z 解压为.tar.gz格式
    
    tar -zxvf 压缩包名.tar.gz -C 指定目录
    	-C 解压到指定目录
    ```
    

**.tar.bz2**：先打包为 .tar 格式，再压缩为 .bz2 格式

*   压缩
    
    ```shell
    tar -jcvf 压缩包名.tar.bz2 源文件
    	-j 压缩为.tar.bz2格式
    ```
    
*   解压
    
    ```shell
    tar -jxvf 压缩包名.tar.bz2
    	-j 压缩为.tar.bz2格式
    ```
    

### 关机与重启命令

shutdown 在关机时会自动保存数据：

```shell
shutdown [选项] 时间
	-c		取消前一个关机命令
	-h		关机
	-r		重启
```

以下命令也可以关机，但是不会保存数据：halt、poweroff、init 0

重启命令：reboot、init 6

退出登陆：logout

**系统运行级别**

*   查看运行级别
    
    ```shell
    runlevel
    ```
    
*   系统默认运行级别
    
    ```shell
    # 可以修改系统默认级别
    cat /etc/inittab
    ```
    

### 其他命令

查看用户登陆信息：w、who

查看最后一次登陆时间：lastlog

磁盘使用情况：df

```shell
df # 显示磁盘的使用情况
df -h # 人性化显示
```

查看任务进程：top

查看内存占用：free

查看操作历史：history

在显示器输出内容：echo

**文件查看命令**：

*   cat：整个文件的内容显示出来
*   tail：默认在屏幕上显示指定文件的末尾 10 行