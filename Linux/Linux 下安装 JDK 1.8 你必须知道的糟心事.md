> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [juejin.cn](https://juejin.cn/post/6953409604340416542)

来源：Atstudy 网校

### 1. 简介

在 Oracle 收购 Sun 后，Java 的一系列产品就被整合到 Oracle 官网中，打开官网乍眼一看也不知道去哪里下载，还的一个一个的摸索尝试，而且网上大多数都是一些 Oracle 收购 Sun 前，或者就是一些老的资料文章，为了避免这些坑，宏哥才决定写着一篇文章。

JDK 是 JAVA 的软件开发工具包，如果要使用 JAVA 来进行开发，或者部署基于其开发的应用，那么就需要安装 JDK。本次将在 Linux 下安装 JDK 及配置环境。在以前宏哥也在 Linux 下安装过 JDK，安装过程溜得飞起来。结果这次安装过程中却是十分的不顺，所以才有了这篇文章用来记录和分享安装 过程中遇到的问题和心得体会，希望对各位有所帮忙，避免做更多的无用功。

宏哥的环境：

查看 Linux 环境输入命令：**lsb_release -a**

```
[admin@oftpclient201 ~]$ lsb_release -a 
LSB Version:    :core-4.1-amd64:core-4.1-noarch:cxx-4.1-amd64:cxx-4.1-noarch:desktop-4.1-amd64:desktop-4.1-noarch:languages-4.1-amd64:languages-4.1-noarch:printing-4.1-amd64:printing-4.1-noarch 
Distributor ID: CentOS 
Description:    CentOS Linux release 7.9.2009 (Core) 
Release:        7.9.2009
```

### 2. 下载 JDK

在安装之前，检查是否存在 Linux 下自带的 OpenJDK，命令：**rpm -qa | grep java**。若存在，则需要进行卸载，命令：**rpm -e --nodeps 卸载的软件名**。

**其他方法：**

java -version echo $PATH 看环境变量 bai 是否配 du 置了 java 路径 find / -name java 查找 zhijava 文件

JDK 历史版本链接：[www.oracle.com/technetwork…](https://link.juejin.cn?target=https%3A%2F%2Flink.zhihu.com%2F%3Ftarget%3Dhttps%253A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Farchive-139210.html "https://link.zhihu.com/?target=https%3A//www.oracle.com/technetwork/java/javase/archive-139210.html")

接着，我们可以通过 `wget` 命令下载 JDK 安装包，或者下载后传到 Linux。我这里下载的安装包版本是 [jdk-8u281-linux-x64.tar.gz](https://link.juejin.cn?target=https%3A%2F%2Flink.zhihu.com%2F%3Ftarget%3Dhttps%253A%2F%2Fwww.oracle.com%2Fjava%2Ftechnologies%2Fjavase%2Fjavase-jdk8-downloads.html%2523license-lightbox "https://link.zhihu.com/?target=https%3A//www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html%23license-lightbox") 。

### 2.1 使用 wget 下载 JDK8

每次去官网下载 JDK 有点烦 但是直接使用 wget 又得同意协议所以 使用如下的 wget 就好了 (注意是 64 位的哦)  
1. 先去官网看一下地址变化 没有如下 : 修改后面的下载地址即可 注意哦~ 现在和以前变化挺大的，所以宏哥在这里赘述一下具体步骤：  
（1）在浏览器的地址栏输入 JDK 官网地址：**[www.oracle.com/](https://link.juejin.cn?target=https%3A%2F%2Flink.zhihu.com%2F%3Ftarget%3Dhttps%253A%2F%2Fwww.oracle.com%2F "https://link.zhihu.com/?target=https%3A//www.oracle.com/") ，** 如下图所示：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/90df4f4865a94bec82e2ae5bc0be718f~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)

（2）鼠标放在 product 上在下拉菜单中点击 Java，如下图所示：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/22d4d693240f44a5909931161b70708e~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)

（3）然后再点击 “Download Java”，如下图所示：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/59976dd1cfa64a4e83cb263b5195e56d~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)

（4）下滑页面找到 Java SE8，然后点击 “JDK Download”, 如下图所示：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/cb040a99a7724a1d9550863981b26ccb~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)

（5）找到 Linux 系统的 64 位的 JDK，选中安装包右键，点击 “复制链接地址”，如下图所示：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/9dc477137f2d42c8924d71de211b7079~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)

（6）在 xshell 上输入命令 wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" 后面加上你刚才复制的下载链接  
2. 然后使用下面的 wget 下载就好了~(注意文件的后缀，有时候不可以直接解压需要重命名后缀名哦)

```
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/jdk-8u171-linux-x64.tar.gz
```

### 3. 解压安装包

创建一个文件夹，用于存放 JDK 安装包，然后解压到该目录下。

创建文件夹：**mkdir /usr/JDK**  
进入文件夹：**cd /usr/JDK**  
将下载好的压缩文件剪贴到创建好的文件夹下：**mv jdk-8u281-linux-x64.tar.gz /usr/JDK/**  
解压：

```shell
mkdir /usr/JDK
cd /usr/JDK
# 上传jdk到/usr/JDK
tar -zxvf jdk-8u351-linux-x64.tar.gz
```



![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/58560f7ff272409caba8064d5404b570~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)

可以看到，本次解压到了当前目录 `/usr/JDK/jdk1.8.0_351下。

### 4. 配置环境

解压完成之后，我们要配置下环境变量，通过 `vim` 命令修改配置文件 `/etc/profile` 来设置环境变量。

```shell
vim /etc/profile
```

在文件最后一行，输入 `i` 进入编辑模式，添加以下内容，然后按 `Esc` 退出编辑模式，再输入 `:wq` 保存并退出。

```
export JAVA_HOME=/usr/JDK/jdk1.8.0_351   
export JRE_HOME=${JAVA_HOME}/jre   
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib   
export PATH=${JAVA_HOME}/bin:$PATH
```

设置完之后，如果要使环境变量立即生效，需要通过命令：**`source /etc/profile`**，重新加载配置文件。

```shell
source /etc/profile
```



### 5. 验证是否安装成功

所有都配置好了，我们需要验证下是否安装成功。

依次输入 **java -version**、**java**、**javac**，不会出现报错并且显示出 jdk 版本号及 java/javac 相关命令参数说明界面。

```
[admin@oftpclient201 ~]$ java -version
java version "1.8.0_281"
Java(TM) SE Runtime Environment (build 1.8.0_281-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.281-b09, mixed mode)
[admin@oftpclient201 ~]$ java
Usage: java [-options] class [args...]
           (to execute a class)
   or  java [-options] -jar jarfile [args...]
           (to execute a jar file)
where options include:
    -d32          use a 32-bit data model if available
    -d64          use a 64-bit data model if available
    -server       to select the "server" VM
                  The default VM is server.

    -cp <class search path of directories and zip/jar files>
    -classpath <class search path of directories and zip/jar files>
                  A : separated list of directories, JAR archives,
                  and ZIP archives to search for class files.
    -D<name>=<value>
                  set a system property
    -verbose:[class|gc|jni]
                  enable verbose output
    -version      print product version and exit
    -version:<value>
                  Warning: this feature is deprecated and will be removed
                  in a future release.
                  require the specified version to run
    -showversion  print product version and continue
    -jre-restrict-search | -no-jre-restrict-search
                  Warning: this feature is deprecated and will be removed
                  in a future release.
                  include/exclude user private JREs in the version search
    -? -help      print this help message
    -X            print help on non-standard options
    -ea[:<packagename>...|:<classname>]
    -enableassertions[:<packagename>...|:<classname>]
                  enable assertions with specified granularity
    -da[:<packagename>...|:<classname>]
    -disableassertions[:<packagename>...|:<classname>]
                  disable assertions with specified granularity
    -esa | -enablesystemassertions
                  enable system assertions
    -dsa | -disablesystemassertions
                  disable system assertions
    -agentlib:<libname>[=<options>]
                  load native agent library <libname>, e.g. -agentlib:hprof
                  see also, -agentlib:jdwp=help and -agentlib:hprof=help
    -agentpath:<pathname>[=<options>]
                  load native agent library by full pathname
    -javaagent:<jarpath>[=<options>]
                  load Java programming language agent, see java.lang.instrument
    -splash:<imagepath>
                  show splash screen with specified image
See http://www.oracle.com/technetwork/java/javase/documentation/index.html for more details.
[admin@oftpclient201 ~]$ javac
Usage: javac <options> <source files>
where possible options include:
  -g                         Generate all debugging info
  -g:none                    Generate no debugging info
  -g:{lines,vars,source}     Generate only some debugging info
  -nowarn                    Generate no warnings
  -verbose                   Output messages about what the compiler is doing
  -deprecation               Output source locations where deprecated APIs are used
  -classpath <path>          Specify where to find user class files and annotation processors
  -cp <path>                 Specify where to find user class files and annotation processors
  -sourcepath <path>         Specify where to find input source files
  -bootclasspath <path>      Override location of bootstrap class files
  -extdirs <dirs>            Override location of installed extensions
  -endorseddirs <dirs>       Override location of endorsed standards path
  -proc:{none,only}          Control whether annotation processing and/or compilation is done.
  -processor <class1>[,<class2>,<class3>...] Names of the annotation processors to run; bypasses default discovery process
  -processorpath <path>      Specify where to find annotation processors
  -parameters                Generate metadata for reflection on method parameters
  -d <directory>             Specify where to place generated class files
  -s <directory>             Specify where to place generated source files
  -h <directory>             Specify where to place generated native header files
  -implicit:{none,class}     Specify whether or not to generate class files for implicitly referenced files
  -encoding <encoding>       Specify character encoding used by source files
  -source <release>          Provide source compatibility with specified release
  -target <release>          Generate class files for specific VM version
  -profile <profile>         Check that API used is available in the specified profile
  -version                   Version information
  -help                      Print a synopsis of standard options
  -Akey[=value]              Options to pass to annotation processors
  -X                         Print a synopsis of nonstandard options
  -J<flag>                   Pass <flag> directly to the runtime system
  -Werror                    Terminate compilation if warnings occur
  @<filename>                Read options and filenames from file

[admin@oftpclient201 ~]$
```

### 6. 小结

安装过程中宏哥为了避免麻烦不是在 Windows 上直接安装好然后再上传到 Linux 上，而是直接用 weget 命令直接安装的，结果遇到如下一系列的问题：  
（1）wget + 复制好的下载地址

```
[admin@oftpclient201 ~]$ wget https://download.oracle.com/otn/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz--2021-02-25 15:25:47--  https://download.oracle.com/otn/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gzResolving download.oracle.com (download.oracle.com)... 184.50.93.194Connecting to download.oracle.com (download.oracle.com)|184.50.93.194|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://edelivery.oracle.com/akam/otn/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz [following]
--2021-02-25 15:25:48--  https://edelivery.oracle.com/akam/otn/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz
Resolving edelivery.oracle.com (edelivery.oracle.com)... 184.30.17.110, 2600:1400:c000:482::366, 2600:1400:c000:488::366
Connecting to edelivery.oracle.com (edelivery.oracle.com)|184.30.17.110|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://login.oracle.com:443/oam/server/obrareq.cgi?encquery%3D4PIlzUXFrmsJOQa3pTVPbD8FleZqJX%2Bqm5A%2BmXnkil9RBAqGW%2Bd5Z3FgcL03OH1P%2FL%2FPpBosJhXNi%2BaNvJ5gWRCWlkmsTghONwL1Ixk8tJ66nMfQItxzYLYEA6Ae%2F4xxJtk3wvyzw4EzT%2B88B%2BHNgKJs6w67VGaR6kHqcVem1SIW%2Fxxps6xSH%2Bqe9li%2BI0FMXUw2Dptss7A0VEtYvTTpUHnLteqH9Wt2TJiK%2F8%2BVmtRUTs5Y2YaOxZqrr6VcBsyAi83RSY1PKFiJChmpxiao1%2Bomcz9PKzc04xMlEg2hVJFAoTxUkyPKCWWEmipcbGPfcQOJYVrfjGqqmAGIDBfUfmpAGcQf21ceJqkm739Jf%2Bue68iM7YPcI7WOdFjxWLG9Ykj2NVjTwjmCDzZUsBFzMiT8SeXRqH7dTevRYZuzg0b%2F8PZNqeTjzZ5LHzG6WA1%2FlWsXpwgBuB3OklWgZs5HBg25Mx5Uqdu%2BHzc6qzaGNlWKrQj5pWUvGpz1rVPB23SywqnzFy9iUm4I%2FlXYeJDzJQ%3D%3D%20agentid%3Dedelivery-extprod%20ver%3D1%20crmethod%3D2&ECID-Context=1.005j2GF_XvzFo2KimTctkJ00023d00wI1l%3BkXjE [following]
--2021-02-25 15:25:50--  https://login.oracle.com/oam/server/obrareq.cgi?encquery%3D4PIlzUXFrmsJOQa3pTVPbD8FleZqJX%2Bqm5A%2BmXnkil9RBAqGW%2Bd5Z3FgcL03OH1P%2FL%2FPpBosJhXNi%2BaNvJ5gWRCWlkmsTghONwL1Ixk8tJ66nMfQItxzYLYEA6Ae%2F4xxJtk3wvyzw4EzT%2B88B%2BHNgKJs6w67VGaR6kHqcVem1SIW%2Fxxps6xSH%2Bqe9li%2BI0FMXUw2Dptss7A0VEtYvTTpUHnLteqH9Wt2TJiK%2F8%2BVmtRUTs5Y2YaOxZqrr6VcBsyAi83RSY1PKFiJChmpxiao1%2Bomcz9PKzc04xMlEg2hVJFAoTxUkyPKCWWEmipcbGPfcQOJYVrfjGqqmAGIDBfUfmpAGcQf21ceJqkm739Jf%2Bue68iM7YPcI7WOdFjxWLG9Ykj2NVjTwjmCDzZUsBFzMiT8SeXRqH7dTevRYZuzg0b%2F8PZNqeTjzZ5LHzG6WA1%2FlWsXpwgBuB3OklWgZs5HBg25Mx5Uqdu%2BHzc6qzaGNlWKrQj5pWUvGpz1rVPB23SywqnzFy9iUm4I%2FlXYeJDzJQ%3D%3D%20agentid%3Dedelivery-extprod%20ver%3D1%20crmethod%3D2&ECID-Context=1.005j2GF_XvzFo2KimTctkJ00023d00wI1l%3BkXjE
Resolving login.oracle.com (login.oracle.com)... 209.17.4.8
Connecting to login.oracle.com (login.oracle.com)|209.17.4.8|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 4103 (4.0K) [text/html]
Saving to: ‘jdk-8u281-linux-x64.tar.gz.1’

100%[==============================================================================================================================================================================>] 4,103       17.6KB/s   in 0.2s

2021-02-25 15:25:51 (17.6 KB/s) - ‘jdk-8u281-linux-x64.tar.gz.1’ saved [4103/4103]

[admin@oftpclient201 ~]$
```

从上图看着好像是下载成功了，但是宏哥在解压的时候有报错了，报错如下：

```
[admin@oftpclient201 ~]$ tar -zxvf jdk-8u281-linux-x64.tar.gz

gzip: stdin: not in gzip format
tar: Child returned status 1
tar: Error is not recoverable: exiting now
[admin@oftpclient201 ~]$
```

从上图的报错可以发现说是压缩包不完整，宏哥好奇既然下载成功怎么会是不完整的了，于是宏哥看了一下压缩包的大小，如下所示才 4.1K，

```
[admin@oftpclient201 ~]$ ll -lh
total 16K
-rw-rw-r--. 1 admin admin    0 Feb 25 15:25 ]
-rw-rw-r--. 1 admin admin 4.1K Feb 25 15:24 jdk-8u281-linux-x64.tar.gz
-rw-rw-r--. 1 admin admin   39 Feb  2 16:20 test.txt
-rw-rw-r--. 1 admin admin   78 Feb  2 16:21 t.zip
[admin@oftpclient201 ~]$
```

这个与 Oracle 官网显示的大小 100 多 M 差太多了，所以确定是下载不成功而且回过头来看一下下载时间几乎零点几秒就下载完成了，更加确定下载的压缩包是不完整的。  
查了资料说是由于 Oracle 需要 accept license、cookie，所以需要给 wget 带如下参数即可，于是就有了第二种方法的尝试  
（2）wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie"+ 复制好的下载地址

```
[admin@oftpclient201 ~]$ wget --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie;" https://download.oracle.com/otn/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz
--2021-02-25 15:56:55--  https://download.oracle.com/otn/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz
Resolving download.oracle.com (download.oracle.com)... 92.123.228.99
Connecting to download.oracle.com (download.oracle.com)|92.123.228.99|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://edelivery.oracle.com/akam/otn/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz [following]
--2021-02-25 15:56:56--  https://edelivery.oracle.com/akam/otn/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz
Resolving edelivery.oracle.com (edelivery.oracle.com)... 23.76.64.181, 2a02:26f0:a00:3be::366, 2a02:26f0:a00:385::366
Connecting to edelivery.oracle.com (edelivery.oracle.com)|23.76.64.181|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://login.oracle.com:443/oam/server/obrareq.cgi?encquery%3DSyDVndLEpx6WpSyvf2If6hV7vdEN%2BIvCiOpWhz5xUgWJKCBA4mYVvksWHq4vohW6kyMvXAjWfJyda2XrAkGPdxyVK9FcqsRYhvs1DZDEbVLAtEnG5a2Zw2R07iGjm1v79FPjjXyDKSR%2B52%2F0qdEhmP6pPdLPEjhEHVNXr1usKyMmhI0FeYrjn7YcQLjvVOGbZsVR6%2Bu0hqU8jZPxDtXpo8zANJoPdEGxwOiThOVkCWm2cvpOC3Y0QBICZ1yvcWtvTOSHUwZMZgeEp2EdCqlHwH0nm0JWFAKHf6qL9RI1Qs0ZRNth%2FkzjYDLrlY%2B9D1%2BESsQHCAUa2kRHwKXf89u%2BbIUMT2o0xqFtzvJLgQjlG9pUZ7JIxHYVZXzRc1lQwiJove8sjHWNOaujjaryNEGMH5WfiqCp8Y6ogcgd%2B4qNoBLOz7oCOsZkYrzAyfq7481ei7PPvESoBRw6ezsmB06LZt1YHnpf%2BGwf2q33TckhLOVb8C6Bs3bwkmhBfwhDNMnGd%2BonHh%2Fg7kcNscjuTkbwFw%3D%3D%20agentid%3Dedelivery-extprod%20ver%3D1%20crmethod%3D2&ECID-Context=1.005j2HyusRcFo2KimTXvWJ0002ZZ00IcIc%3BkXjE [following]
--2021-02-25 15:56:57--  https://login.oracle.com/oam/server/obrareq.cgi?encquery%3DSyDVndLEpx6WpSyvf2If6hV7vdEN%2BIvCiOpWhz5xUgWJKCBA4mYVvksWHq4vohW6kyMvXAjWfJyda2XrAkGPdxyVK9FcqsRYhvs1DZDEbVLAtEnG5a2Zw2R07iGjm1v79FPjjXyDKSR%2B52%2F0qdEhmP6pPdLPEjhEHVNXr1usKyMmhI0FeYrjn7YcQLjvVOGbZsVR6%2Bu0hqU8jZPxDtXpo8zANJoPdEGxwOiThOVkCWm2cvpOC3Y0QBICZ1yvcWtvTOSHUwZMZgeEp2EdCqlHwH0nm0JWFAKHf6qL9RI1Qs0ZRNth%2FkzjYDLrlY%2B9D1%2BESsQHCAUa2kRHwKXf89u%2BbIUMT2o0xqFtzvJLgQjlG9pUZ7JIxHYVZXzRc1lQwiJove8sjHWNOaujjaryNEGMH5WfiqCp8Y6ogcgd%2B4qNoBLOz7oCOsZkYrzAyfq7481ei7PPvESoBRw6ezsmB06LZt1YHnpf%2BGwf2q33TckhLOVb8C6Bs3bwkmhBfwhDNMnGd%2BonHh%2Fg7kcNscjuTkbwFw%3D%3D%20agentid%3Dedelivery-extprod%20ver%3D1%20crmethod%3D2&ECID-Context=1.005j2HyusRcFo2KimTXvWJ0002ZZ00IcIc%3BkXjE
Resolving login.oracle.com (login.oracle.com)... 209.17.4.8
Connecting to login.oracle.com (login.oracle.com)|209.17.4.8|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 4149 (4.1K) [text/html]
Saving to: ‘jdk-8u281-linux-x64.tar.gz’

100%[==============================================================================================================================================================================>] 4,149       --.-K/s   in 0s

2021-02-25 15:56:59 (24.7 MB/s) - ‘jdk-8u281-linux-x64.tar.gz’ saved [4149/4149]

[admin@oftpclient201 ~]$
```

按方法一确认以后发现下载的压缩包还是不完整，查了资料说是按这种方法就可以，也有的说是这种方法已经过时了。还有的说是 wget 命令有缺陷不会重定向，但是抓包是发现网页中点击下载会有重定向的请求，... 等等各种各样的说法，第二天宏哥正要老老实实的在 Oracle 注册、登录账号（太麻烦）据说注册、登录账号后，复制的下载链接中会自带 cookie、协议和安全认证，然后用 wget 命令 + 上此下载链接可以下载成功，但是宏哥觉得有点麻烦，这种方法就没尝试，你如果有兴趣的话，童鞋们或者小伙伴自己可以尝试一下按宏哥说的是否可以成功下载压缩包。，然后下载 Linux 的 JDK 安装包，然后再上传到 Linux 系统中。但是宏哥这个人比较懒吧，也比较不甘心，所以再次决定再查查资料。结果百度查询了半天左右，经过反复的摸索终于找到一个靠谱的答案。网上那些太多的都是复制来复制去，都不能解决实质性问题。  
（3）在方法二的基础上将下载地址中的 otn 修改成 otn-pub=

```
[admin@oftpclient201 ~]$ wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" https://download.oracle.com/otn-pub/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz
--2021-02-25 16:10:40--  https://download.oracle.com/otn-pub/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz
Resolving download.oracle.com (download.oracle.com)... 184.86.92.87
Connecting to download.oracle.com (download.oracle.com)|184.86.92.87|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://edelivery.oracle.com/otn-pub/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz [following]
--2021-02-25 16:10:41--  https://edelivery.oracle.com/otn-pub/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz
Resolving edelivery.oracle.com (edelivery.oracle.com)... 23.76.87.214, 2600:1400:c000:482::366, 2600:1400:c000:488::366
Connecting to edelivery.oracle.com (edelivery.oracle.com)|23.76.87.214|:443... connected.
HTTP request sent, awaiting response... 302 Moved Temporarily
Location: https://download.oracle.com/otn-pub/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz?AuthParam=1614240762_cf401250c965e2946ab43486aa6c2db3 [following]
--2021-02-25 16:10:42--  https://download.oracle.com/otn-pub/java/jdk/8u281-b09/89d678f2be164786b292527658ca1605/jdk-8u281-linux-x64.tar.gz?AuthParam=1614240762_cf401250c965e2946ab43486aa6c2db3
Connecting to download.oracle.com (download.oracle.com)|184.86.92.87|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 143722924 (137M) [application/x-gzip]
Saving to: ‘jdk-8u281-linux-x64.tar.gz’

100%[==============================================================================================================================================================================>] 143,722,924 2.67MB/s   in 57s

2021-02-25 16:11:40 (2.41 MB/s) - ‘jdk-8u281-linux-x64.tar.gz’ saved [143722924/143722924]

[admin@oftpclient201 ~]$ ll -h
```

从上图可以看出下载时间是 57s, 虽然这个时间与网速有关系，但是大致感觉时间也差不多了。因此宏哥判断这次是真的解决了，安装包下载成功了。  
和宏哥来再次看一下压缩的安装包的大小是 138M，和官网展示的大小也差不多，预示着下载成功，问题解决

```
[admin@oftpclient201 ~]$ ll -h
total 138M
-rw-rw-r--. 1 admin admin    0 Feb 25 16:09 ]
-rw-rw-r--. 1 admin admin 138M Dec 11 03:12 jdk-8u281-linux-x64.tar.gz
-rw-rw-r--. 1 admin admin   39 Feb  2 16:20 test.txt
-rw-rw-r--. 1 admin admin   78 Feb  2 16:21 t.zip
[admin@oftpclient201 ~]$
```

### 7. 拓展

7.1linux 下如何查看已安装的 centos 版本信息  
1.Linux 查看当前操作系统版本信息 cat /proc/version

```
[admin@oftpclient201 ~]$ cat /proc/version
Linux version 3.10.0-1160.11.1.el7.x86_64 (mockbuild@kbuilder.bsys.centos.org) (gcc version 4.8.5 20150623 (Red Hat 4.8.5-44) (GCC) ) #1 SMP Fri Dec 18 16:34:56 UTC 2020
[admin@oftpclient201 ~]$
```

2.Linux 查看版本当前操作系统内核信息 uname -a

```
[admin@oftpclient201 ~]$ uname -a
Linux oftpclient201.test.cedex.cn 3.10.0-1160.11.1.el7.x86_64 #1 SMP Fri Dec 18 16:34:56 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
```

3.linux 查看版本当前操作系统发行信息 cat /etc/issue 或 cat /etc/centos-release

```
[admin@oftpclient201 ~]$ cat /etc/centos-release
CentOS Linux release 7.9.2009 (Core)
```

4.Linux 查看 cpu 相关信息，包括型号、主频、内核信息等 cat /etc/cpuinfo

```
[admin@oftpclient201 ~]$ cat /proc/cpuinfo 
processor    : 0 #数值为0表示第1颗cpu
vendor_id    : GenuineIntel
cpu family    : 6
model        : 58
model name    : Intel(R) Core(TM) i5-3320M CPU @ 2.60GHz
stepping    : 9
microcode    : 0x15
cpu MHz        : 2594.170
cache size    : 3072 KB
physical id    : 0
siblings    : 1
core id        : 0
cpu cores    : 1
apicid        : 0
initial apicid    : 0
fpu        : yes
fpu_exception    : yes
cpuid level    : 13
wp        : yes
flags        : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts mmx fxsr sse sse2 ss syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts nopl xtopology tsc_reliable nonstop_tsc aperfmperf eagerfpu pni pclmulqdq ssse3 cx16 pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm epb fsgsbase tsc_adjust smep dtherm ida arat pln pts
bogomips    : 5188.34
clflush size    : 64
cache_alignment    : 64
address sizes    : 42 bits physical, 48 bits virtual
power management:

processor    : 1 #数值为1表示第2颗cpu
vendor_id    : GenuineIntel
cpu family    : 6
model        : 58
model name    : Intel(R) Core(TM) i5-3320M CPU @ 2.60GHz
stepping    : 9
microcode    : 0x15
cpu MHz        : 2594.170
cache size    : 3072 KB
physical id    : 2
siblings    : 1
core id        : 0
cpu cores    : 1
apicid        : 2
initial apicid    : 2
fpu        : yes
fpu_exception    : yes
cpuid level    : 13
wp        : yes
flags        : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts mmx fxsr sse sse2 ss syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts nopl xtopology tsc_reliable nonstop_tsc aperfmperf eagerfpu pni pclmulqdq ssse3 cx16 pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm epb fsgsbase tsc_adjust smep dtherm ida arat pln pts
bogomips    : 5188.34
clflush size    : 64
cache_alignment    : 64
address sizes    : 42 bits physical, 48 bits virtual
power management:
```

**processor　：** 系统中逻辑处理核的编号。对于单核处理器，则课认为是其 CPU 编号，对于多核处理器则可以是物理核、或者使用超线程技术虚拟的逻辑核  
**vendor_id　：**CPU 制造商  
**cpu family　：**CPU 产品系列代号  
**model　　　：**CPU 属于其系列中的哪一代的代号  
**model name：**CPU 属于的名字及其编号、标称主频  
**stepping　 ：**CPU 属于制作更新版本  
**cpu MHz　 ：**CPU 的实际使用主频  
**cache size ：**CPU 二级缓存大小  
**physical id ：** 单个 CPU 的标号  
**siblings ：** 单个 CPU 逻辑物理核数  
**core id ：** 当前物理核在其所处 CPU 中的编号，这个编号不一定连续  
**cpu cores ：** 该逻辑核所处 CPU 的物理核数  
**apicid ：** 用来区分不同逻辑核的编号，系统中每个逻辑核的此编号必然不同，此编号不一定连续  
**fpu ：** 是否具有浮点运算单元（Floating Point Unit）  
**fpu_exception ：** 是否支持浮点计算异常  
**cpuid level ：** 执行 cpuid 指令前，eax 寄存器中的值，根据不同的值 cpuid 指令会返回不同的内容  
**wp ：** 表明当前 CPU 是否在内核态支持对用户空间的写保护（Write Protection）  
**flags ：** 当前 CPU 支持的功能  
**bogomips ：** 在系统内核启动时粗略测算的 CPU 速度（Million Instructions Per Second）  
**clflush size ：** 每次刷新缓存的大小单位  
**cache_alignment ：** 缓存地址对齐单位  
**address sizes ：** 可访问地址空间位数  
5.Linux 查看版本说明当前 CPU 运行在 32bit 模式下 (但不代表 CPU 不支持 64bit)

```
[admin@oftpclient201 etc]$ getconf LONG_BIT
```

7.2uname 的使用  
uname 命令用于打印当前系统相关信息（内核版本号、硬件架构、主机名称和操作系统类型等）。

```
uname -a显示全部信息
-m或--machine：显示电脑类型；
-r或--release：显示操作系统的发行编号；
-s或--sysname：显示操作系统名称；
-v：显示操作系统的版本；
-p或--processor：输出处理器类型或"unknown"；
-i或--hardware-platform：输出硬件平台或"unknown"；
-o或--operating-system：输出操作系统名称；
--help：显示帮助；
--version：显示版本信息。
```

7.3 查看 Linux 版本  
1. 查看系统版本信息的命令 lsb_release -a  
(使用命令时提示 command not found, 需要安装 yum install redhat-lsb -y)

```
[admin@oftpclient201 etc]$
[admin@oftpclient201 etc]$ lsb_release -a
LSB Version:    :core-4.1-amd64:core-4.1-noarch:cxx-4.1-amd64:cxx-4.1-noarch:desktop-4.1-amd64:desktop-4.1-noarch:languages-4.1-amd64:languages-4.1-noarch:printing-4.1-amd64:printing-4.1-noarch
Distributor ID: CentOS
Description:    CentOS Linux release 7.9.2009 (Core)
Release:        7.9.2009
Codename:       Core
```

注: 这个命令适用于所有的 linux，包括 RedHat、SUSE、Debian 等发行版。  
2. 查看 centos 版本号 cat /etc/issue

```
[admin@oftpclient201 etc]$ cat /etc/issue
\S
Kernel \r on an \m

[admin@oftpclient201 etc]$
```

3. 使用 file /bin/ls

```
[admin@oftpclient201 etc]$ file /bin/ls
/bin/ls: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.32, BuildID[sha1]=c8ada1f7095f6b2bb7ddc848e088c2d615c3743e, stripped
[admin@oftpclient201 etc]$
```

> **为了方便大家在移动端也能看到我分享的博文，现已注册个人微信公众号，扫描左下方二维码即可，欢迎大家关注，有时间会及时分享相关技术博文。**  
> **为了方便大家互动讨论相关技术问题，现已组建专门的微信群，由于微信群满 100，请您扫描右下方宏哥个人微信二维码拉你进群（请务必备注：已关注公众号进群）平时上班忙（和你一样），所以加好友不及时，请稍安勿躁~，欢迎大家加入这个大家庭，我们一起畅游知识的海洋。**  
> 感谢您花时间阅读此篇文章, 如果您觉得这篇文章你学到了东西也是为了犒劳下博主的码字不易不妨打赏一下吧，让博主能喝上一杯咖啡，在此谢过了！  
> 如果您觉得阅读本文对您有帮助，请点一下左下角 **“推荐” **按钮，您的** “推荐”** 将是我最大的写作动力！另外您也可以选择 <**a h**ref="">【关注我】，可以很方便找到我！  
> 本文版权归作者和博客园共有，来源网址：[www.cnblogs.com/du-hong](https://link.juejin.cn?target=https%3A%2F%2Flink.zhihu.com%2F%3Ftarget%3Dhttps%253A%2F%2Fwww.cnblogs.com%2Fdu-hong "https://link.zhihu.com/?target=https%3A//www.cnblogs.com/du-hong") 欢迎各位转载，但是未经作者本人同意，转载文章之后必须在文章页面明显位置给出作者和原文连接，否则保留追究法律责任的权利！

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/fed31a8d9dda4764b7cee9e4b356e479~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/694948ea73fb4cd89f5b29604568dc0f~tplv-k3u1fbpfcp-zoom-in-crop-mark:1512:0:0:0.awebp)