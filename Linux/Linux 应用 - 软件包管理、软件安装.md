软件包管理器
------

Linux 系统是使用**软件包管理器**来进行软件的安装、卸载、查询等操作

软件包管理器又分为后端工具和前端工具：

*   **后端工具**：rpm，dpt  
    rpm：RedHat package Management，是 Linux 界的软件管理的一个工业标准  
    dpt：debian 的一个包管理器
*   **前端工具**：yum  
    基于后端工具的软件包管理，主要解决 rpm 安装软件时的依赖问题

> 具体如何解决？ yum 工具会根据你要安装的软件解析该软件的依赖树，然后把整个依赖树的软件从网上 (yum 库) 下载下来再安装。

依赖管理：

```shell
X ---> Y
X ---> Y ---> Z
X ---> Y ---> X
```

### rpm

rpm 常用命令：

```shell
# 安装一个包
rpm -ivh <包名>
	--nodeps
	--force

# 查询一个包是否被安装
rpm -q <软件名>
	-i # 得到被安装的包的信息
	-l # 列出该包中有哪些文件
	-f # 列出服务器上的一个文件或目录属于哪一个rpm包
	-a # 列出所有被安装的 npm package

# 卸载一个包
rpm -e <软件名>
```

### yum

yum 常用命令：

```shell
# 查看yum库中所有的包
yum list

# 安装命令
yum install <软件包>
	-y 确定安装

# 查看已经安装的软件包
yum list installed

# 卸载软件包
yum remove <软件名>

# 搜索对应的软件名称
yum search <软件名>
```

yum 远程仓库配置：

*   配置阿里云的仓库
    [https://developer.aliyun.com/mirror/centos](https://developer.aliyun.com/mirror/centos)
*   配置 163 的镜像仓库  
    [http://mirrors.163.com/.help/centos.html](http://mirrors.163.com/.help/centos.html)

软件的安装
-----

### jdk 1.8

1.  从官网上下载需要的 jdk（jdk-8u161-linux-x64.tar.gz）
    
2.  把文件上传到 /user/local/soft 目录
    
3.  解压缩文件：
    
    ```shell
    tar -zxvf /usr/local/soft/jdk-8u161-linux-x64.tar.gz -C /usr/local
    ```
    
4.  配置环境变量
    
    ```shell
    vi /etc/profile.d/jdk.sh
    ```
    
    ```shell
    #bin/bash
    export JAVA_HOME=/usr/local/jdk1.8.0_161
    export PATH=$JAVA_HOME/bin:$PATH # 后面跟上系统的环境变量
    ```
    
5.  重新加载配置文件
    
    ```shell
    source /etc/profile
    ```
    
6.  验证配置是否正确
    
    ```shell
    java -version
    ```
    

### mysql 5.7

1.  先将 postfix 和 mariadb-libs 卸载，不然会有依赖包冲突
    
    ```shell
    rpm -e postfix mariadb-libs
    ```
    
2.  安装 mysql 的依赖 net-tools 和 perl
    
    ```shell
    yum -y install net-tools perl
    ```
    
3.  安装 MySQL 的包
    
    ```shell
    cd /usr/local/soft/mysql5.7
    yum install -y *.rpm
    ```
    
4.  设置数据库开机启动shell
    
    ```shell
    systemctl enable mysqld
    ```
    
5.  启动 MySQL 服务
    
    ```shell
    systemctl start mysqld
    ```
    
6.  查看临时密码
    
    ```shell
    grep 'temporary password' /var/log/mysqld.log
    ```
    
7.  登陆账号修改密码
    
    ```shell
    mysql -u root -p 'xxxx'
    ALTER USER user() IDENTIFIED BY "Root_1234";
    ```
    
    修改完密码后 quit 退出，重新登陆
8.  开放远程登陆权限
    
    ```shell
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'Root_1234'  WITH GRANT OPTION;
    
    FLUSH PRIVILEGES; # 刷新
    ```
    
9.  开放 3306 端口
    
    ```shell
    firewall-cmd --zone=public --add-port=3306/tcp --permanent 
    firewall-cmd --reload
    ```
    

配置 Mysql：

1.  mysql 的安装配置文件：/etc/my.cnf
2.  启动错误日志查看：
    
    ```shell
    less /var/log/messages
    less /var/log/mysqld.log
    ```
    
3.  设置 utf8 字符集
    
    ```shell
    vi /etc/my.cnf
    ```
    
    在 [mysqld] 下面添加 character_set_server=utf8

### tomcat8

1.  解压二进制文件 apache-tomcat-8.5.53.tar.gz 到指定目录
    
    ```shell
    tar -zxvf /usr/local/soft/apache-tomcat-8.5.53.tar.gz -C /usr/local
    ```
    
2.  配置防火墙开放 8080 端口
    
    ```shell
    firewall-cmd --zone=public --add-port=8080/tcp --permanent 
    firewall-cmd --reload
    ```
    
3.  启动 tomcat
    
    ```shell
    /usr/local/apache-tomcat-8.5.53/bin/startup.sh
    ```
    
4.  查看启动日志
    
    ```shell
    tail -100f /usr/local/apache-tomcat-8.5.53/logs/catalina.out
    ```
    
5.  查看端口启动信息：
    
    ```shell
    netstat -ntpl
    ps -ef | grep tomcat
    ps -ef | grep tomcat | grep -v grep
    ```
    
6.  停止服务
    
    ```shell
    kill -9 进程ID
    
    /usr/local/apache-tomact-8.5.53/bin/shutdown.sh
    ```