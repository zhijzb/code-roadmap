# Linux基础命令



## Linux的目录结构

![image-20221027214128453](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027214128.png)

- `/`，根目录是最顶级的目录了
- Linux只有一个顶级目录：`/`
- 路径描述的层次关系同样适用`/`来表示
- /home/itheima/a.txt，表示根目录下的home文件夹内有itheima文件夹，内有a.txt



## ls命令

功能：列出文件夹信息

语法：`ls [-l -h -a] [参数]`

- 参数：被查看的文件夹，不提供参数，表示查看当前工作目录
- -l，以列表形式查看
- -h，配合-l，以更加人性化的方式显示文件大小
- -a，显示隐藏文件



### 隐藏文件、文件夹

在Linux中以`.`开头的，均是隐藏的。

默认不显示出来，需要`-a`选项才可查看到。



## pwd命令

功能：展示当前工作目录

语法：`pwd`



## cd命令

功能：切换工作目录

语法：`cd [目标目录]`

参数：目标目录，要切换去的地方，不提供默认切换到`当前登录用户HOME目录`



## HOME目录

每一个用户在Linux系统中都有自己的专属工作目录，称之为HOME目录。

- 普通用户的HOME目录，默认在：`/home/用户名`

- root用户的HOME目录，在：`/root`



FinalShell登陆终端后，默认的工作目录就是用户的HOME目录



## 相对路径、绝对路径

- 相对路径，==非==`/`开头的称之为相对路径

  相对路径表示以`当前目录`作为起点，去描述路径，如`test/a.txt`，表示当前工作目录内的test文件夹内的a.txt文件

- 绝对路径，==以==`/`开头的称之为绝对路径

  绝对路径从`根`开始描述路径



## 特殊路径符

- `.`，表示当前，比如./a.txt，表示当前文件夹内的`a.txt`文件
- `..`，表示上级目录，比如`../`表示上级目录，`../../`表示上级的上级目录
- `~`，表示用户的HOME目录，比如`cd ~`，即可切回用户HOME目录



## mkdir命令

功能：创建文件夹

语法：`mkdir [-p] 参数`

- 参数：被创建文件夹的路径
- 选项：-p，可选，表示创建前置路径
  - `mkdir -p /mkdir/mkdir`



## touch命令

功能：创建文件

语法：`touch 参数`

- 参数：被创建的文件路径



## cat命令

功能：查看文件内容

语法：`cat 参数`

- 参数：被查看的文件路径



## more命令

功能：查看文件，可以支持翻页查看

语法：`more 参数`

- 参数：被查看的文件路径
- 在查看过程中：
  - `空格`键翻页
  - `q`退出查看



## cp命令

功能：复制文件、文件夹

语法：`cp [-r] 参数1 参数2`

- 参数1，被复制的
- 参数2，要复制去的地方
- 选项：-r，可选，复制文件夹使用

示例：

- cp a.txt b.txt，复制当前目录下a.txt为b.txt
- cp a.txt test/，复制当前目录a.txt到test文件夹内
- cp -r test test2，复制文件夹test到当前文件夹内为test2存在



## mv命令

功能：移动文件、文件夹

语法：`mv 参数1 参数2`

- 参数1：被移动的
- 参数2：要移动去的地方，参数2如果不存在，则会进行改名



## rm命令

功能：删除文件、文件夹

语法：`rm [-r -f] 参数...参数`

- 参数：支持多个，每一个表示被删除的，空格进行分隔
- 选项：-r，删除**文件夹**使用
- 选项：-f，强制删除，不会给出确认提示，一般root用户会用

> rm命令很危险，一定要注意，特别是切换到root用户的时候。



## which命令

功能：查看命令的程序本体文件路径

语法：`which 参数`

- 参数：被查看的命令



## find命令

功能：搜索文件

语法1按文件名搜索：`find 路径 -name 参数`

- 路径，搜索的起始路径
- 参数，搜索的关键字，支持通配符*， 比如：`*`test表示搜索任意以test结尾的文件



## grep命令

功能：过滤关键字

语法：`grep [-n] 关键字 文件路径`

- 选项-n，可选，表示在结果中显示匹配的行的行号。
- 参数，关键字，必填，表示过滤的关键字，带有空格或其它特殊符号，建议使用””将关键字包围起来
- 参数，文件路径，必填，表示要过滤内容的文件路径，可作为内容输入端口



> 参数文件路径，可以作为管道符的输入



## wc命令

功能：统计

语法：`wc [-c -m -l -w] 文件路径`

- 选项，-c，统计bytes数量
- 选项，-m，统计字符数量
- 选项，-l，统计行数
- 选项，-w，统计单词数量
- 参数，文件路径，被统计的文件，可作为内容输入端口

默认输出：

行数、字数和单词数

> 参数文件路径，可作为管道符的输入



## 管道符|

写法：`|`

功能：将符号左边的结果，作为符号右边的输入

示例：

`cat a.txt | grep itheima`，将cat a.txt的结果，作为grep命令的输入，用来过滤`itheima`关键字



可以支持嵌套：

`cat a.txt | grep itheima | grep itcast`



## echo命令

功能：输出内容

语法：`echo 参数`

- 参数：被输出的内容



## `反引号

功能：被两个反引号包围的内容，会作为命令执行

示例：

- echo \`pwd\`，会输出当前工作目录



## tail命令

功能：查看文件尾部内容

语法：`tail [-f] 参数`

- 参数：被查看的文件
- 选项：-f，持续跟踪文件修改



## head命令

功能：查看文件头部内容

语法：`head [-n] 参数`

- 参数：被查看的文件
- 选项：-n，查看的行数



## 重定向符

功能：将符号左边的结果，输出到右边指定的文件中去

- `>`，表示覆盖输出
- `>>`，表示追加输出



## vi编辑器

### 命令模式快捷键

<img src="images/image-20230530154948236.png" alt="image-20230530154948236" style="zoom:50%;" />

![image-20221027215841573](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027215841.png)

![image-20221027215846581](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027215846.png)

![image-20221027215849668](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027215849.png)

### 底线命令快捷键

![image-20221027215858967](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027215858.png)



## 命令的选项

我们学习的一系列Linux命令，它们所拥有的选项都是非常多的。

比如，简单的ls命令就有：-a -A -b -c -C -d -D -f -F -g -G -h -H -i -I -k -l -L -m -n -N -o -p -q -Q -r-R -s -S -t -T -u -U -v -w -x -X -1等选项，可以发现选项是极其多的。

课程中， 并不会将全部的选项都进行讲解，否则，一个ls命令就可能讲解2小时之久。

课程中，会对常见的选项进行讲解， 足够满足绝大多数的学习、工作场景。



### 查看命令的帮助

可以通过：`命令 --help`查看命令的帮助手册

![image-20221027220005610](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027220005.png)

### 查看命令的详细手册

可以通过：`man 命令`查看某命令的详细手册

![image-20221027220009949](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027220010.png)





# Linux常用操作

## 软件安装

- CentOS系统使用：
  - yum [install remove search] [-y] 软件名称
    - install 安装
    - remove 卸载
    - search 搜索
    - -y，自动确认
- Ubuntu系统使用
  - apt [install remove search] [-y] 软件名称
    - install 安装
    - remove 卸载
    - search 搜索
    - -y，自动确认

> yum 和 apt 均需要root权限



## systemctl

功能：控制系统服务的启动关闭等

语法：`systemctl start | stop | restart | disable | enable | status 服务名`

- start，启动
- stop，停止
- status，查看状态
- disable，关闭开机自启
- enable，开启开机自启
- restart，重启



## 软链接

功能：创建文件、文件夹软链接（快捷方式）

语法：`ln -s 参数1 参数2`

- 参数1：被链接的
- 参数2：要链接去的地方（快捷方式的名称和存放位置）



## 日期

语法：`date [-d] [+格式化字符串]`

- -d 按照给定的字符串显示日期，一般用于日期计算

- 格式化字符串：通过特定的字符串标记，来控制显示的日期格式
  - %Y   年%y   年份后两位数字 (00..99)
  - %m   月份 (01..12)
  - %d   日 (01..31)
  - %H   小时 (00..23)
  - %M   分钟 (00..59)
  - %S   秒 (00..60)
  - %s   自 1970-01-01 00:00:00 UTC 到现在的秒数



示例：

- 按照2022-01-01的格式显示日期

  ![image-20221027220514640](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027220514.png)

- 按照2022-01-01 10:00:00的格式显示日期

  ![image-20221027220525625](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027220525.png)

- -d选项日期计算

  ![image-20221027220429831](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027220429.png)

  - 支持的时间标记为：

    ![image-20221027220449312](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027220449.png)





## 时区

修改时区为中国时区

![image-20221027220554654](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027220554.png)



## ntp

功能：同步时间

安装：`yum install -y ntp`

启动管理：`systemctl start | stop | restart | status | disable | enable ntpd`



手动校准时间：`ntpdate -u ntp.aliyun.com`



## ip地址

格式：a.b.c.d

- abcd为0~255的数字



特殊IP：

- 127.0.0.1，表示本机
- 0.0.0.0
  - 可以表示本机
  - 也可以表示任意IP（看使用场景）



查看ip：`ifconfig`



## 主机名

功能：Linux系统的名称

查看：`hostname`

设置：`hostnamectl set-hostname 主机名`



## 配置VMware固定IP

1. 修改VMware网络，参阅PPT，图太多

2. 设置Linux内部固定IP

   修改文件：`/etc/sysconfig/network-scripts/ifcfg-ens33`

   示例文件内容：

   ```shell
   TYPE="Ethernet"
   PROXY_METHOD="none"
   BROWSER_ONLY="no"
   BOOTPROTO="static"			# 改为static，固定IP
   DEFROUTE="yes"
   IPV4_FAILURE_FATAL="no"
   IPV6INIT="yes"
   IPV6_AUTOCONF="yes"
   IPV6_DEFROUTE="yes"
   IPV6_FAILURE_FATAL="no"
   IPV6_ADDR_GEN_MODE="stable-privacy"
   NAME="ens33"
   UUID="1b0011cb-0d2e-4eaa-8a11-af7d50ebc876"
   DEVICE="ens33"
   ONBOOT="yes"
   IPADDR="192.168.88.131"		# IP地址，自己设置，要匹配网络范围
   NETMASK="255.255.255.0"		# 子网掩码，固定写法255.255.255.0
   GATEWAY="192.168.88.2"		# 网关，要和VMware中配置的一致
   DNS1="192.168.88.2"			# DNS1服务器，和网关一致即可
   ```



## ps命令

功能：查看进程信息

语法：`ps -ef`，查看全部进程信息，可以搭配grep做过滤：`ps -ef | grep xxx`



## kill命令

![image-20221027221303037](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221303.png)



## nmap命令

![image-20221027221241123](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221241.png)



## netstat命令

功能：查看端口占用

用法：`netstat -anp | grep xxx`



## ping命令

测试网络是否联通

语法：`ping [-c num] 参数`

![image-20221027221129782](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221129.png)



## wget命令

![image-20221027221148964](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221149.png)

## curl命令

![image-20221027221201079](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221201.png)

![image-20221027221210518](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221210.png)



## top命令

功能：查看主机运行状态

语法：`top`，查看基础信息



可用选项：

![image-20221027221340729](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221340.png)



交互式模式中，可用快捷键：

![image-20221027221354137](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221354.png)



## df命令

查看磁盘占用

![image-20221027221413787](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221413.png)



## *iostat命令

查看CPU、磁盘的相关信息

![image-20221027221439990](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221440.png)

![image-20221027221514237](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221514.png)



## *sar命令

查看网络统计

![image-20221027221545822](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221545.png)



## 环境变量

- 临时设置：export 变量名=变量值
- 永久设置：
  - 针对用户，设置用户HOME目录内：`.bashrc`文件
  - 针对全局，设置`/etc/profile`



### PATH变量

记录了执行程序的搜索路径

可以将自定义路径加入PATH内，实现自定义命令在任意地方均可执行的效果



## $符号

可以取出指定的环境变量的值

语法：`$变量名`

示例：

`echo $PATH`，输出PATH环境变量的值

`echo ${PATH}ABC`，输出PATH环境变量的值以及ABC

如果变量名和其它内容混淆在一起，可以使用${}





## 压缩解压

### 压缩

`tar -zcvf 压缩包 被压缩1...被压缩2...被压缩N`

- -z表示使用gzip，可以不写



`zip [-r] 参数1 参数2 参数N`

![image-20221027221906247](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221906.png)



### 解压

`tar -zxvf 被解压的文件 -C 要解压去的地方`

- -z表示使用gzip，可以省略
- -C，可以省略，指定要解压去的地方，不写解压到当前目录



`unzip [-d] 参数`

![image-20221027221939899](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027221939.png)





## su命令

切换用户

语法：`su [-] [用户]`

![image-20221027222021619](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027222021.png)



## sudo命令

![image-20221027222035337](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027222035.png)



比如：

```shell
itheima ALL=(ALL)       NOPASSWD: ALL
```

在visudo内配置如上内容，可以让itheima用户，无需密码直接使用`sudo`



## chmod命令

修改文件、文件夹权限



语法：`chmod [-R] 权限 参数`

- 权限，要设置的权限，比如755，表示：`rwxr-xr-x`

  ![image-20221027222157276](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027222157.png)

- 参数，被修改的文件、文件夹

- 选项-R，设置文件夹和其内部全部内容一样生效

```
chmod +r /file
chmod -r /file
```

## chown命令

修改文件、文件夹所属用户、组



语法：`chown [-R] [用户][:][用户组] 文件或文件夹`

![image-20221027222326192](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027222326.png)



## 用户组管理

![image-20221027222354498](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027222354.png)



## 用户管理

![image-20221027222407618](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027222407.png)



## genenv命令

- `getenv group`，查看系统全部的用户组

  ![image-20221027222446514](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027222446.png)

- `getenv passwd`，查看系统全部的用户

  ![image-20221027222512274](https://image-set.oss-cn-zhangjiakou.aliyuncs.com/img-out/2022/10/27/20221027222512.png)



## *env命令

查看系统全部的环境变量

语法：`env`

# 补充

![image-20230530153637767](images/image-20230530153637767.png)![image-20230530153928901](images/image-20230530153928901.png)

## 文件类型

| 文件类型 | 描述           | 相同点                        | 不同点                                                   |
| -------- | -------------- | ----------------------------- | -------------------------------------------------------- |
| 普通文件 | 存储数据       | 有文件名和i节点号             | 内容为数据                                               |
| 目录文件 | 存储目录项     | 有文件名和i节点号             | 内容为目录项或文件名与节点对应表                         |
| 设备文件 | 不占用磁盘空间 | 有文件名和i节点号，与驱动对应 | 不占用磁盘空间，通过其节点信息可建立与内核驱动程序的联系 |

## tar

`c` 表示 "create"，即创建新的 tar 文件；

`x` 表示 "extract"，即释放（解开）一个 tar 包并将其还原回原本的文件、目录（变量）；

## 用户与用户组

1. 创建用户

useradd tom

不指定组，默认创建同名组

2. 删除用户

userdel -r tom

3. 修改用户

usermod -l root tom

4. 创建用户组：可以使用“groupadd”命令来创建用户组。例如，以下命令将创建名为“dev”的用户组：

```shell
groupadd dev
```

5. 删除用户组：可以使用“groupdel”命令来删除用户组。例如，以下命令将删除名为“dev”的用户组：

```shell
groupdel dev
```

6. 将用户添加到用户组：可以使用“usermod”命令将用户添加到用户组。例如，以下命令将名为“tom”的用户添加到用户组“dev”：

```shell
usermod -aG dev tom
```

7. 从用户组中删除用户：可以使用“gpasswd”命令从用户组中删除用户。例如，以下命令将名为“tom”的用户从用户组“dev”中删除：

```shell
gpasswd -d tom dev
```



## vim

:set nu

移动到58行，向右移动10个字符

58G;10I

复原

:u

复制65到75行的内容

:65,73y

光标移到最后一行

`p`  复制



## 重定向符

`>`  覆盖

`>>` 追加





## shell编程

### 示例

1、删除10个账号的shell程序：

```shell
#!/bin/bash
for i in {1..10}
do
    userdel -r us$i\
done
echo "The accounts have been deleted."
```

2、传统方式的九九乘法表：

```shell
#!/bin/bash
for i in {1..9}
do
    for j in {1..9}
    do
        echo -n "$i*$j=$(($i*$j))"
    done
echo ""   
done
```

3、检查密码的脚本：

```shell
#!/bin/bash
pw="123456"
count=0
while [ $count -lt 3 ]
do
    read -p "please input your password: " pwd
    if [ $pwd = $pw ]
    then
        echo "you are right,ok"
        exit 0
    else
        echo "password is incorrect. Please try again."
        count=`expr $count + 1`
    fi
done
echo "Too many attempts. Exiting script."
exit 1
```

这三个脚本都可以在Linux终端中运行，并根据需要进行修改。第一个脚本可以删除指定的账号，第二个脚本可以显示传统方式的九九乘法表，第三个脚本可以检查密码并计数，如果输入错误密码超过三次，则退出脚本。



-  用Shell编程，判断一文件是不是只读文件，如果是将其拷贝到 /dev 目录下。

```shell
#!/bin/bash

filename=$1

if [ -r $filename ]
then
    echo "$filename is readable"
    cp $filename /dev
    echo "Copied $filename to /dev"
else
    echo "$filename is not readable"
fi
```



- 设计一个shell程序，添加一个新组为keji，然后添加属于这个组的30个用户，用户名的形式为usxx，其中xx从01到30。

![img](images/wps2.jpg)

## 条件表达式

在 Shell 中，条件表达式用于在脚本中对条件进行判断和处理。在 Shell 脚本中，条件表达式通常使用方括号([ ])或者双括号(( ))括起来。

常见的条件表达式有以下几种：

- 字符串比较：使用 = 或者 != 进行比较。例如，[ "$str1" = "$str2" ] 表示判断 str1 是否等于 str2。

- 数值比较：使用 -eq、-ne、-gt、-ge、-lt、-le 进行比较。例如，[ $num1 -gt  $num2 ] 表示判断 num1 是否大于 num2。

- 文件类型判断：使用 -f、**-d**、-e、-r、-w、-x 等参数判断文件的类型及权限。

  ```shell
  if [ -f test.txt ]
  then
      echo "test.txt 存在且为普通文件"
  fi
  
  if [ -d mydir ]
  then
      echo "mydir 存在且为目录"
  fi
  
  if [ -e test.txt ]; then
      echo "test.txt 存在"
  fi
  
  if [ -r test.txt ]; then
      echo "test.txt 存在且可读"
  fi
  
  if [ -w test.txt ]; then
      echo "test.txt 存在且可写"
  fi
  
  if [ -x test.sh ]; then
      echo "test.sh 存在且可执行"
  fi
  
  ```

  

- 逻辑运算：使用 &&、||、! 等运算符进行逻辑运算。

- 高级运算：使用正则表达式=~ 进行匹配，使用 (( )) 进行数学运算。

例如，可以使用下面的语句判断一个字符串是否为空：

```shell
if [ -z "$str" ]; then
    echo "The string is empty."
else
    echo "The string is not empty."
fi
```

如果 str 字符串为空，则输出"The string is empty."，否则输出"The string is not empty."。



以下脚本来测试 `$HOME` 目录下的 `retesting` 文件是否可写：

```bash
#!/bin/bash

if [[ -d $HOME && -w $HOME/retesting ]]; then
  echo "The file exists and you can write to it"
else
  echo "I can't write to it"
fi
```

## 条件语句

在 Shell 中，可以使用条件语句来实现根据不同的条件执行不同的操作。

条件语句包括：

1. if 语句：根据一个条件判断是否执行某个语句块。if 语句的基本结构为：

```
if [ condition ]
then
    command
fi
```

其中，`condition` 为判断条件，`command` 为要执行的命令或语句。

2. if-else 语句：根据一个条件判断是否执行某个语句块或另一个语句块。if-else 语句的基本结构为：

```
if [ condition ]
then
    command1
else
    command2
fi
```

其中，`condition` 为判断条件，`command1` 为条件成立时要执行的命令或语句，`command2` 为条件不成立时要执行的命令或语句。

3. if-elif-else 语句：根据多个条件判断是否执行不同的语句块。if-elif-else 语句的基本结构为：

```
if [ condition1 ]
then
    command1
elif [ condition2 ]
then
    command2
else
    command3
fi
```

其中，`condition1`、`condition2` 为不同的判断条件，`command1`、`command2` 为条件成立时要执行的命令或语句，`command3` 为所有条件都不成立时要执行的命令或语句。

在条件语句中，可以使用多种比较符号和逻辑运算符，例如：`-eq`（等于）、`-ne`（不等于）、`-lt`（小于）、`-gt`（大于）、`-le`（小于等于）、`-ge`（大于等于）、`-a`（逻辑与）、`-o`（逻辑或）等。



### `case` 命令

是用于比较多个值的 Shell 控制结构之一，可以用于替换 `if` 命令中的多个嵌套条件判断语句，使代码更加简洁易懂。

`case` 的语法格式如下：

```bash
case 值 in
模式1)
  命令序列1
  ;;
模式2)
  命令序列2
  ;;
模式3)
  命令序列3
  ;;
*)
  命令序列n
  ;;
esac
```

`值` 是要比较的值，可以是常量，变量，表达式等。`模式` 是一个匹配表达式，可以使用 `*` 通配符匹配任意字符。

当匹配到第一个符合条件的 `模式` 时，该模式对应的命令序列将被执行，并且会跳出 `case` 语句。如果匹配不到任何符合条件的 `模式`，则会执行 `*` 对应的命令序列。

下面是一个 `case` 命令的示例，用于判断输入的数字是奇数还是偶数：

```bash
echo "Please enter a number:"
read num

case $num in
  [0-9]*)
    # 使用正则表达式匹配输入的数字
    # 如果匹配成功，继续判断是否为偶数
    case $((num % 2)) in
      0)
        echo "$num is an even number."
        ;;
      1)
        echo "$num is an odd number."
        ;;
    esac
    ;;
  *)
    # 输入不是数字时输出错误提示
    echo "Please enter a valid number."
    ;;
esac
```

在这个例子中，如果输入的是一个数字，`[0-9]*` 这个正则表达式会匹配成功，继而使用内部的 `case` 命令判断输入的数字是

## 循环语句

循环语句可以让我们重复执行相同或类似的操作。在 Shell 中，常见的循环语句有 for 循环和 while 循环。

**for 循环**可以在给定的列表或序列中循环，每次迭代执行一次循环体中的命令。语法如下：

```sh
for variable in list
do
  commands
done
```

其中，变量 `variable` 会依次取 `list` 中的值，循环体中的命令会在每个值上执行一次。例如：

```shell
for i in 1 2 3
do
  echo $i
done
```

输出结果为：

```
1
2
3
```



**while 循环**会一直循环，直到条件不满足为止。语法如下：

```shell
while condition
do
  commands
done
```

其中，`condition` 是一个布尔表达式，只要其返回值为真，就会一直执行循环体中的命令。例如：

```shell
i=1
while [ $i -le 5 ]
do
  echo "The value of i is: $i"
  i=$((i+1))
done
```

输出结果为：

```shell
The value of i is: 1
The value of i is: 2
The value of i is: 3
The value of i is: 4
The value of i is: 5
```

这个例子展示了如何使用 while 循环和条件语句来构建一个累加器。在循环体中，我们输出 `$i` 的值，然后将其加 1。当 `i` 的值达到 5 时，循环结束。















