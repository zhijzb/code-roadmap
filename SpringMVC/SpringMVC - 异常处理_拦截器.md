# SpringMVC - 异常处理_拦截器

Gitee 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/03_SpringMVC](https://gitee.com/szluyu99/mj_java_frame/tree/master/03_SpringMVC)

异常处理
----

[SpringMVC](https://so.csdn.net/so/search?q=SpringMVC&spm=1001.2101.3001.7020) 可以对异常信息作统一的处理，主要有 4 种方式：

1.  使用 SpringMVC 自带的异常处理类：`SimpleMappingExceptionResolver`
2.  自定义异常处理类：实现 `HandlerExceptionResolver` 接口
3.  使用注解 `@ExceptionHandler`
4.  使用注解 `@ExceptionHandler` + `@ControllerAdvice`

### 简单的异常与页面映射 - SimpleMappingExceptionResolver

这种方法简单的将某个异常与页面作映射，并且只能将异常对象传给页面（无法传其他的数据）

```xml
<bean class="org.springframework.web.servlet.handler.SimpleMappingExceptionResolver">
    <!-- 出现异常时，转发到对应的页面 -->
    <property >
        <props>
            <prop key="java.lang.ArithmeticException">/WEB-INF/page/error/arthmetic.jsp</prop>
            <prop key="java.io.IOException">/WEB-INF/page/error/io.jsp</prop>
            <prop key="java.lang.ClassNotFoundException">/WEB-INF/page/error/not.jsp</prop>
        </props>
    </property>
    <!-- 异常对象的属性名(可以通过EL表达式访问) -->
    <property />
    <!-- 其他异常统一转发的页面 -->
    <property />
</bean>
```

### √自定义异常处理类 - 实现 HandlerExceptionResolver 接口

这种方法比 SimpleMappingExceptionResolver 强大了不少，不止是简单的将异常与某个页面做映射，可以做更多复杂的操作与传输数据

```java
public class MyExceptionResolver implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response,
                                         Object handler,
                                         Exception ex) {
		// 可以获取到产生异常的类与方法
		// HandlerMethod method = (HandlerMethod) handler;
		// method.getBean();
		// method.getMethod();

        ModelAndView mv = new ModelAndView();
        mv.addObject("ex", ex);
        mv.setViewName("/WEB-INF/page/error/default.jsp");
        return mv;
    }
}
```

将该类加载到 IoC 容器当中：

```xml
<bean class="com.mj.handler.MyExceptionResolver"/>
```

> 如果配置了 component-scan，也可以直接在类上使用 `@Component` 注解

### @ExceptionHandler

`@ExceptionHandler` 标注的方法，与出现异常的方法，必须处在同一个 controller 中

*   **该方法并不常用**，只能处理某一个 controller 中的异常

```java
@Controller
public class ExceptionController {
	// 【出现异常的方法】
    @RequestMapping("/test1")
    public void test1() {
        throw new ArithmeticException("test1");
    }

    @RequestMapping("/test2")
    public void test2() throws Exception {
        throw new ClassNotFoundException("test2");
	}

	// 【处理异常的方法】
	// 处理指定的异常
	@ExceptionHandler({ArithmeticException.class, IOException.class})
	public ModelAndView resolveException1(Exception ex) {
	    ModelAndView mv = new ModelAndView();
	    mv.addObject("ex", ex);
	    mv.setViewName("/WEB-INF/page/error/runtime.jsp");
	    return mv;
	}
	
	// 处理默认的异常
	@ExceptionHandler
	public ModelAndView resolveException2(Exception ex) {
	    ModelAndView mv = new ModelAndView();
	    mv.addObject("ex", ex);
	    mv.setViewName("/WEB-INF/page/error/default.jsp");
	    return mv;
	}
}
```

### √@ExceptionHandler + @ControllerAdvice

使用 `@ControllerAdvice` 标识的类可以去处理所有的异常（可以指定处理范围）

`@ControllerAdvice` 可以通过属性设置它的处理范围

*   basePackages、basePackageClasses、assignableTypes、annotations

```java
@ControllerAdvice(basePackages = "com.mj.controller")
public class MyExceptionResolver {
    @ExceptionHandler({ArithmeticException.class, IOException.class})
    public ModelAndView resolveException1(Exception ex) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("ex", ex);
        mv.setViewName("/WEB-INF/page/error/runtime.jsp");
        return mv;
    }

    @ExceptionHandler
    public ModelAndView resolveException2(Exception ex) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("ex", ex);
        mv.setViewName("/WEB-INF/page/error/default.jsp");
        return mv;
    }
}
```

[拦截器](https://so.csdn.net/so/search?q=%E6%8B%A6%E6%88%AA%E5%99%A8&spm=1001.2101.3001.7020) (Interceptor)
--------------------------------------------------------------------------------------------------------

> 如：
>
> - 防止重复提交拦截器(ruoyi)
> - 

**拦截器 (Interceptor)** 的功能，跟 [过滤器 (filter)](https://blog.csdn.net/weixin_43734095/article/details/119277503) 有点类似，但是有本质区别的

**过滤器**：是 Servlet 规范的一部分

*   能拦截任意请求，在请求抵达 Servlet 之前、响应客户端之前进行拦截
*   常用于：编码设置、登陆校验

**拦截器**：是 SpringMVC 的一部分

*   只能拦截 DispatcherServlet 拦截到的内容，一般用来拦截 controller
*   常用于：抽取 controller 的公共代码

### 实现 HandlerInterceptor 接口

```java
public class MyInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object handler,
                           ModelAndView modelAndView) throws Exception { }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler,
                                Exception ex) throws Exception { }
}
```

### 配置拦截器

实现 HandlerInterceptor 接口的对象是一个拦截器对象，但是仅仅有拦截器对象是不够的，Spring 并不知道它的存在，需要将它配置到 IoC 容器中：

```xml
<mvc:interceptors>
    <mvc:interceptor>
    	<!-- 需要拦截的路径(可以写多个) -->
    	<!-- **代表当前目录下的所有内容(包括子目录) -->
        <mvc:mapping path="/**"/>
        <!-- 排除asset目录的所有内容 -->
        <mvc:exclude-mapping path="/asset/**"/>
        <!-- 拦截器对象 -->
        <bean class="com.mj.interceptor.MyInterceptor"/>
    </mvc:interceptor>
</mvc:interceptors>
```

### 方法解析 preHandle、postHadnle、afterCompletion

**preHandle**：在 controller 的处理方法之前调用

*   一般在这里进行初始化、请求预处理操作
*   如果返回 false，则不会再调用 controller 的处理方法、postHandle、afterCompletion
*   当有多个拦截器时，这个方法按照正序执行

**postHandle**：在 controller 的处理方法之后、在 DispatcherServlet 进行视图渲染之前调用

*   一般在这里进行请求后续加工处理操作
*   当有多个拦截器时，这个方法按照`逆序`执行

**afterCompletion**：在 DispatcherServlet 进行`视图渲染之后调用`

*   一般在这里进行资源回收操作
*   当有多个拦截器时，这个方法按照`逆序`执行

### SpringMVC 的执行流程 - 源码跟踪

① FrameworkServlet - service  
② HttpServlet - service  
③ FrameworkServlet - doGet  
④ FrameworkServlet - processRequest  
⑤ DispatcherServlet - doService  
⑥ DispatcherServlet - doDispatch

<img src="https://img-blog.csdnimg.cn/feafd55b1514487c98509fd61096093f.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70" style="zoom:50%;" />

#### doDispatch 核心流程

`mappedHandler = getHandler`：获取 [Handler](https://so.csdn.net/so/search?q=Handler&spm=1001.2101.3001.7020)（用来处理 controller、拦截器）!!!

`ha = getHandlerAdapter`：获取 Handle

`mapperHandler.applyPreHandle`：调用拦截器的 preHandle 方法

`mv = ha.handle`：调用 controller 的处理方法

*   如果返回的不是视图页面（比如是 JSON 数据），在这个步骤中就已经将数据写会给客户端

`mapperHandler.applyPostHandle`：调用拦截器诶 postHandle 方法

`processDispatchResult`：处理异常、渲染视图（转发、重定向）

`triggerAfterCompletion`：调用拦截器的 afterCompletion 方法
