# SpringMVC - 返回值_自定义
Gitee 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/03_SpringMVC](https://gitee.com/szluyu99/mj_java_frame/tree/master/03_SpringMVC)

controller 中的返回值
----------------

### 无返回值 - 使用原始的 [Servlet](https://so.csdn.net/so/search?q=Servlet&spm=1001.2101.3001.7020) 的方法

如果返回值为 void，则和以前操作 [Servlet](https://blog.csdn.net/weixin_43734095/article/details/118979055) 的方式相同，使用 response 完成相关操作

```java
@RequestMapping("/void")
public void testVoid(HttpServletRequest request,
                     HttpServletResponse response) throws Exception {
    response.setContentType("text/html; charset=UTF-8");
    response.getWriter().write("MJ666");
}
```

### 普通文本、HTML - 设置响应头 Content-Type

主要是利用 `@RequestMapping` 的 produces 属性，本质上是设置响应头 Content-Type

```java
@RequestMapping(
		value = "/plainText",
		produces = "text/plain; charset=UTF-8"		
)
@ResponseBody
public String plainText() {
	return "this is plainText";
}
```

```java
@RequestMapping(
		value = "/html",
		produces = "html/plain; charset=UTF-8"		
)
@ResponseBody
public String html() {
	return "<h1>This is MJ</h1>";
}
```

### XML - 推荐使用 jaxb

方法 1（不推荐）：直接返回字符串拼接的 XML  
![](https://img-blog.csdnimg.cn/a03f3ddb839b44758f459d7a8968361a.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
以上方法的本质是返回值为 String 字符串，因此解决乱码问题在 `@RequestMapping` 中设置即可

**方法 2（推荐）**：利用 **jaxb** 将 [Model](https://so.csdn.net/so/search?q=Model&spm=1001.2101.3001.7020) 对象转为 XML

> jaxb 与 SpringMVC 集成的很好，推荐使用

```
<!-- Model转XML字符串 -->
<dependency>
    <groupId>javax.xml.bind</groupId>
    <artifactId>jaxb-api</artifactId>
    <version>2.4.0-b180830.0359</version>
</dependency>

<dependency>
    <groupId>javax.xml</groupId>
    <artifactId>jaxb-impl</artifactId>
    <version>2.1</version>
</dependency>
```

> 必须使用 <mvc:annotation-driven>

使用 `@XmlRootElement`、`@XmlElement`、`@XmlAttribute` 标识如何将 Model 转为 XML  
![](https://img-blog.csdnimg.cn/949d4032779841c19d5ed320cab696c8.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
在控制器中返回 Model 对象，会自动转为 XML 格式的数据：  
![](https://img-blog.csdnimg.cn/04a8c29ed38e48149a1040d12245ca61.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
利用该方法，要解决乱码问题，需要通过 Jaxb2RootElementHttpMessageConverter 设置编码

```
<mvc:annotation-driven>
    <mvc:message-converters>
        <!-- 影响返回值是Model对象（最后通过JAXB转成XML字符串） -->
        <bean class="org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter">
            <property />
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```

### ==JSON - 框架中已经集成 jackson==

方法 1（不推荐）：直接返回字符串拼接的 JSON  
![](https://img-blog.csdnimg.cn/1926fb74b46d4573bcc4c93a978d0268.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
**方法 2（推荐）**：使用 Jackson（SpringMVC 中已经集成，只需要引入依赖即可启用）

```xml
<!-- Model转JSON字符串 -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.11.0</version>
</dependency>
```

在 Maven 中引入依赖后，在 controller 中`返回的 Model 对象就会自动转成 JSON 格式`

```java
@RequestMapping("/json2")
@ResponseBody
public Student json() {
	Student student = new Student();
	student.setAge(20);
	student.setName("MJ");
	return Student;
}
```

利用该方法，要解决乱码问题，需要通过` MappingJackson2HttpMessageConverter `设置编码

```xml
<mvc:annotation-driven>
    <mvc:message-converters>
        <!-- 影响返回值是Model对象（最后通过Jackson转成JSON字符串） -->
        <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            <property />
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```

jsp 相关
------

### 转发 - ModelAndView、String

可以利用 ModelAndView 将数据和视图绑定到一起，返回给客户端

```
@RequestMapping("/mv1")
public ModelAndView mv1(){
	// 创建ModelAndView并设置要转发的页面
    ModelAndView mv = new ModelAndView("/person.jsp");
    // mv.setViewName("/page/jsp1.jsp"); // 也可以这么写
    
    Person person = new Person("MJ", 20);
    request.setAttribute("person", person);
    
    // 本质就是request.setAttribute()
    mv.addObject("person", person);
    return mv;
}
```

不加 `@ResponseBody` 的 String 返回值，也代表 viewName

```
@RequestMapping("/mv3")
public ModelAndView mv3(){    
	Person person = new Person("MJ", 20);
    request.setAttribute("person", person);
    return "/jsp3.jsp";
}
```

### 路径问题总结（Java、Html）

思考：前面的代码中将 /person.jsp 改成 person.jsp 结果会有什么不同？

在 Java 代码中，路径问题总结：

```
1.假设请求路径是："http://IP地址:端口/context_path/path1/path2/path3"
2.假设转发路径是："/page/test.jsp"
    1> 以斜线（/）开头，参考路径是context_path
    2> 所以最终转发路径是："http://IP地址:端口/context_path" + "/page/test.jsp"
3.假设转发路径是："page/test.jsp"
    1> 不以斜线（/）开头，参考路径是当前请求路径的上一层路径
    2> 所以最终转发路径是："http://IP地址:端口/context_path/path1/path2/" + "page/test.jsp"
```

在 jsp、html 代码中，路径问题总结：

```
1.假设请求路径是："http://IP地址:端口/context_path/path1/path2/path3"
2.假设跳转路径是："/page/test.jsp"
    1> 以斜线（/）开头，参考路径是"http://IP地址:端口"
    2> 所以最终转发路径是："http://IP地址:端口" + "/page/test.jsp"
```

### [重定向](https://so.csdn.net/so/search?q=%E9%87%8D%E5%AE%9A%E5%90%91&spm=1001.2101.3001.7020) - “redirect:”

在 viewName 前面加上 `"redirect:"` 表示重定向

通过返回 String 进行重定向：

```
@RequestMapping("/jsp3")
public String jsp3(){
    return "redirect:/page/person.jsp";
}
```

通过 ModelAndView 进行重定向：

```
@RequestMapping("/jsp4")
public ModelAndView jsp4(){
    ModelAndView mv = new ModelAndView();
    // 设置数据
    mv.addObject("name", "Jack");
    mv.addObject("price", 666);
    // 设置页面
    mv.setViewName("redirect:/page/jsp4.jsp?test=10");
    return mv;
}
```

参考源码：org.springframework.web.servlet.view.UrlBasedViewResolver  
![](https://img-blog.csdnimg.cn/3b63afa001944f3aa6c5f88a4d2d1e96.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
SpringMVC 默认会将 ModelAndView 对象 addObject 的内容，通过请求参数的形式传递给重定向的页面

> 一些简单的数据（String、int 等）会自动转化为请求参数，像 Model 对象无法直接作为请求参数

*   在重定向的页面中可以通过 `request.getParameter` 或 `${param]}` 获取请求参数

### mvc:view-controller - 配置请求路径和 viewName

可以使用 <mvc:view-controller> 直接配置请求路径和 viewName

```
<mvc:view-controller path="/mv5" view- />
```

*   当没有 controller 处理这个 path 时，才会交给 <mvc:view-controller> 去处理

使用了 <mvc:view-controller> 后，需要加上 <mvc:annotation-drive>

*   否则会导致 controller 无法处理请求

### InternalResourceViewResolver - 设置视图的公共前缀、后缀

可以通过 InternalResourceViewResolver 设置视图路径的公共前缀、后缀

```
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
    <!-- 前缀 -->
    <property />
    <!-- 后缀 -->
    <property />
</bean>
```

受 InternalResourceViewResolver 影响的有：

*   通过返回值 ModelAndView 设置的 viewName
*   通过返回值 String 设置的 viewName
*   通过 <mvc:view-controller> 设置的 viewName

可以配置多个 InternalResourceViewResolver，order 越小，优先级越高

> 注意，并不是从优先级高的开始依次往后找，优先级最高的那个找不到就不会再去找其他
> 
> *   解决方案是自定义 InternalResourceViewResolver（后面有）

```
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
    <!-- order越小,优先级越高 -->
    <property />
    <!-- 前缀 -->
    <property />
    <!-- 后缀 -->
    <property />
</bean>
```

#### 忽略 InternalResourceViewResolver

有的时候配置了公共前缀、后缀，但是某个视图希望不使用该后缀，解决方案：

*   方法 1：在 viewName 前面加上 `"forward:"` 或 `"redirect:"`
*   方法 2：通过 ModelAndView 的 setView 方法

```
// 转发
view.setView(new InternalResourceView("/person.jsp"));
view.setView(new JstlView("/person.jsp"));
// 重定向
view.setView(new RedirectView("/person.jsp"));
```

实际上，之前通过返回值 String、ModelAndView 设置 viewName 之后

*   SpringMVC 内部会根据具体情况创建对应的 View 对象  
    InternalResourceView、JstlView、RedirectView

InternalResourceViewResolver 影响的是：没有带 `"forward:"`、`redirect:` 的 viewName

#### 自定义 InternalResourceViewResolver

> 自定义 InternalResourceViewResolver 主要是重写：checkResource 方法，因为源码中该方法必然返回 True，哪怕找不到资源也会返回 True，所以哪怕配置多个 InternalResourceViewResolver 并通过 order 指定优先级，也只会执行优先级最高的然后直接返回 True  
> ![](https://img-blog.csdnimg.cn/25d2c5d028c644f586a866f7f13d2a69.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

通过自定义 InternalResourceViewResolver，可以实现：

*   当一个 InternalResourceViewResolver 拼接的路径找不到时
*   就尝试下一个 order 比较大的 InternalResourceViewResolver

```
public class MyView extends InternalResourceView {
    @Override
    public boolean checkResource(Locale locale) throws Exception {
        // 根据实际情况来返回
        // 存在, 返回true
        // 不存在, 返回false

        // 项目部署的根路径 + /page/index.jsp
        // String path = getServletContext().getRealPath("/") + getUrl();
        String path = getServletContext().getRealPath(getUrl());
        File file = new File(path);
        return file.exists();
    }
}
```

```
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
    <!-- 自定义InternalResourceViewResolver,没有找到资源则执行其他 -->
    <property />
    <property />
    <property />
    <property />
</bean>

<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
     <!-- 最后一次还是需要交由默认处理,无论实际资源是否存在都返回True -->
    <property />
    <property />
    <property />
</bean>
```

### @ResponseStatus - 设置响应码

可以通过 `@ResponseStatus` 设置响应码

```
@RequestMapping("/test1")
@ResponseStatus(
		value = HttpStatus.BAD_REQUEST,
		reason = "你这个错误666啊"
)
public String test1() throws Exception {
	return "/index.jsp";		
}
```
