## SpringMVC - 简介

SpringMVC 的正式名称是 Spring Web MVC

是属于Spring框架的一部分

是基于Servlet API的Web框架

> 核心功能：拦截和处理客户端的请求

[官方参考文档](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html)

SpringMVC - 入门
--------------

spring-webmvc 依赖于：

*   spring-aop
*   spring-beans
*   spring-context
*   spring-core
*   spring-expression
*   spring-web

```xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>5.2.8.RELEASE</version>
</dependency>
```

### web.[xml](https://so.csdn.net/so/search?q=xml&spm=1001.2101.3001.7020) 配置 DispatcherServlet

> - **`XML 被设计用来传输和存储数据。 `**
> - **` HTML 被设计用来显示数据`**

```xml
<!-- 配置SpringMVC自带的DispatcherServlet -->
<servlet>
    <servlet-name>springmvc</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <!-- 默认会去加载/WEB-INF/${servlet-name}-servlet.xml -->
    <!-- Spring的配置文件位置 -->
    <init-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:applicationContext.xml</param-value>
    </init-param>
    <!-- 项目一旦部署到服务器，就会创建Servlet -->
    <load-on-startup>0</load-on-startup>
</servlet>

<servlet-mapping>
    <servlet-name>springmvc</servlet-name>
    <!-- 拦截JSP以外的所有请求 -->
    <url-pattern>/</url-pattern>
</servlet-mapping>
```

### appliactionContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd>

    <context:component-scan base-package="com.mj"/>

</beans>
```

### 新建 Controller

```java
@Controller
public class TestController() {
	@GetMapping("/test")
	@ResponseBody
	public String test() {
		return "Hello World";
	}
}
```

当 GET 请求 context_path/test 的时候，就会调用 TestController 的 test 方法

*   并且将 “Hello World” 输出给客户端  
    ![](http://121.41.87.138:9001/picgo/img/a10b2d0c273144429fa0bbc6b4e24b42.png)

> **@RequestMapping**
>
> @GetMapping、@PostMapping可以用在方法上，底层是基于@RequestMapping

SpringMVC - 基础
--------------

### 请求参数 - @RequestParam、@RequestBody

默认情况下，SpringMVC 会主动传递一些参数给请求方法：

*   WebRequest、HttpServletRequest、HttpServletResponse、HttpSession 等
*   参考：[参考文档](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc-ann-arguments)

默认情况下，请求参数会传递给同名的方法参数

*   可以通过 `@RequestParam` 指定方法参数对应的请求参数名  
    require 属性为 false：请求参数是可选的，客户端可以不传  
    require 属性为 true：请求参数是必传的，客户端必须传，否则出现 404 错误
*   可以通过 Model 对象去接收请求参数  
    Model 对象的属性名需要和请求参数保持一致

请求参数注解补充：

*   `@RequestParam`：普通的请求参数
*   `@RequestBody`：请求体，接收 Content-Type 为 application/json 数据

> 前端发送了 json 格式的数据，后端接收时必须用 `@RequestBody` 接收

*   `@RequestHeader("header-name")`：请求头

### 请求路径变量 - @PathVariable

如果请求路径中的内容是变化的，可以定义成 `{变量名}`，然后通过 `@PathVariable` 去获取值

```
@GetMapping("/user/{id}")
@ResponseBody
public String testId(@PathVariable("id") String id) {
	return "your id is " + id;
}
```

### 利用[反射](https://so.csdn.net/so/search?q=%E5%8F%8D%E5%B0%84&spm=1001.2101.3001.7020)获取参数名

从 JDK8 开始，可以通过 java.lang.reflect.Parameter 类获取参数名

*   前提：在编译 *.java 的时候保留参数名信息到 *.class
    
    ```shell
    javac -parameters hello.java
    ```
    
*   可以通过以下指令查看 class 文件的参数名信息(字节码文件)
    
    ```shell
    javap -v hello.class
    ```
    

Java 代码获取方法的参数名：

```java
public class TestParam {
	public void test(String name, int age) {}
	public static void main(String[] args) throws Exception {
		Method method = TestParam.class.getMethod("test", String.class, int.class);
		for(Parameter paramter : method.getParameters()) {
			System.out.println(parameter.getName());
		}
	}
}
```


![image-20240118092939542](http://121.41.87.138:9001/picgo/img/image-20240118092939542.png)

> this相当于第一个参数
> 

------



==而SpringMVC内部通过 `ParameterNameDiscoverer` 类获取参数名==

`PrioritizedParameterNameDiscoverer.java`

```java
@Override
@Nullable
public String[] getParameterNames(Method method) {
    for (ParameterNameDiscoverer pnd : this.parameterNameDiscoverers) {
        String[] result = pnd.getParameterNames(method);
        if (result != null) {
            return result;
        }
    }
    return null;
}
```

------

获取变量名：优先使用JDK反射方案，没有则通过局部变量表

### 乱码处理 - GET、POST 请求参数乱码

**从 Tomcat8 开始，GET 请求已经没有乱码问题了**

Tomcat 8 以前处理乱码的解决方案：

*   在 TOMCAT_HOME/conf/server.xml 中给 Connector 标签增加属性
    
    ```xml
    URIEncoding="UTF-8"
    ```
    

```xml
<Connector port="8080" protocol="HTTP/1.1"
		   connectionTimeout="20000"
		   redirectPort="8443"
		   URIEncoding="UTF-8"/>
```

POST 请求乱码问题仍然存在，解决方案：

通过 Filter 拦截请求，调用 `request.setCharacterEncoding("UTF-8")`

*   SpringMVC 已经内置了这样的 Filter，直接在 web.xml 中配置即可使用

```xml
<!-- 解决POST请求参数乱码问题 -->
<filter>
    <filter-name>CharacterEncodingFilter</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
        <param-name>encoding</param-name>
        <param-value>UTF-8</param-value>
    </init-param>
</filter>

<filter-mapping>
    <filter-name>CharacterEncodingFilter</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>
```

### 乱码处理 - 响应数据乱码

方法 1：设置 `@RequestMapping` 的 [produces](https://gitee.com/szluyu99/mj_java_frame/blob/master/03_SpringMVC/31_SpringMVC01_%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8/src/main/java/com/mj/controller/EncodingController.java) 属性

```java
@RequestMapping(
		value="/coding",
		produces="text/html; charset=UTF-8"
)
@ResponseBody
public String test() {
	return "<h1>哈哈哈</h1>";
}
```

方法 2：在 [<mvc:annotation-driven>](https://gitee.com/szluyu99/mj_java_frame/blob/master/03_SpringMVC/31_SpringMVC01_%E5%9F%BA%E6%9C%AC%E4%BD%BF%E7%94%A8/src/main/webapp/WEB-INF/web.xml) 中添加 <mvc:message-converters>

```xml
<mvc:annotation-driven>
    <mvc:message-converters>
        <bean class="org.springframework.http.converter.StringHttpMessageConverter">
            <property />
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```

> <mvc:annotation-driven> 还有别的作用，可以保证 controller 正常使用，后面有写到

### [Servlet](https://so.csdn.net/so/search?q=Servlet&spm=1001.2101.3001.7020) 的 url 匹配 - 3 种

`*.do`：不会拦截**动态资源**（比如 *.jsp）、**静态资源**（*.html、*.js）

`/`：会拦截**静态资源**（比如 *.html、*.js），不会拦截**动态资源**（比如 *.jsp）

`/*`：会拦截**动态资源**（比如 *.jsp）、**静态资源**（比如 *.html、*.js）

*   一般用于 Filter 中

#### Tomcat 中默认的 Servlet

Tomcat 中有两个默认的 Servlet，可以在 TOMCAT_HOME/conf/web.xml 中找到

1.  **org.apache.catalina.servlets.DefaultServlet**  
    url-pattern 是 `/`，即可以处理静态资源  
    ![](http://121.41.87.138:9001/picgo/img/d915e2d3c27b402ea9f32c1c314441e9.png)2. **org.apache.jasper.servlet.JspServlet**  
    url-pattern 是 `*.jsp`，作用是拦截 .jsp 文件并将其转成 Servlet（jsp 的本质是 Servlet）  
    ![](http://121.41.87.138:9001/picgo/img/b85b1434bc394c3aaa66b0bdbdbac103.png)

#### 静态资源被拦截的解决方案 - 2 种

如果 SpringMVC 的 DispatcherServlet 的 url-pattern 设置为 `/`，会导致静态资源被拦截

解决方案 1：**将静态资源交回给 Tomcat 的 DefaultServlet 去处理**

```
<!-- 在applicationContext.xml中配置 -->
<mvc:default-servlet-handler/>
```

<mvc:default-servlet-handler/> 的原理：

*   通过 DefaultServletHttpRequestHandler 对象将静态资源转发给 Tomcat 的 DefaultServlet

注：使用了 <mvc:default-servlet-handler/> 后，会导致 controller 无法处理请求

*   加上 <mvc:annotation-driven/> 即可保证 controller 正常使用

```
<!-- 在applicationContext.xml中配置 -->
<!-- 保证@Controller能够正常使用 -->
<mvc:annotation-driven/>
```

解决方案 2：由 SpringMVC 框架内部来处理静态资源

*   框架内部是通过 ResourceHttpRequestHandler 对象来处理

```
<!-- **代表所有子路径 -->
<!-- mapping是请求路径 -->
<!-- location是静态资源的位置 -->
<mvc:resources mapping="/asset/**" location="/asset/"/>
```

注：同样需要加上<<mvc:annotation-driven/>>来保证 controller 正常使用

```
<!-- 在applicationContext.xml中配置 -->
<!-- 保证@Controller能够正常使用 -->
<mvc:annotation-driven/>
```