# SpringMVC - 特殊的请求参数
Gitee 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/03_SpringMVC](https://gitee.com/szluyu99/mj_java_frame/tree/master/03_SpringMVC)

[集合](https://so.csdn.net/so/search?q=%E9%9B%86%E5%90%88&spm=1001.2101.3001.7020)类型 - Map、List、Set、String[]
----------------------------------------------------------------------------------------------------------

请求：/test?names1=mj&names1=aa&&names2=mj&names2=bb&&names3=mj&names3=cc

```java
@RequestMapping("/test")
@ResponseBody
public String test(@RequestParam Map<String, Object> map,
				   @RequestParam List<String> names1,
				   @RequestParam(required = false) Set<String> names2,
				   String[] names3) {
	System.out.println(map);
	System.out.println(names1);
	System.out.println(names2);
	for (String name : names3) {
		System.out.println(name);
	}
	return "666";
}
```

Multipart 参数 - multipartResolver
--------------------------------

添加依赖：

```xml
<!-- commons-fileupload: 解析Multipart参数 -->
<dependency>
    <groupId>commons-fileupload</groupId>
    <artifactId>commons-fileupload</artifactId>
    <version>1.4</version>
</dependency>
```

添加 CommonsMultipartResolver 到 IoC 容器，id 值固定为 **multipartResolver**

```java
<bean id="multipartResolver"
	  class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
	<!-- 保证请求参数、文件名不乱码 -->
	<property />
</bean>
```

> 一般请求的乱码问题可以统一通过 Filter 解决

### 文件参数 - [MultipartFile](https://so.csdn.net/so/search?q=MultipartFile&spm=1001.2101.3001.7020)

文件参数基于【Multipart 参数】配置，即需要配置 **multipartResolver**

前端 form 表单指定 enctype 为 multipart/form-data：

```javascript
<form method="post" action="upload"
	  enctype="multipart/form-data">
	<input type="file" >
	<button type="submit">提交</button>
</form>
```

```java
@RequestMapping("/upload")
@ResponseBody
public String upload(MultipartFile photo) throws Exception {
	// 文件名
	String filename = photo.getOriginalFilename();
	// 将文件内容写出去(会自动生成本不存在的文件夹)
	photo.transferTo(new File("C:/Users/yusael/Desktop/img/" + filename));
}
```

### 多文件参数 - MultipartFile[]、List<>

文件参数基于【Multipart 参数】配置，即需要配置 **multipartResolver**

前端上传多个文件：

```js
<form method="post" action="uploads" enctype="multipart/form-data">
	<input type="file" >
	<input type="file" >
	<input type="file" >
	<button type="submit">提交</button>
</form>
```

后端可以用 `MultipartFile[]` 接收：

```java
@RequestMapping("/uploads")
@ResponseBody
public String uploads(MultipartFile[] photos) throws Exception {
	System.out.println(phots.size());
	return "Uploads success!";
}
```

也可以用 `List<MultipartFile>` 进行接收：

```java
@RequestMapping("/uploads")
@ResponseBody
public String uploads(List<MultipartFile> photos) throws Exception {
	System.out.println(phots.size());
	return "Uploads success!";
}
```

### CommonsMultipartResolver 的常用属性

以下属性均是在 [multipartResolver](https://gitee.com/szluyu99/mj_java_frame/blob/master/03_SpringMVC/33_SpringMVC03_%E7%89%B9%E6%AE%8A%E7%9A%84%E8%AF%B7%E6%B1%82%E5%8F%82%E6%95%B0/src/main/resources/applicationContext.xml) 中进行配置：

defaultEncoding：设置 request 的请求编码

uploadTempDir：设置上传文件时的临时目录，默认是 Servlet 容器的临时目录

maxUploadSize：限制总的上传文件大小，以字节为单位。当设为 -1 时表示无限制，默认为 -1

maxUploadSizePerFile：限制每个上传文件的大小

maxInMemorySize：设置每个文件上传时允许写到内存中的最大值，以字节为单位，默认 10240

*   若一个文件的大小超过这个数值，就会生成临时文件；否则不会产生临时文件

```
<!-- 解析Multipart参数 -->
<bean id="multipartResolver"
      class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
	<!-- 设置request的请求编码为UTF-8 -->
    <property />
	<!-- 设置每个文件上传时允许写到内存最大值为 20480kb -->
    <property />
</bean>
```

日期类型
----

示例代码：[SpringMVC - 特殊的请求参数](https://gitee.com/szluyu99/mj_java_frame/tree/master/03_SpringMVC/33_SpringMVC03_%E7%89%B9%E6%AE%8A%E7%9A%84%E8%AF%B7%E6%B1%82%E5%8F%82%E6%95%B0)

Spring (MVC) 默认支持 `yyyy/MM/dd` 的日期转换格式，其他日期格式需要特殊处理

### @DateTimeFormat

方法 1：使用 `@DateTimeFormat`

```java
@RequestMapping("/testDate")
@ResponseBody
public String testDate(@DateTimeFormat(pattern = "yyyy-MM-dd") Date bir) {
	System.out.prinln(bri);
	return "success!";
}
```

### Converter

方法 2：使用 Converter

> [conversionService](https://blog.csdn.net/weixin_43734095/article/details/119359462#_Converter_133) 参考之前 Spring 中讲解的 ConversionServiceFactoryBean

```java
public class DateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(source);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
```

```xml
<mvc:annotation-driven conversion-service="conversionService"/>

<!-- 类型转换器 -->
<bean id="conversionService"
      class="org.springframework.context.support.ConversionServiceFactoryBean">
    <property >
        <set>
            <bean class="com.mj.converter.DateConverter"/>
        </set>
    </property>
</bean>
```

