# SpringBoot - 入门_配置文件_YAML

#### SpringBoot - 入门_配置文件_YAML

[Gitee](https://so.csdn.net/so/search?q=Gitee&spm=1001.2101.3001.7020) 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot)

JavaEE 开发存在的问题（即使使用了 SSM）

*   配置繁多、部署复杂、集成第三方库并不简单
*   SpringBoot 可以很好地解决上述问题

官网：[https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)

*   本课程使用的版本：2.3.4.RELEASE

SpringBoot 的特点

*   内置了很多常用配置，让 JavaEE 开发更加简单
*   项目可以独立运行，无须额外依赖 web 容器（内置了 web 容器）
*   …

SpringBoot - 入门
---------------

入门项目：[Gitee 代码](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot/41_SpringBoot01_HW)

pom.xml：

```xml
<!-- 继承父项目,里面有各种库的版本号 -->
<parent>
    <artifactId>spring-boot-starter-parent</artifactId>
    <groupId>org.springframework.boot</groupId>
    <version>2.3.4.RELEASE</version>
</parent>

<!-- 依赖 -->
<dependencies>
    <!-- web项目(已经集成了SpringMVC中很多的常用库) -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
</dependencies>
```

controller：

```java
@RestController
public class TestController {
    @GetMapping("/test")
    public String test() {
        return "Hello SpringBoot";
    }
}
```

程序入口：

```java
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

执行 main 方法后，在浏览器输入：http://localhost:8080/test  
![](https://img-blog.csdnimg.cn/a814e9281a674ce8a90e29dc73f74424.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

### @SpringBootApplication

`@SpringBootApplication` 中包含了 3 个注解：

*   `@EnableAutoConfiguration`：可以根据 maven 依赖自动构建相关环境  
    比如为 spring-boot-starter-web 构建 web 容器环境等
*   `@componentScan`：默认会`扫描当前包以及子包中的所有类`
*   `@SpringBootConfiguration`  
    从 SpringBoot 2.2.1 开始，被 SpringBoot 扫描到的 `@Component` 不用添加 `@Configuration`

### 可运行 jar - spring-boot-[maven](https://so.csdn.net/so/search?q=maven&spm=1001.2101.3001.7020)-plugin

如果希望通过 `mvn package` 打包出一个可运行的 jar，需要增加一个插件：

```xml
<build>
    <finalName>sb_hw</finalName>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

也可以使用插件的 run 功能，直接运行 SpringBoot 应用：  
![](https://img-blog.csdnimg.cn/522b0afe12da468496c27d9ab605edeb.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

### 热部署 - devtools

增加依赖（ Debug 模式下，它可以监控 classpath 的变化）

```xml
<!-- 热部署 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
</dependency>
```

重新编译项目（2 种方式）

*   手动编译（Build Project、Build Module）
*   自动编译

> 需要注意的是：使用了 spring-boot-devtools 后可能会执行 2 次 main 方法
> 
> *   不用在意，因为 devtools 仅限于开发调试阶段使用

需要使用热部署还需要在 Idea 开启以下设置：

*   Build project automatically：  
    ![](https://img-blog.csdnimg.cn/13b41acc09ca485c96c6349242b881d8.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)
*   Ctrl + Shift + Alt + /  
    ![](https://img-blog.csdnimg.cn/1c916c5d4ffc4ca7b90caaa940e6410c.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
    ![](https://img-blog.csdnimg.cn/d3c40fa4ea3b4f799cd165f64029684e.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

以上配置的效果为： 当 Idea 失去焦点时，会进行自动编译（会有点延迟）

配置文件 - application
------------------

### 配置文件默认加载路径

SpringBoot 默认会去加载一个**应用程序配置文件**，文件名为 application

> 应用程序配置文件（Application Property Files）

application 配置文件默认可以存放的位置如下所示（按照优先级从高到低排序）

*   **file:./config/**
*   **file:./config/**
*   **file:./**
*   **classpath:/config/**
*   **classpath:/**

> 注： **file:./** 代表项目根路径（jar 包）， **classpath:/** 代表 resources 目录

最常见的做法就是：**classpath:/**，即直接在 resources 下放配置文件

注：application 配置文件的内容可以直接通过 `@Value` 进行注入  
![](https://img-blog.csdnimg.cn/cabdc3949a0c494fac51c08b8832ee25.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

> 注意：Map 类型的注入比较特殊，后面有介绍

> 如果是其他配置文件需要 `@PropertySource` 引入配置文件
> 
> *   由于 application 在 SpringBoot 项目中必然会被加载，因此无需引入，可以直接注入

### 运行参数、VM 选项

可以通过运行参数或 VM 选项，指定配置文件的文件名、位置：

```shell
java -jar myproject.jar --spring.config.name=mj
```

```shell
java -jar myproject.jar --spring.config.location=F:/mj.properties
```

在 Idea 中启动配置中可以配置**运行参数**、**VM 选项**：

*   通过运行参数或 VM 选项，可以覆盖配置文件的内容  
    ![](https://img-blog.csdnimg.cn/570fdd8f0ed3455fb480048c22fab466.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

运行参数会传递给 main 方法的 args 参数：

> `--spring.config.name=mj` 是运行参数

System.getProperty、Ststem.getProperties 可以获取 VM 选项：

> `-Dspring.config.name=mj` 是 VM 选项

### 配置文件的内容

在 application 配置文件中，关于服务器的配置内容都可以从下面找到：

*   spring-boot-autoconfigure.jar / META-INF / spring.factories
*   ServletWebServerFactoryAutoConfiguration
*   ServerProperties  
    ![](https://img-blog.csdnimg.cn/b00b3f51b7784bf9a6d9d48baadc18c7.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
    ![](https://img-blog.csdnimg.cn/9eb05f123a984dc8af0c57984cca178a.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
    ![](https://img-blog.csdnimg.cn/9ccfa28048284babbea06c454f1eb9fa.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
    ![](https://img-blog.csdnimg.cn/e3794a9e0a064e948f41c13d5ea045de.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

### [YAML](https://so.csdn.net/so/search?q=YAML&spm=1001.2101.3001.7020)

YAML（YAML Ain’t a Markup Language，YAML 不是一种标记语言）

*   **SpringBoot 更推荐使用 YAML 作为配置文件**
*   文件拓展名是 .yml  
    ![](https://img-blog.csdnimg.cn/2e4c451e2abf4eb8ae81869c2d2ca613.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)
*   YAML 使用空格或 TAB 作为层级缩进
*   冒号 `:` 与后面紧跟的内容之间必须要有空格或 TAB
*   字符串可以不用加引号。如果有转义字符，可以使用双引号括住字符串

```yaml
# \n会作为字符串输出
name1: m\nj
# \n会被转移为换行输出
name2: "m\nj"
```

### 提高开发效率的依赖

为了更快捷的完成绑定工作，可以添加 2 个依赖：

```xml
<!-- 配置文件属性名提示 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
</dependency>

<!-- 在编译期间帮助生成Getter、Setter等代码 -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <scope>provided</scope>
</dependency>
```

注意：

*   spring-boot-configuration-processor 需要代码重新编译后，才会生成提示
*   Lombok 仅仅在编译期间使用即可，所以 scope 设置为 provided 即可

仅仅引入 Lombok 依赖，虽然使用了 `@Data` 会自动生成 Getter、Setter 方法，但是在开发期间不会有代码提示，需要代码提示还需要安装 Lombok 插件：[Lombok 官网](https://projectlombok.org/)  
![](https://img-blog.csdnimg.cn/a4e467b91d3d47d8a868fa5d8ab38db1.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
![](https://img-blog.csdnimg.cn/c604b9c0bf4c43b0914ecc35587fec56.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

### 属性绑定 - @ConfigurationProperties

> 属性绑定（JavaBean properties binding）

使用了 `@ConfigurationProperties("student")` 指定了配置文件中的前缀，通过这个注解可以**将配置文件中的值直接读取到指定类中**（类名与前缀对应）：  
![](https://img-blog.csdnimg.cn/d3fd89ba472047b18bd0e269616cbd20.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
![](https://img-blog.csdnimg.cn/f2a26f0329b24cba847b4a6b5a23a533.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
使用了 `@EnableConfigurationProperties` 后，Student 就不用就加 `@Component` 了

> 再倒回去看 ServletWebServerFactoryAutoConfiguration，可以理解的更加深刻  
> ![](https://img-blog.csdnimg.cn/e00c07f8df804279a45c36f3076a2fe3.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)  
> ![](https://img-blog.csdnimg.cn/f112578f2e674cdc89dc075c34229f3c.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

`@ConfigurationProperties` 也可以用在 `@Bean` 上：  
![](https://img-blog.csdnimg.cn/2f8c6a8ca2944632b0c74dd8cf0aee58.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

### 构造方法绑定 - @ConstructorBinding

> 构造方法绑定（Constructor binding）

构造方法绑定只能用 `@EnableConfigurationProperties`，不能使用 `@Component` 的方式  
![](https://img-blog.csdnimg.cn/0fe08081e136452486b8d5e556d43eb6.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

### 宽松绑定 (Relaxed Binding) - 概念

配置文件中的属性名格式比较宽松，有多种风格：

*   **Kebab 格式（烤肉串格式），推荐这种写法**：

```
acme.my-project.person.first-name
```

*   驼峰写法：

```
acme.myProject.person.firstName
```

*   下划线写法：

```
acme.my_project.person.first_name
```

*   变量名大写：使用系统环境变量时建议使用

```
ACME_MYPROJECT_PERSON_FIRSTNAME
```

### 拆分配置文件

文件名 `application-*` 的后半部分默认就是文档名

```yaml
# application.yml
spring:
  profiles:
  	active: [cat, student]
  	
person:
  age: 11
  name: Person-{person.age}
```

```yaml
# application-cat.yml
cat:
  age: 22
  name: Cat-${cat.age}
```

```yaml
# application-student.yml
student:
  age: 33
  name: Student-${student.age}
```

在 application.yml 中引入了 application-cat.yml 和 application-student.yml，相当于如下效果：

```yaml
# application.yml
person:
  age: 11
  name: Person-{person.age}

cat:
  age: 22
  name: Cat-${cat.age}

student:
  age: 33
  name: Student-${student.age}
```

### banner

参考：[官网文档](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#boot-features-banner)

Banner 生成：[http://patorjk.com/software/taag](http://patorjk.com/software/taag)
