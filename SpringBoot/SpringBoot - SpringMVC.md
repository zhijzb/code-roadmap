# SpringBoot - SpringMVC

Gitee 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot)

SpringMVC 的配置
-------------

示例代码：[SpringBoot - SpringMVC](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot/00_parent/49_mvc)

默认情况下，SpringBoot 已经**内置**了很多 SpringMVC 的常用配置口

*   如果希望在此基础上，继续增加一些配置，可以使用 WebMvcConfigurer

```java
@Configuration
public class SpringMVCConfig implements WebMvcConfigurer {
}
```

*   如果希望`完全自定义 SpringMVC `的配置，那就是用 `@EnableWebMvc`

```java
@Configuration
@EnableWebMvc
public class SpringMVCConfig implements WebMvcConfigurer {
}
```

在 [application](https://gitee.com/szluyu99/mj_java_frame/blob/master/04_SpringBoot/00_parent/49_mvc/src/main/resources/application.yml) 配置文件中的配置

> 参考spring-boot-autoconfigure.jar 中的 WebMvcProperties 类

```yaml
spring:
  mvc:
    servlet:
      load-on-startup: 0 # 第一次访问servlet时创建，默认值为-1
```

```yaml
# 日期格式转换
spring:
  mvc:
    format:
      date: yyyy-MM-dd
```

文件上传功能
------

文件大小限制的配置：默认最大1MB

```yaml
spring:
  servlet:
    multipart:
      max-request-size: 100MB
      max-file-size: 100MB
```

在 SpringBoot 中使用 `servletContext.getRealPath("/")` 获取的路径可能如下：

*   `C:\Users\MJ\AppData\Local\Temp\tomcat-docbase.14216908544470700598.8080`

```java
@PostMapping("/upload")
public String upload(String username, MultipartFile photo, HttpServletRequest request) throws Exception {
    // 获取文件扩展名
    String extension = FilenameUtils.getExtension(photo.getOriginalFilename());

    // 目标文件
    String dir = properties.getUpload().getImageFullpath();
    String filename = UUID.randomUUID() + "." + extension;
    File file = new File(dir + filename);

    // 创建好目标文件所在的父目录
    FileUtils.forceMkdirParent(file);

    // 将文件数据写到目标文件，小文件可能存放在内存中，大文件一般写到临时文件中
    photo.transferTo(file);

    return "上传成功!!!";
}
```

> MultipartFile.transferTo

> 使用工具类，提高开发效率
>
> commons-io

文件下载 - 以附件形式下载
--------------

> 如果直接通过一个链接去访问文件，则会直接在浏览器上显示该文件内容，而不是进行下载

```java
@GetMapping("/download")
public void download(HttpServletResponse response) throws Exception {
    
    // 设置响应头，把文件以附件（attachment）的形式返回
    response.setHeader("Content-Disposition", "attachment; file);
    // 读取文件
    try (InputStream is = new ClassPathResource("static/txt/bg.png").getInputStream()) {
        // 将文件数据利用response写回到客户端
        IOUtils.copy(is, response.getOutputStream());
    }
}
```

单独在 SpringMVC 中也是这样使用

> ```java
> IOUtils.copy(is, response.getOutputStream());
> ```

静态资源访问
------

在 SpringBoot 中，静态资源需要进行`映射`才能直接访问

SpringBoot 默认的静态资源映射处理

*   spring-boot-autoconfigure.jar
*   WebMvcAutoConfiguration
*   WebMvcAutoConfigurationAdapter
*   addResourceHandlers

### 静态资源访问 - 映射

方法 1：可以通过 [application](https://gitee.com/szluyu99/mj_java_frame/blob/master/04_SpringBoot/00_parent/49_mvc/src/main/resources/application.yml) 配置文件进行静态资源的映射

```yaml
spring:
  mvc:
    static-path-pattern: /**
   
  resources:
    static-locations: classpath:/myStatic/, file:///F:/myStatic/  # 静态资源目录
    #static-locations: classpath:/myStatic/, file:///${project.upload.base-path}  # ${}取值
```

> file:///F:/myStatic/
>
> `file://`：固定写法
>
> `/F:/myStatic/`：根路径下的F盘

方法 2：Java 代码中，可以通过 WebMvcConfigurer 的方法进行静态资源的映射

```java
@Configuration
public class SpringMvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	// 映射classpath下的资源
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/");
                
		// 映射本地资源
        registry.addResourceHandler("/**")
                .addResourceLocations("file:///F:/static/");
        registry.addResourceHandler("/**")
                .addResourceLocations("file:///F:/upload/");
    }
}
```

### 静态资源访问 - webjars

SpringBoot 支持使用 [webjars](https://www.webjars.org/) 的方式访问静态资源

> WebJars是**将这些通用的Web前端资源打包成Java的Jar包**，然后借助Maven工具对其管理，保证这些Web资源版本唯一性，升级也比较容易。

示例：使用 webjars 访问 jquery

```xml
<dependency>
	<groupId>org.webjars</groupId>
	<artifactId>jquery</artifactId>
	<verison>3.5.1</version>
</dependency>
```

![](https://img-blog.csdnimg.cn/61f7839c26ee495f9524033dd7362a7e.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

```xml
<!-- path: /webjars/** -->
<!-- location: classpath:/META-INF/resources/webjars/ -->
<script th:src="@{/webjars/jquery/3.5.1/jquery.js}"></script>
```

> 应用：swagger-ui
