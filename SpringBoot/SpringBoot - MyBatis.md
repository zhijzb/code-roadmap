# SpringBoot - MyBatis

Gitee 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot)

集成 MyBatis
----------

示例代码：[SpringBoot 集成 MyBatis](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot/00_parent/47_MyBatis)

### 引入依赖

> 参考：[http://mybatis.org/spring-boot-starter/mybatis-spring-boot-autoconfigure/](http://mybatis.org/spring-boot-starter/mybatis-spring-boot-autoconfigure/)

一般来说，SpringBoot 认为常用的库都会在 parent 项目中锁定了版本，例如 mysql

```xml
<!-- 需要写版本号 -->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.3</version>
</dependency>

<!-- 不需要填写版本号-->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>

<!-- 需要填写版本号 -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid-spring-boot-starter</artifactId>
    <version>1.2.1</version>
</dependency>
```

### 数据源配置 - 源码

```yaml
spring:
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    username: root
    password: root
    url: jdbc:mysql://localhost:3306/test_mybatis?serverTimezone=UTC
    druid:
      initial-size: 5
      max-active: 10
```

数据源的相关配置，可以参考：

* spring-boot-autoconfigure.jar 中的 DataSourceProperties 类  

    ![image-20240131102653180](http://121.41.87.138:9001/picgo/img/image-20240131102653180.png)
    ![image-20240131102745523](http://121.41.87.138:9001/picgo/img/image-20240131102745523.png)![image-20240131103101501](http://121.41.87.138:9001/picgo/img/image-20240131103101501.png)

*   druid-spring-boot-autoconfigure.jar 中的 DruidDataSourceWrapper 类  
    ![image-20240131103334600](http://121.41.87.138:9001/picgo/img/image-20240131103334600.png)



### MyBatis 配置 - 源码

```yaml
mybatis:
#  mapper-locations: classpath:/mappers/*.xml
#  config-location: classpath:mybatis-config.xml # 引入本地配置文件
  type-aliases-package: com.mj.domain
  configuration:
    map-underscore-to-camel-case: true # 数据库：my_name -> Java: myName
    use-generated-keys: true # 
```

> 保持同名且同一目录下，可省略：
>
> mapper-locations: classpath:/mappers/*.xml

MyBatis 相关的配置，可以参考：

*   mybatis-spring-boot-autoconfigure.jar 中的 MybatisProperties 类  
    ![image-20240131103818507](http://121.41.87.138:9001/picgo/img/image-20240131103818507.png)

### 扫描 Dao

```java
@SpringBootApplication
@MapperScan("com.mj.dao")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

### MyBatis 主配置 - XML、注解、application

> useGeneratedkeys 除了可以在 mapper 中单独配置，也可以进行全局统一配置（3 种方式）  
> 下面的配置中包含了 mapUnderscoreToCamelCase 和 useGeneratedkeys

MyBatis 主配置文件主要有以下三种形式：

*   使用额外的 **XML** 文件，然后在 SpringBoot 中引入该 XML（通用）
*   利用**注解**实现纯代码进行配置（SpringBoot 专用）
*   在 **application** 中进行配置（SpringBoot 专用）

#### XML 配置

在 application 中指定 mybatis 配置文件的位置：

```yaml
mybatis:
  config-location: classpath:mybatis-config.xml
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <settings>
        <!-- 使用新生成记录的主键 -->
        <setting />
        <!-- 数据库: my_first_name -> Java: myFirstName -->
        <setting />
    </settings>
</configuration>
```

#### 注解

```java
@Configuration
public class MyBatisConfig {
    @Bean
    public ConfigurationCustomizer customizer() {
        return (configuration) -> {
            configuration.setMapUnderscoreToCamelCase(true);
            configuration.setUseGeneratedKeys(true);
        };
    }
}
```

#### application

```yaml
mybatis:
  configuration:
    map-underscore-to-camel-case: true
    use-generated-keys: true
```

starter 的命名规范
-------------

SpringBoot 官方提供的 starter：**spring-boot-starter-***

自定义的 starter（非 SpringBoot 官方）：***-spring-boot-starter**

参考：[官方文档](https://docs.spring.io/spring-boot/docs/current/reference/html/using.html#using-boot-starter)

![image-20240131102135817](http://121.41.87.138:9001/picgo/img/image-20240131102135817.png)