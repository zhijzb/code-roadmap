> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [blog.csdn.net](https://blog.csdn.net/weixin_43734095/article/details/119705786)

#### 交错的日志系统、SpringBoot 集成日志框架


Gitee 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot)

在实际开发过程中，不同的库（项目）内部使用的日志系统不一定相同

*   当多个带有日志系统的项目混合在一起时，日志系统可能就会变得有点复杂，甚至产生冲突

![](https://img-blog.csdnimg.cn/84c1424cd0d04da98400c73a6a909810.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)  
在上图中，3 个项目的日志系统可以互不影响、独立运行

交错复杂的日志系统① - 多个项目实现 SLF4J 门面
----------------------------

![](https://img-blog.csdnimg.cn/c4bd2025a57845cbb8f0a3020e4afd46.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)  
上图中，SLF4J 发现有 3 种实现，会发生冲突，最终会选择其中一 种覆盖其他实现

可以根据自己的需要排除掉 2 种实现，剩下 1 种想要的实现：

*   剩下 Logback 实现：  
    ![](https://img-blog.csdnimg.cn/7c1bcb853b45448493b04ea71b3fa552.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)  
    * 剩下 Log4j 2.x 实现  
    ![](https://img-blog.csdnimg.cn/66154e71821c44bf9d711d469b1c5c9c.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

交错复杂的日志系统② - 统一底层实现为 Logback
----------------------------

![](https://img-blog.csdnimg.cn/9ca1910926dc43d49273ce9f4fc79e20.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)  
如果希望将所有的日志系统进行统一，比如底层都使用 Logback，解决方案：

*   排除 Log4j 1.x、Log4j 2.x
*   增加 Log4j 1.x 转为调用 SLF4J 的包：**log4j-over-slf4j**
*   增加 Log4j 2.x 转为调用 SLF4J 的包：**log4j-to-slf4j**

```
<!-- log4j 1.x的盗版实现，内部会调用SLF4J -->
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>log4j-over-slf4j</artifactId>
    <version>1.7.30</version>
</dependency>

<!-- log4j 2.x的盗版实现，内部会调用SLF4J -->
<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-to-slf4j</artifactId>
    <version>2.13.3</version>
</dependency>
```

最终实现的结构：  
![](https://img-blog.csdnimg.cn/c7d7e398859c4bb4b79736479ea90276.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

交错复杂的日志系统③ - 统一底层实现为 [Log4j](https://so.csdn.net/so/search?q=Log4j&spm=1001.2101.3001.7020) 2.x
-----------------------------------------------------------------------------------------------

![](https://img-blog.csdnimg.cn/ce53fc52c5414ce784d608ae5657fbbe.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)  
如果希望将所有的日志系统进行统一，比如底层都使用 Log4j 2.x

### 解决方案 1

*   排除 Log4j 1.x、Logback
*   增加 Log4j 1.x 转为调用 SLF4J 的包：**log4j-over-slf4j**
*   增加 Log4j 2.x 与 SLF4J 的适配包：**log4j-slf4j-impl**

```
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>log4j-over-slf4j</artifactId>
    <version>1.7.30</version>
</dependency>

<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-slf4j-impl</artifactId>
    <version>2.13.3</version>
</dependency>
```

最终实现的结构：  
![](https://img-blog.csdnimg.cn/2395637572b642f5aa801ca21b8cae45.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

### 解决方案 2

*   排除 Log4j 1.x、Logback
*   增加 Log4j 1.x 转为调用 Log4j 2.x 的包：**log4j-1.2-api**
*   增加 Log4j 2.x 与 SLF4J 的适配包：**log4j-slf4j-impl**

```
<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-1.2-api</artifactId>
    <version>2.14.0</version>
</dependency>

<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-slf4j-impl</artifactId>
    <version>2.13.3</version>
</dependency>
```

最终实现结构：  
![](https://img-blog.csdnimg.cn/c16e72996af748bd8ca5f2a6b2be75e5.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

日志依赖总结
------

门面接口：

*   log4j-api：门面接口（log4j 2.x）
*   slf4j-api：门面接口

日志实现：

*   logback-classic：实现了 slf4j-api 门面接口的日志实现框架
*   log4j-core：实现了 log4j-api 门面接口的日志实现框架（**log4j 2.x**）
*   log4j：**log4j 1.x** 的日志实现框架

与 SLF4J 的适配包：

*   slf4j-log4j12：实现了 slf4j-api 门面接口，它的内部会调用 log4j
*   log4j-slf4j-impl：实现了 slf4j-api 门面接口，它的内部会调用 log4j-core

> logback 默认就是实现了 slf4j-api 门面接口

*   log4j-over-slf4j：log4j 的盗版实现，它的内部会调用 slf4j-api
*   log4j-1.2-api：log4j 的盗版实现，它的内部会调用 log4j-core
*   log4j-to-slf4j：log4j-core 的盗版实现，它的内部会调用 slf4j-api

SpringBoot 集成日志框架
-----------------

在 SpringBoot 中，日志框架的建议：

*   SLF4J + Logback
*   SLF4J + Log4j 2.x

参考：[官方文档](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-logging)

### SpringBoot 集成 Logback

SpringBoot 默认已经继承了 Logback，不用再添加 Logback 的依赖，配置文件位置是：

*   classpath:logback.xml
*   classpath:logback-spring.xml（SpringBoot 推荐）

SpringBoot 内置的 [Logback 配置](https://github.com/spring-projects/spring-boot/blob/v2.3.5.RELEASE/spring-boot-project/spring-boot/src/main/resources/org/springframework/boot/logging/logback/defaults.xml)

*   spring-boot.jar
*   org/springframework/boot/logging/logback/defaults.xml

### SpringBoot 集成 Log4j 2.x

方法 1：通过 <exclusion> 标签排除包

```
<dependencies>
   <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <exclusions>
        	<!-- 去掉logging -->
            <exclusion>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-logging</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
	<!-- 添加Log4j2的starter -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-log4j2</artifactId>
    </dependency>
</dependencies>
```

方法 2：使用一个版本号不存在的依赖覆盖原来的依赖：

```
<dependencies>
	<!-- 使用不存在的版本号覆盖依赖 -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-logging</artifactId>
        <version>0</version>
    </dependency>
	<!-- 添加Log4j2的starter -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-log4j2</artifactId>
    </dependency>
</dependencies>
```

配置文件的位置应该是：

*   classpath:log4j2.xml
*   **classpath:log4j2-spring.xml**（SpringBoot 推荐）

SpringBoot 内置的 [Log4j 2.x 默认配置](https://github.com/spring-projects/spring-boot/blob/v2.3.5.RELEASE/spring-boot-project/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2.xml)

*   spring-boot.jar
*   org/springframework/boot/logging/log4j2/log4j2.xml

### SpringBoot 的 logging 配置 (application)

设置日志级别：

```
logging:
  level:
    root: info
    com.mj.dao: debug
    com.mj.controller: debug
```

自定义配置文件的路径：

```
logging:
  config: classpath:log4j2-spring.xml
```

定义[日志组](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-custom-log-groups)：

```
logging:
  level:
    root: info
    project: debug
  group:
  	project:
  	  - com.mj.dao
  	  - com.mj.controller
```