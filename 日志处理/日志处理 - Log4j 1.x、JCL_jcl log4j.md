> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [blog.csdn.net](https://blog.csdn.net/weixin_43734095/article/details/119635425)

[Gitee](https://so.csdn.net/so/search?q=Gitee&spm=1001.2101.3001.7020) 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot)



日志 (Log) 的作用：

*   **开发调试**：在开发过程中，进行逻辑跟踪、查看运行结果
*   **问题排查**：辅助排查和定位线上问题，优化程序运行性能
*   **状态监控**：监控系统的运行状态、检测非授权的操作
*   **数据分析**：  
    日志中蕴含了大量的用户数据，包括点击行为，兴趣偏好等  
    用户数据对于公司下一步的战略方向有一定的指导方向

Java 日志处理的发展史
-------------

**最原始**：

*   System.out、System.err

**JUL (Java Util Logging)**

*   JDK 自带的日志框架，在 java.util.logging 包下
*   API 不完善、对开发者不友好，很少人使用

**Log4j (Log for Java)**

*   由 Apache 组织推出
*   1.x 版本于 2015 年 8 月 5 日 宣布停止维护

**JCL (Jakarta Commons Logging)**

*   由 Apache 组织推出的**日志门面接口**
*   提供一套 API 来实现不同 Logger 之间的切换

[SLF4J](http://www.slf4j.org/) **(Simple Logging Facade For Java)**

*   由 Log4j 作者开发的**日志门面接口**，比 JCL 更优秀更好用

[Logback](https://logback.qos.ch/)

*   由 Log4j 作者开发，比 Log4j 1.x 性能高很多，实现了 SLF4J

[Log4j2](http://logging.apache.org/log4j/2.x/index.html)

*   由 Apache 组织推出的，Log4j 1.x 的重大升级改进版，改进了 Logback 的一些问题
*   **既是门面接口，又是日志实现**

日志框架总结：

*   **门面接口**：JCL、SLF4J、Log4j2
*   **日志实现**：JUL、Log4j 1.x、Logback、Log4j2

课程大纲：

*   Log4j 1.x（实现）
*   JCL（门面）  
    Log4j 1.x（实现）
*   SLF4J（门面）  
    Log4j 1.x（实现）  
    **Logback（实现）+ SpringBoot**（重点学习）  
    Log4j 2.x（实现）+ SpringBoot
*   Log4j 2.x（门面）  
    Log4j 2.x（实现）

Log4j 1.x
---------

示例代码：[log4j - properties](https://gitee.com/TAMADA/mj_java_frame/blob/master/04_SpringBoot/02_Log/01_Log4j1/src/main/resources/log4j_0.properties)

```xml
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
```

配置文件：

*   classpath:log4j.xml（优先级高）
*   classpath:log4j.properties

> log4j.properties 功能比 log4j.xml 弱一些
> 
> *   比如 log4j.properties 无法设置 Filter

### 日志级别 - 6 种

```java
public class TestLog4j {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(TestLog4j.class);
        logger.fatal("致命_FATAL");
        logger.error("错误_ERROR");
        logger.warn("警告_WARN");
        logger.info("信息_INFO");
        logger.debug("调试_DEBUG");
        logger.trace("痕迹_TRACE");
    }
}
```

日志的级别由小到大是：TRACE < DEBUG < INFO < WARN < ERROR < FATAL < OFF

*   设置了日志级别后，能输出 **大于等于** 它级别的日志信息，OFF 是关闭日志

### pattern - 输出格式

参考：[https://logging.apache.org/log4j/2.x/manual/layouts.html#PatternLayout](https://logging.apache.org/log4j/2.x/manual/layouts.html#PatternLayout)

*   %c：Logger 的全类名
*   %d：时间，`%d{yyyy-MM-dd HH:mm:ss.SSS}`
*   %t：线程的名称  
    
*   %F：文件名
*   %L：代码行号
*   %M：方法
*   %l：代码的具体位置（哪个类的哪个方法？哪一行？）  
    
*   %m：消息
*   %n：换行
*   %p：日志级别
*   %%：一个 %

Log4j 1.x - properties
----------------------

### 子 Logger

子 Logger 可以是**包级别**、**类级别**

子 Logger 默认会继承 父 Logger 的 Appender

所有 子 Logger 的 最终父 Logger 是 **rootLogger**

```
log4j.logger.com.mj=ERROR, console, file
# 不继承父级Logger的Appender
log4j.additivity.com.mj=false

log4j.logger.com.mj.TestLog4j=WARN, console
# 不继承父级Logger的Appender
log4j.additivity.com.mj.TestLog4j=false
```

### Appender - 控制日志的输出目标

示例代码：[log4j - properties](https://gitee.com/TAMADA/mj_java_frame/blob/master/04_SpringBoot/02_Log/01_Log4j1/src/main/resources/log4j.properties)

#### ConsoleAppender - 将日志输出到控制台

ConsoleAppender 表示输出目标是控制台

```
# 全局配置
CHARSET=UTF-8
PATTERN=%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5p] [%t]: %m%n

# 全局Logger配置(日志级别,输出目标)
log4j.rootLogger=TRACE, console

# 输出到控制台
log4j.appender.console=org.apache.log4j.ConsoleAppender
log4j.appender.console.encoding=${CHARSET}
# 输出格式
log4j.appender.console.layout=org.apache.log4j.PatternLayout
log4j.appender.console.layout.conversionPattern=${PATTERN}
```

#### FileAppender - 将日志输出到文件（单个）

FileAppender 表示输出目标是文件，缺点是只会将所有日志输出到一个文件中

```
# 全局Logger配置(日志级别,输出目标)
log4j.rootLogger=TRACE, console

# 输出目标(文件)
log4j.appender.file=org.apache.log4j.FileAppender
log4j.appender.file.encoding=${CHARSET}
log4j.appender.file.file=F:/logs/log4j.log
# 输出格式
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.conversionPattern=${PATTERN}
```

还有其他的 Layout，比如：HTMLLayout

#### RollingFileAppender - 将日志输出到文件（滚动）

滚动：当日志文件大到一定程度的时候，会生成新的日志文件，将内容输出到新的文件中

```
# 输出到滚动文件
log4j.appender.rollingFile=org.apache.log4j.RollingFileAppender
log4j.appender.rollingFile.encoding=${CHARSET}
log4j.appender.rollingFile.file=F:/logs/log4j.log
# 每个文件最多占用1MB
log4j.appender.rollingFile.maxFileSize=1MB
# 文件的最大索引是10,最多会产生11个文件
# 当文件数量满了,会优先删除事件较早的日志
log4j.appender.rollingFile.maxBackupIndex=10
log4j.appender.rollingFile.layout=org.apache.log4j.PatternLayout
log4j.appender.rollingFile.layout.conversionPattern=${PATTERN}
```

#### DailyRollingFileAppender - 根据日期格式将日志输出到文件（滚动）

RollingFileAppender 可以根据指定大小和数量，将内容输出到新的文件，但是不太方便根据时间定位日期，DailyRollingFileAppender 可以设置一个 **datePattern（日期格式）**，从而实现以日期来划输出的日志文件：

示例：

*   `datePattern='.'yyyy-MM-dd-HH-mm` 则会将每一秒的日志信息输出到不同文件；
*   `datePattern='.'yyyy-MM-dd` 则会将每一天的日志信息输出到不同的文件；

```
# 输出到滚动文件
log4j.appender.dailyRollingFile=org.apache.log4j.DailyRollingFileAppender
log4j.appender.dailyRollingFile.encoding=${CHARSET}
log4j.appender.dailyRollingFile.file=F:/logs/log4j.log
# 文件名格式,这里精确到秒
log4j.appender.dailyRollingFile.datePattern='.'yyyy-MM-dd-HH-mm
log4j.appender.dailyRollingFile.layout=org.apache.log4j.PatternLayout
log4j.appender.dailyRollingFile.layout.conversionPattern=${PATTERN}
```

Log4j 1.x - XML
---------------

示例代码：[log4j - xml](https://gitee.com/TAMADA/mj_java_frame/blob/master/04_SpringBoot/02_Log/01_Log4j1/src/main/resources/log4j_0.xml)

理解了前面的 properties 的写法后，利用 XML 去配置就会十分容易：

*   ConsoleAppender - 将日志输出到控制台
*   FileAppender - 将日志输出到单个文件
*   RollingFileAppender - 将日志输出到滚动文件
*   DailyRollingFileAppender - 将日志根据日期格式输出到滚动文件

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE log4j:configuration
        PUBLIC "-//log4j/log4j Configuration//EN" "log4j.dtd">
        
<log4j:configuration>
    <!-- appender -->
    <appender >
        <param />
        <layout class="org.apache.log4j.PatternLayout">
            <param %d{HH:mm:ss.SSS} [%-5p] %c: %m%n"/>
        </layout>
    </appender>
		
	<!-- 将日志输出到文件 -->
    <appender >
        <param />
        <param />
        <layout class="org.apache.log4j.PatternLayout">
            <param  
            	   value="%d{HH:mm:ss.SSS} [%-5p] %c: %m%n"/>
        </layout>
    </appender>

	<!-- 将日志输出到滚动文件 -->
    <appender >
        <param />
        <param />
        <param />
        <param />
        <layout class="org.apache.log4j.PatternLayout">
            <param  
           		    value="%d{HH:mm:ss.SSS} [%-5p] %c: %m%n"/>
        </layout>
    </appender>

	<!-- 将日志根据时间格式输出到滚动文件 -->
    <appender >
        <param />
        <param />
        <param />
        <layout class="org.apache.log4j.PatternLayout">
            <param  
            	   value="%d{HH:mm:ss.SSS} [%-5p] %c: %m%n"/>
        </layout>
    </appender>

    <!-- logger -->
    <logger >
        <level value="TRACE"/>
        <appender-ref ref="dailyRollingFile"/>
    </logger>

    <root>
        <level value="TRACE"/>
        <appender-ref ref="console"/>
        <appender-ref ref="file"/>
    </root>
</log4j:configuration>
```

### Filter - 让 Appender 在 Logger 的基础上过滤日志信息

DenyAllFilter 的作用是：关闭所有的日志输出

```
<appender >
    <layout class="org.apache.log4j.PatternLayout">
        <param  
        	   value="%d{HH:mm:ss.SSS} [%-5p] %c: %m%n"/>
    </layout>
    <!-- 关闭所有的日志输出 -->
    <filter class="org.apache.log4j.varia.DenyAllFilter">
</appender>
```

Filte 中还有以下的概念：

*   LevelMatchFilter - 根据日志级别进行过滤，常用来关闭某个级别的日志
*   LevelRangeFilter - 根据日志级别范围进行过滤
*   StringMatchFilter - 匹配 %m 中的字符串信息

LevelMatchFilter 的使用：关闭 INFO 级别、DEBUG 级别的日志

```
<!-- 关闭INFO级别的日志 -->
<filter class="org.apache.log4j.varia.LevelMatchFilter">
    <param />
    <param />
</filter>
<!-- 关闭DEBUG级别的日志 -->
<filter class="org.apache.log4j.varia.LevelMatchFilter">
    <param />
    <param />
</filter>
```

LevelRangeFilter 的使用：关闭 [DEBUG, ERROR] 之间的日志

```
<!-- 只输出[DEBUG, ERROR]之间的日志 -->
<filter class="org.apache.log4j.varia.LevelRangeFilter">
    <param />
    <param />
</filter>
```

StringMatchFilter 的使用：关闭 %m 中包含字符串 “E” 的日志

```
<!-- 关闭打印信息中包含字符串"E"的日志 -->
<filter class="org.apache.log4j.varia.StringMatchFilter">
    <param />
    <param />
</filter>
```

Log4j 1.x - 开启内部日志信息
--------------------

3 种方法开启内部日志信息：

1.  在 log4j.properties 中配置：

```
# log4j.properties
log4j.debug=true
```

2.  在 log4j.xml 中配置：

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE log4j:configuration
		  PUBLIC "-//log4j/log4j Configuration//EN" "log4j.dtd">
<log4j:configuration debug="true">	
</log4j>
```

3.  在代码中配置：

```
LogLog.setInternalDebugging(true);
```

JCL + Log4j 1.x
---------------

示例代码：[jcl + log4j1.x](https://gitee.com/)

添加依赖：

```
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>

<dependency>
    <groupId>commons-logging</groupId>
    <artifactId>commons-logging</artifactId>
    <version>1.2</version>
</dependency>
```

使用：

```
public class TestJCL_Log4j1 {
    public static void main(String[] args) {
        Log log = LogFactory.getLog(TestJCL_Log4j.class);
        log.fatal("致命_FATAL");
        log.error("错误_ERROR");
        log.warn("警告_WARN");
        log.info("信息_INFO");
        log.debug("调试_DEBUG");
        log.trace("痕迹_TRACE");
    }
}
```

使用 Lombook 自动生成 JCL 的 Log 定义：

```
@CommonsLog
public class TestJCL_Log4j {
	// 使用@CommonsLog相当于生成了下面的代码
    // private static final Log log = LogFactory.getLog(TestJCL_Log4j.class);

    public static void main(String[] args) {
    	log.fatal("致命_FATAL");
        log.error("错误_ERROR");
        log.warn("警告_WARN");
        log.info("信息_INFO");
        log.debug("调试_DEBUG");
        log.trace("痕迹_TRACE");
	}
}
```

### JCL - 实现原理

JCL 的实现原理是：JCL 内部实现了适配器，在适配器中调用 JUL、Log4j  
![](https://img-blog.csdnimg.cn/3a40378568894c0c8d068741a6cd0395.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)  
它有个致命的缺点：一旦要适配新推出的日志框架，JCL 需要修改内部源码，对框架的维护要求十分苛刻，目前已经被 apache 淘汰。