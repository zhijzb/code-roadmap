> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [blog.csdn.net](https://blog.csdn.net/weixin_43734095/article/details/119649232)

#### 日志处理 - SLF4J、Logback、[Log4j](https://so.csdn.net/so/search?q=Log4j&spm=1001.2101.3001.7020) 2.x

Gitee 代码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot)

SLF4J 对各种框架的支持：  
![](https://img-blog.csdnimg.cn/e1faa80160774bde82b6117fa3b6ddfc.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

SLF4J + Log4j 1.x
-----------------

引入依赖：

```xml
<!-- 依赖slf4j-api、log4j 1.x -->
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-log4j12</artifactId>
    <version>1.7.30</version>
</dependency>
```

使用示例：

*   在 SLF4J 中，没有 FATAL 级别（或者可以理解为 FATAL 等价于 ERROR）

```
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSLF4J_Log4j {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(TestSLF4J_Log4j.class);
        log.error("错误_ERROR");
        log.warn("警告_WARN");
        log.info("信息_INFO");
        log.debug("调试_DEBUG");
        log.trace("痕迹_TRACE");
    }
}
```

使用 Lombok 自动生成 SLF4J 的 Logger 定义：

```
@Slf4j
public class TestSLF4J_Log4j {
	// 使用@slf4j相当生成了下面的代码
    // private static final Logger logger =
    // LoggerFactory.getLogger(TestSLF4J_Log4j.class);
    public static void main(String[] args) {
        log.error("错误_ERROR");
        log.warn("警告_WARN");
        log.info("信息_INFO");
        log.debug("调试_DEBUG");
        log.trace("痕迹_TRACE");
    }
}
```

SLF4J + Logback
---------------

> Logback 语法比较自由，在它的 xml 中没有约束，log4j 1.x 存在 dtd 约束

引入依赖：

```
<!-- 依赖slf4j-api、logback-core -->
<dependency>
    <groupId>ch.qos.logback</groupId>
    <artifactId>logback-classic</artifactId>
    <version>1.2.3</version>
</dependency>
```

使用示例：

*   Logback 内部有一套默认配置，可以不用提供配置文件

> log4j 1.x 必须提供配置文件

```
@Slf4j
public class TestSLF4J_Logback {
	// 使用@slf4j相当生成了下面的代码
    // private static final Logger logger =
    // LoggerFactory.getLogger(TestSLF4J_Logback.class);
    public static void main(String[] args) {
        for (int i = 0; i < 500; i++) {
            log.error("错误_ERROR");
            log.warn("警告_WARN");
            log.info("信息_INFO");
            log.debug("调试_DEBUG");
            log.trace("痕迹_TRACE");
        }
	}
}
```

### Logback - 配置文件

配置文件位置：classpath:logback.xml

> Logback 和 log4j 1.x 的配置内容很像，学习起来比较轻松

```
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
    <!-- 抽取公共内容 -->
    <property  
     		  value="%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5p] [%t]: %m%n"/>
    <property />
    
    <appender >
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <charset>${CHARSET}</charset>
            <pattern>${PATTERN}</pattern>
        </encoder>
    </appender>
    
    <root level="INFO">
        <appender-ref ref="console"/>
    </root>

    <logger >
        <appender-ref ref="console"/>
    </logger>
</configuration>
```

### Logback - 控制台彩色打印

参考：[控制台彩色打印文档](https://logback.qos.ch/manual/layouts.html#coloring)

```
<property  
	      value="%d{HH:mm:ss.SSS} [%highlight(%-5p)] %cyan(%c{5}): %m%n"/>
<property />

<appender >
    <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
        <charset>${CHARSET}</charset>
        <pattern>${PATTERN}</pattern>
    </encoder>
</appender>
```

### Logback - configuration

configuration 标签的常用属性：

*   `debug="true"`：可以打印 Logback 内部的日志
*   `scan="true"` + `scanPeriod="30 seconds"`  
    每隔 30 秒扫描配置文件，并且应用配置文件的最新修改  
    scanPeriod 的单位可以是：milliseconds，seconds，minutes，hours

```
<?xml version="1.0" encoding="UTF-8"?>
<configuration debug="true" scan="true" scanPeriod="5 seconds">

</configuration>
```

### Logback - Appender 控制日志输出位置

和 Log4j 类似，Logback 中有以下几种 Appender：

*   ConsoleAppender - 将日志输出到控制台
*   FileAppender - 将日志输出到文件（单个）
*   RollingFileAppender - 将日志输出到文件（滚动）

FileAppender 使用：

```
<appender >
    <file>${BASE_PATH}/${BASE_NAME}.log</file>
    <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
        <charset>${CHARSET}</charset>
        <pattern>${FILE_PATTERN}</pattern>
    </encoder>
</appender>

<appender >
    <file>${BASE_PATH}/${BASE_NAME}.html</file>
    <encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
        <charset>${CHARSET}</charset>
        <layout class="ch.qos.logback.classic.html.HTMLLayout">
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS}%p%t%m</pattern>
        </layout>
    </encoder>
</appender>
```

RollingFileAppender 使用：

*   TimeBasedRollingPolicy：基于时间的滚动策略

```
<appender >
    <file>${BASE_PATH}/${BASE_NAME}.log</file>
    <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
        <charset>${CHARSET}</charset>
        <pattern>${PATTERN}</pattern>
    </encoder>
    <!-- 基于时间的滚动策略 -->
    <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
        <!-- 支持压缩 -->
        <fileNamePattern>${BASE_PATH}/logback_rolling_%d{yyy-MM-dd-HH-mm-ss}.log.gz</fileNamePattern>
        <!-- 删除20秒以前的日志文件(时间单位取决于fileNamePattern) -->
        <maxHistory>20</maxHistory>
        <!-- 总的日志大小限制(超过了,就删除最早的日志) -->
        <totalSizeCap>10KB</totalSizeCap>
    </rollingPolicy>
</appender>
```

*   SizeAndTimeBasedRollingPolicy：基于文件大小和时间的滚动

```
<!--基于文件大小和时间的滚动策略 -->
<rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
    <!-- 支持压缩 -->
    <fileNamePattern>${BASE_PATH}/logback_rolling_%d{HH-mm}_%i.log.gz</fileNamePattern>
    <!-- 删除20秒以前的日志文件(时间单位取决于fileNamePattern) -->
    <maxHistory>20</maxHistory>
    <!-- 当日志文件大小超过1MB,就生成新的日志文件 -->
    <maxFileSize>1MB</maxFileSize>
</rollingPolicy>
```

### Logback - Filter 在 Logger 基础上过滤日志输出信息

通过 Filter 设置只打印 WARN 级别的信息：

```
<appender >
    <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
        <charset>${CHARSET}</charset>
        <pattern>${CONSOLE_PATTERN}</pattern>
    </encoder>
    <!-- 只打印WARN级别的信息 -->
    <filter class="ch.qos.logback.classic.filter.LevelFilter">
        <level>WARN</level>
        <!-- 当匹配: 接收(开启打印) -->
        <onMatch>ACCEPT</onMatch>
        <!-- 当不匹配: 否定(关闭打印) -->
        <onMismatch>DENY</onMismatch>
    </filter>
</appender>
```

也可以配置关闭 WARN，只打印其他级别：

```
<!-- 关闭WARN,打开其他 -->
<filter class="ch.qos.logback.classic.filter.LevelFilter">
    <level>WARN</level>
    <onMatch>DENY</onMatch>
    <onMismatch>ACCEPT</onMismatch>
</filter>
```

### Logback - AsyncAppender 将写日志变为异步操作

在 Logback 中，可以使用 AsyncAppender 提高效率

*   使得写日志的操作，不会影响到程序的正常执行（一般用于将日志写到文件等耗时操作中）

```
<appender >
    <file>${BASE_PATH}/${BASE_NAME}.log</file>
    <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
        <charset>${CHARSET}</charset>
        <pattern>${PATTERN}</pattern>
    </encoder>
</appender>

<appender >
    <!-- 阻塞队列的容量 -->
    <param />
    <!-- 当阻塞队列的剩余20%容量时,会默认丢弃TRACE、DEBUG、INFO级别的日志 -->
    <!-- discardingThreshold设置为0,就不会丢弃 -->
    <param />
    <!-- 将file这个appender设置为异步的 -->
    <appender-ref ref="file"/>
</appender>
```

AsyncAppender 的原理：  
![](https://img-blog.csdnimg.cn/3189240b465e43a8b59ebe2cb642915f.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

Log4j 2.x
---------

示例代码：[log4j 2.x](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot/02_Log/05_Log4j2)

引入依赖：

```
<!-- 依赖log4j-api -->
<dependency>
    <groupId>org.apache.logging.log4j</groupId>
    <artifactId>log4j-core</artifactId>
    <version>2.13.3</version>
</dependency>
```

基本使用：Log4j 2.x 默认的日志等级是 ERROR（没有配置文件的情况下）

```
public class TestLog4j2 {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(TestLog4j2.class);
        logger.fatal("致命_FATAL");
        logger.error("错误_ERROR");
        logger.warn("警告_WARN");
        logger.info("信息_INFO");
        logger.debug("调试_DEBUG");
        logger.trace("痕迹_TRACE");
    }
}
```

### Log4j 2.x - 配置文件

配置文件：classpath:log4j2.xml

> 各种配置文件的配置大同小异，有前面的基础就很好理解了

```
<?xml version="1.0" encoding="UTF-8"?>
<Configuration>
    <Properties>
        <Property >%d{HH:mm:ss.SSS} [%-5p] %c{1.}: %m %n</Property>
        <Property >UTF-8</Property>
    </Properties>
    <Appenders>
    	<Console >
	    	<PatternLayout pattern="${PATTERN}" charset="${CHARSET}"/>
		</Console>
	</Appenders>
	<Loggers>
	    <Root level="TRACE">
	        <AppenderRef ref="Console"/>
	    </Root>
	    <Logger >
	        <AppenderRef ref="Console"/>
	    </Logger>
	</Loggers>
</Configuration>
```

Configuration 标签中的常用属性：

*   status：控制 Log4j **内部日志**的打印级别，比如 `status="WARN"`
*   monitorInterval：每隔多少秒扫描配置文件、应用配置文件的最新修改

```
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN" monitorInterval="5">
</Configuration>
```

### Log4j 2.x - 控制台彩色打印

参考：[http://logging.apache.org/log4j/2.x/manual/layouts.html#Patterns](http://logging.apache.org/log4j/2.x/manual/layouts.html#Patterns)

```
<Properties>
	<Property %style{%d{HH:mm:ss.SSS}}{black}\
[%highlight{%t}] %highlight{%-5p}\
%style{%c{1.}}{magenta}: %m%n"/>
    <Property >UTF-8</Property>
</Properties>

<Appenders>
	<Console >
		<!-- disableAnsi="false" 开启彩色打印 -->
	    <PatternLayout pattern="${PATTERN}" charset="${CHARSET}"
	    			   disableAnsi="false"/>
	</Console>
</Appenders>
```

### Log4j 2.x - Filter

Log4j 2.x 中常用的 Filter 有：

*   ThresholdFilter - 以某个级别为阈值，控制是否输出日志
*   LevelRangeFilter - 以某个范围来控制日志的输出
*   LevelMatchFilter - 匹配某个日志级别
*   DenyAllFilter - 禁止所有的日志级别
*   StringMatchFilter - 按照日志信息中的字符串来匹配

ThresholdFilter 使用：

```
<Console >
    <PatternLayout pattern="${PATTERN}" charset="${CHARSET}"/>
    <!-- 达到WARN -> DENY -> 关闭 -->
    <!-- 没达到WARN -> ACCEPT -> 开启 -->
	<ThresholdFilter level="WARN" onMatch="DENY" onMismatch="ACCEPT"/>
</Console>
```

*   ACCEPT：开启；会忽略后面所有的 Filter
*   DENY：关闭；会忽略后面所有的 Filter
*   NEUTRAL：中立。会传递给

```
<!-- 只开启[DEBUG、WARN]级别的日志 -->
<Filters>
	<ThresholdFilter level="ERROR" onMatch="DENY" onMismatch="NEUTRAL">
	<ThresholdFilter level="DEBUG" onMatch="ACCEPT" onMismatch="DENY">
</Filters>
```

LevelRangeFilter 使用：

*   注意，在 Log4j 2.x 的 LevelRangeFilter 中，  
    minLevel 是填写最大级别、maxlLevel 是填写最小级别

```
<!-- 只开启[DEBUG, WARN]级别的日志 -->
<Filters>
	<LevelRangeFilter minLevel="WARN" maxLevel="DEDBUG"
					  onMatch="ACCEPT" onMismatch="DENY"/>
</Filters>
```

### Log4j 2.x - Appender

日志框架的结构都差不多，Log4j 2.x 中可以直接通过标签来指定 Appender：

> Log4j 1.x 和 Logback 中通过 <Appender> 标签 和 class 属性指定 Appender

*   <Console>：将日志输出到控制台
*   <File>：将日志输出到文件（单个）
*   <RollingFile>：将日志输出到文件（滚动）

**<Console>**：

*   target：可以取值 `SYSTEM_OUT`（默认值）、`SYSTEM_ERR`，控制输出到控制台的颜色

```
<Appenders>
	<Console >
	    <PatternLayout pattern="${PATTERN}" charset="${CHARSET}"/>
	</Console>
</Appenders>
```

**<File>**：

```
<Properties>
    <Property >%d{HH:mm:ss.SSS} [%-5p] %c{1.}: %m %n</Property>
    <Property >UTF-8</Property>
    <Property >F:/logs</Property>
    <Property >log4j2</Property>
</Properties>
```

```
<File ${BASE_PATH}/${BASE_NAME}_file.log">
	<PatternLayout pattern="${PATTERN}" charset="${CHARSET}"/>
</File>
```

[<RollingFile>](http://logging.apache.org/log4j/2.x/manual/appenders.html#RollingFileAppender)：

```
<RollingFile 
             fileName="${BASE_PATH}/${BASE_NAME}.log"
             filePattern="${BASE_PATH}/%d{yyyy}/%d{MM}/%d{dd}/HH_mm_%i.log.gz">
    <PatternLayout pattern="${PATTERN}" charset="${CHARSET}"/>
    <Policies>
        <!-- 基于时间的滚动策略：当时间跨度达到2分钟就滚动 -->
        <!-- 时间单位取决于filePattern的最小时间单位 -->
        <TimeBasedTriggeringPolicy interval="2"/>

        <!-- 基于文件大小的滚动策略：当文件大小达到10KB时就滚动 -->
        <SizeBasedTriggeringPolicy size="10KB"/>
    </Policies>
    <!-- 默认值是7,设置%i的最大值 -->
    </DefaultRolloverStrategy max="10">
</RollingFile>
```

#### 滚动策略 - Delete

对于输出到滚动文件而言，可以设置滚动策略：

参考文档：[Delete](http://logging.apache.org/log4j/2.x/manual/appenders.html#Log_Archive_Retention_Policy:_Delete_on_Rollover)

```
<!-- 设置%i的最大值，默认是7 -->
<DefaultRolloverStrategy max="100">
    <!-- maxDepth: 要访问目录的最大级别数, 默认值1, 代表仅访问basePath目录中的文件 -->
    <Delete basePath="${BASE_PATH}" maxDepth="10">
        <!-- IfFileName && IfLastModified -->
        <!-- 填写相对于basePath的相对路径 -->
        <IfFileName glob="*.log.gz"/>
        <!-- 文件的时间超过5s -->
        <IfLastModified age="5s">
            <!-- IfAccumulatedFileSize || IfAccumulatedFileCount -->
            <IfAny>
                <IfAccumulatedFileSize exceeds="20KB"/>
                <IfAccumulatedFileCount exceeds="10"/>
            </IfAny>
        </IfLastModified>
    </Delete>
</DefaultRolloverStrategy>
```

### Log4j 2.x - Loggers

```
<Loggers>
    <Root level="WARN">
        <AppenderRef ref="Console"/>
    </Root>
	<Logger >
		<AppenderRef ref="RollingFile"/>
	</Logger>
</Loggers>
```

### Log4j 2.x - Async

```
<Console >
	<PatternLayout pattern="${PATTERN}" charset="${CHARSET}"/>
</Console>
<Async >
    <Appender-ref redf="Console"/>
</Async>
```

SLF4J + Log4j 2.x
-----------------

示例代码：[SLF4J + Log4j 2.x](https://gitee.com/szluyu99/mj_java_frame/tree/master/04_SpringBoot/02_Log/06_SLF4J_Log4j2)

引入依赖：

```
<!-- 依赖slf4j-api、log4j-api、log4j-core -->
<dependency>
	<groupId>org.apache.logging.log4j</groupId>
	<artifactId>log4j-slf4j-impl</artifactId>
	<version>2.13.3</version>
</dependency>
```

代码示例：

```
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSLF4J_Log4j2 {
    public static void main(String[] args) {
    	Logger logger = LoggerFactory.getLogger(TestSLF4J_Log4j2.class);
        log.error("错误_ERROR");
        log.warn("警告_WARN");
        log.info("信息_INFO");
        log.debug("调试_DEBUG");
        log.trace("痕迹_TRACE");
    }
}
```

使用 `@slf4j` 注解：

```
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestSLF4J_Log4j2 {
    public static void main(String[] args) {
        log.error("错误_ERROR");
        log.warn("警告_WARN");
        log.info("信息_INFO");
        log.debug("调试_DEBUG");
        log.trace("痕迹_TRACE");
    }
}
```