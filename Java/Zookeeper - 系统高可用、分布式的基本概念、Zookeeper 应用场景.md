
系统高可用
-----

**系统高可用**，主要是指两个方面：

1.  系统的健壮性，不允许系统出现**单点故障**
2.  系统的处理能力，可以提高系统的处理能力，保证系统的运行效率

> **单点故障**：系统中出现一点故障，就导致整个系统瘫痪

### 集群 - 主备集群、主从集群、普通集群

**集群**，主要是指：原来使用是一台服务器处理，现在使用多台服务器保障系统的运行

1.  **主备集群**：有一个主要节点提供服务，另外的节点处于备份状态，平时不提供工作，一旦主节点出现问题，备份节点启动运行，提供正常的服务  
    ![](http://121.41.87.138:9001/picgo/img/f58e6303dc0146eb9c7e6b245f010fa6_repeat_1706256657249__813286.png)
    
2.  **主从集群**：集群中的节点都提供服务，但是每台服务器的角色可能不一样，比如配置数据库的**读写分离**，主数据库可能是写操作，对于实时性要求不高的读操作就使用从数据库  
    ![](http://121.41.87.138:9001/picgo/img/290805e4aec142b68eeec2f0d061aef6_repeat_1706256705709__158065.png)
    
3.  **普通集群**：集群中的节点提供的功能是一样的，所有的节点没有主从之分，主要是提高系统的高可用（并发度、吞吐量，避免单点故障）  
    ![](http://121.41.87.138:9001/picgo/img/64de4d1ffd9449aba176e71c01611e25_repeat_1706256710674__972496.png)
    

> 一个 tomcat 的并发请求最高大约在 350（并发线程数） 左右（不做特别的调优），默认最大线程数是 150

### 分布式（系统部署方式）

**分布式** 是**系统部署方式**，比如我们的业务系统，部署一个业务系统需要的环境（应用服务 Tomcat + 数据库服务 MySQL）

*   **单机部署**：把 Tomcat 和数据库 MySQL 服务部署在同一台服务器，这样 Tomcat 和 MySQL 之间的网络开销可以忽略（直接走 127.0.0.1 不会消耗网络）
*   **分布式应用**：把 Tomcat 和数据库 MySQL 服务部署在不同的服务器，则 Tomact 和 MySQL 之间需要走网络通信， 这种需要走网络的部署方式称为 “分布式应用”  
    ![](http://121.41.87.138:9001/picgo/img/0b7256ceb38c4bf09ec197e9633d096f_repeat_1706256721109__540667.png)

### [微服务](https://so.csdn.net/so/search?q=%E5%BE%AE%E6%9C%8D%E5%8A%A1&spm=1001.2101.3001.7020)（架构设计方式）

**微服务**，指的是系统的**架构设计方式**（区别于分布式指的是部署方式），微服务一定是分布式，但是分布式不一定是微服务

*   **单体应用**：将所有的功能都放到一个项目（应用）中，一个模块出问题会导致整个项目出问题；或者要更新某个模块时，需要把整个系统停掉，导致其他模块都不能用。
*   随着项目开发的功能变多，架构变强，我们需要根据模块来进行划分，每个模块之间通过服务之间的网络调用，称之为**微服务架构**  
    ![](http://121.41.87.138:9001/picgo/img/29ccb61dae54470399f8559456df680a_repeat_1706256725387__711082.png)

分布式的基本概念
--------

### 分布式存储、分布式计算

分布式在大数据时代是非常重要而基础的一个概念。

我们在大数据时代面临的主要问题有：

*   海量数据的存储 —> **分布式存储**
*   海量数据的运算 —> **分布式计算**
*   高并发的请求 —> **分布式系统**

**分布式存储**  
![](https://img-blog.csdnimg.cn/a56a4be042194195b7e3926e3f270628.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)  
**分布式计算**，把一个大的计算任务分发成多个小的计算任务，并行运算，最终把结果进行汇总。

*   对于分布式计算，我们强调的是**移动运算**，而不是移动数据  
    ![](https://img-blog.csdnimg.cn/3079640ae9f14b9792885e774ac6e731.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

### 分布式协调服务

分布式是指整个应用是由部署在多个机器上的服务去统一完成

*   对于部署在多个机器上的应用，通常有不同的角色，比如有 Master 和 Slave 角色的区分（主从）

**分布式协调服务**：主要是指 Zookeeper 在分布式系统中充当一个协调者的角色，帮助具体的业务系统之间的相互协调，保证系统的正常运行

*   对于 Zookeeper 集群， 也会有多个节点，主要包括 leader 节点和 follower 节点

分布式协调服务流程：

1.  启动分布式协调服务 Zookeeper
2.  启动秒杀服务，启动的时候往 Zookeeper 注册自己的信息（id 地址 + 端口）和名称
3.  启动客户端，去 Zookeeper 中找到对应的秒杀服务的地址信息
4.  从地址列表信息中随机选取一个节点地址和端口
5.  直接发送远程服务调用（RPC 调用 或 HTTP 调用）  
    ![](https://img-blog.csdnimg.cn/92cc1b3dc68147bbb429106621ab035c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

Zookeeper 基础 - 应用场景
-------------------

### 服务器在线感知

**服务器在线感知**

*   所有服务器启动的时候，都向 Zookeeper 中的一个指定目录写入一个数据 `/server/serverxxx`
*   客户端连接 Zookeeper，获取到 /servers/ 目录下面所有的可用的服务信息
*   客户端监听 /servers 这个目录下面的数据改变；  
    如果这个目录下的数据发生改变，Zookeeper 会及时通知客户端数据已经发生改变
*   客户端收到 Zookeeper 的通知，及时的去获取最新的数据
*   当应用程序宕机，在 Zookeeper 中会及时的删除对应的服务信息

### 主从协调

**主从协调**，对于我们的集群环境中的多个机器，其中一台是处于活跃状态，可以正常的提供对应的服务，另外的服务器处于备份状态；只有当活跃状态的机器出现问题，不能提供服务的时候，才会把备份状态的机器切换为活跃状态

*   Server01 是 active 状态的机器，向 Zookeeper 中的 /server / 目录写入数据 server01
*   Server02 是 standBy 状态的机器，向 Zookeeper 中监听 /server/ 目录中的数据改变
*   当 Server01 出现宕机，删除 /server/server01 这个数据，并且及时的通知 Server02
*   Server02 收到通知后启动服务，并且向 Zookeeper 注册自己的信息 /server/server02
*   客户端只需要找到 Zookeeper 中 /server/ 目录下面的服务信息，即可正常的访问服务  
    ![](https://img-blog.csdnimg.cn/d472163ea55c4a5b91ebe1f0c1995b4e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

### 配置管理

**配置管理**，在大型应用中，对于一个系统的配置会有许多参数，比如：数据库的配置、Tomcat 的线程数等；如果不使用统一的**配置管理中心**的话，则需要在每个应用服务中去进行一个单独的配置，这样操作麻烦且容易出错，可以使用一个统一的配置管理：

*   提供一个配置管理程序，用于向 Zookeeper 中写入对应的数据，主要包括属性名称和属性值
*   所有的服务启动时都去读取 Zookeeper 中的配置信息并加载，从而完成系统的正常启动  
    ![](https://img-blog.csdnimg.cn/50555fe4898445b6b6d5760dcfda847a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

### 名称服务

**名称服务**，就是指：通过指定的名字来获取资源或者服务的地址。Zookeeper 会在自己的文件系统上（树结构的文件系统）创建一个以路径为名称的节点，它可以指向提供服务的地址，远程对象等。

*   简单来说 Zookeeper 命名服务就是用路径作为名字，路径上的数据就是其名字指向的实体。  
    ![](https://img-blog.csdnimg.cn/dd9717d554754ddc9a9c89405dc50ff5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

### [分布式锁](https://so.csdn.net/so/search?q=%E5%88%86%E5%B8%83%E5%BC%8F%E9%94%81&spm=1001.2101.3001.7020)

**分布式锁**，在分布式系统的架构设计中，有时候需要保证分布式系统中的对于某些接口的原子性的操作，需要控制在同一时刻只能有一个应用程序可以正常操作，其他的程序必须等待在操作的程序完成以后才可以正常的操作数据。

例如有一个生成 ID 的接口服务：

*   所有需要访问生成 ID 接口服务的业务系统都去 Zookeeper 指定目录生成一个服务地址 `/lock/serverxxx`
*   所有的业务系统都判断一下，自己生成的服务地址是否是所有的地址列表中的最小的一个，如果是最小的一个，则可以正常访问接口；否则，处于等待状态
*   当获得锁对象的应用访问完接口以后，需要删除自己在 Zookeeper 中的服务列表，然后再进行对应的操作
*   其他的应用再去判断自己的数据是否是 Zookeeper 中地址列表中的最小的一个数据，如果是，则可以获取到锁对象，开始资源对象的访问  
    ![](https://img-blog.csdnimg.cn/89599c6fb0b64c5cacf2429789a421cc.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)