> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [www.yiibai.com](https://www.yiibai.com/oracle/oracle-11g-install.html)

> 在本教程中，我们将逐步介绍如何下载 Oracle 11g 数据库并演示如何安装在 Windows 10 64 位机器上，以便在系统中学习和实践 PL/SQL 编程。

在本教程中，我们将逐步介绍如何下载 Oracle 11g 数据库并演示如何安装在 Windows 10 64 位机器上，以便在系统中学习和实践 _PL/SQL_ 编程。

下载并安装 Oracle 数据库
----------------

首先，需要到 Oracle 官方网站下载 Oracle 数据库，在这里我们将[下载 Oracle 11g 第 2 版的 Microsoft Windows(x64) 版本](http://www.oracle.com/technetwork/database/enterprise-edition/downloads/index.html "下载Oracle 11g第2版的Microsoft Windows(x64)版本")。



> 官网可能是另外的版本
>
> [备份分享-百度云分享](https://pan.baidu.com/s/1wvz-fE3SRjHqvJHHQoaQSg?pwd=24e9)
> 提取码：24e9 

由于安装文件过大，可以分两个文件下载或一次性下载，如下图所示 
![](http://121.41.87.138:9001/picgo/img/154816_29969_repeat_1706168869966__640968.png)

![image-20240125161711164](http://121.41.87.138:9001/picgo/img/image-20240125161711164_repeat_1706170631544__884394.png)

**下载完成后，将下载的文件都解压缩到一个同一文件夹中 (例如：_C:\课程\oracle安装包\win64_11gR2_database_all_)，如下所示：**

![image-20240125162056860](http://121.41.87.138:9001/picgo/img/image-20240125162056860_repeat_1706170857071__914016.png)

双击 _setup.exe_ 文件开始安装 Oracle 数据库。需要按照以下步骤操作：

**第 1 步：**

在此步骤中，可以提供您的电子邮件，以获取有关 Oracle 安全问题的更新信息。但为了方便，这里我们不提供电子邮件。  
![](http://121.41.87.138:9001/picgo/img/154922_11001_repeat_1706169137590__018947.png)

确认不提供电子邮件，点击 【_是 (Y)_】继续 -

![](https://www.yiibai.com/uploads/images/2018/12/16/154956_61277.png)

**第 2 步：**

此步骤中有三个选项，如屏幕截图所示。选择第一个【_创建和配置数据库_】，然后单击【下一步】按钮。  
![](http://121.41.87.138:9001/picgo/img/155029_41860_repeat_1706169140064__486748.png)

_第 3 步：_

如果要在笔记本电脑或桌面上安装 Oracle 数据库，请选择第一个选项【桌面类】，否则选择第二个选项，然后单击【下一步】按钮。  
![](https://www.yiibai.com/uploads/images/2018/12/16/155106_86775.png)

_第 4 步：_

此步骤允许要求输入完整的数据库安装文件夹。可以更改 Oracle 基本文件夹 (例如：_E:\oracle11g\Administrator_)，其他文件夹将相应更改。填写上管理员密码，之后单击【下一步】按钮进入下一步。  

> 密码要求

![](http://121.41.87.138:9001/picgo/img/155132_13587_repeat_1706169143336__521287.png)

_第 5 步：_

在此步骤中，Oracle 将在安装 Oracle 数据库组件之前执行先决条件检查。  
![](http://121.41.87.138:9001/picgo/img/155217_83632_repeat_1706169187130__513762.png)

_第 6 步：_

此步骤将显示上一步骤检查的摘要信息，单击【完成】按钮开始安装 Oracle 数据库。  
![](http://121.41.87.138:9001/picgo/img/155247_99615_repeat_1706169195791__911289.png)

_第 7 步：_

此步骤将文件复制到相应的文件夹并安装 Oracle 组件和服务。完成所需的时间需要几分钟，请耐心等待。

![](http://121.41.87.138:9001/picgo/img/155318_74203_repeat_1706169201719__108177.png)

完成后，安装程序将显示 “_数据库配置助理_” 对话框 -  
![](http://121.41.87.138:9001/picgo/img/155344_56709_repeat_1706169206459__531456.png)

单击【口令管理】按钮设置不同用户的密码，这里我们将`SYS`，`SYSTEM`和`HR`用户解锁并设置相应的密码 -

![](https://www.yiibai.com/uploads/images/2018/12/16/155412_38253.png)

完成后，点击【确定】。

_第 8 步：_

安装过程完成后，单击【关闭】按钮来关闭安装程序。到此，Oracle 11g 已经成功安装好了。  
![](https://www.yiibai.com/uploads/images/2018/12/16/155435_22442.png)

验证安装情况
------

如果上面安装步骤正常通过，在【开始】->【所有应用】将看到 Oracle 文件夹如下：  
![](https://www.yiibai.com/uploads/images/2018/12/16/155458_53329.png)

首先，启动 _SQL Plus_，它是一个可用于与 Oracle 数据库交互的命令行界面工具。

然后，输入您在安装过程中设置的用户名和密码，我们使用`HR`用户登录，所以这里输入`HR`用户及其相应的密码。  
![](https://www.yiibai.com/uploads/images/2018/12/16/155525_41526.png)

输入以下语句：

```
SELECT * FROM dual;
```

如果能看到输出如下面的屏幕截图所示，那么说明您已经成功安装了 Oracle 数据库。  
![](https://www.yiibai.com/uploads/images/2018/12/16/155548_87722.png)

在本教程中，我们演示了如何安装 Oracle 数据库，并使用 _SQL Plus_ 验证安装结果。希望您在学习 Oracel 也亲自体一下 Oracle 安装过程。在安装过程中，如果没有完全成功，请再次查看每个步骤，并在必要时进行适当的更正。





## 口令设置问题

若在安装过程中，在口令管理设置页面直接选择确定，则需要后续操作进行修改用户口令。

<img src="http://121.41.87.138:9001/picgo/img/image-20240125172720718_repeat_1706174841370__039588.png" alt="image-20240125172720718" style="zoom:33%;" />以管理员身份进入

```shell
sqlplus /nolog；
```

```shell
conn/as sysdba;
# 或者 
conn sys as sysdba;
```

```shell
# alter修改用户的密码，如修改SYSTEM密码
alter user SYSTEM identified by 密码
```



