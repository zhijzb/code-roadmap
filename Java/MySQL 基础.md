#### MySQL

如何长久地存储数据？（**持久化**）  
![](https://img-blog.csdnimg.cn/73a3ef62877f4fa5bedc26e564334bdd.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

*   通常会使用**数据库 (Database)** 来长久地存储数据

使用数据库的明显好处？

*   可以高效地存储、查询数据
*   减少重复、冗余数据
*   提高数据的安全性

数据库分为：**关系型数据库 (Relational Database)**、**非关系型数据库 (NoSQL Database)**  
![](https://img-blog.csdnimg.cn/3dd36c4ccdd94052b1291cf990adbdc7.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

MySQL
-----

MySQL 是一款开源的关系型数据库，有免费版（社区版）、商业版

2008 年被 Sun 公司在 2009 年被 Oracle 公司收购

> 关于 MySQL 的发音：  
> [https://dev.mysql.com/doc/refman/8.0/en/what-is-mysql.html](https://dev.mysql.com/doc/refman/8.0/en/what-is-mysql.html)  
> 官方发音：My-S-Q-L，基本都习惯发音为 My Sequel

MySQL 下载地址：[https://downloads.mysql.com/archives/installer/](https://downloads.mysql.com/archives/installer/)

[MySQL 官方文档](http://www.searchdoc.cn/rdbms/mysql/dev.mysql.com/doc/refman/5.7/en/index.com.coder114.cn.html)

### MySQL 的使用步骤

![](https://img-blog.csdnimg.cn/9fafa058eb9648ad89742ec342c2ce19.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

登录、连接 MySQL 服务器（管理员账户名是 root）

```
mysql -uroot -proot # 密码为root
mysol -uroot -p # 回车后输入密码
```

使用 SQL 对数据库进行 CRUD：

*   Create：创建，增
*   Read：读取，查
*   Update：更新，改
*   Delete：删除，删

在 Windows 中，可以通过以下命令查看占用 3306 端口号的进程 ID：

```
netstat -aon|findstr 3306
```

### 数据库的内部存储细节

![](https://img-blog.csdnimg.cn/046c183c54af4884b127d5531e5d1d60.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

*   一个**数据库 (Database)** 中可以存放多张**表 (Table)**
*   每个表 (Table) 中包含一些**列 (Column，也叫做字段）**
*   每个表 (Table) 中存放的数据，一般称为**记录 (Record)**

### [GUI](https://so.csdn.net/so/search?q=GUI&spm=1001.2101.3001.7020) 工具

在实际开发过程中，经常会使用图形化界面工具来管理数据库。

*   [Navicat Premium](http://www.navicat.com.cn/products/navicat-premium)  
    支持 MySQL、MariaDB、MongoDB、SQL Server、Oracle、PostgresQL、SQLite
*   [SQLyog](https://www.webyog.com/product/sqlyog)  
    仅支持 MySQL

### SQL 语句

SQL 是 Structured Query Language 的简称，译为 "结构化查询语言"，用于操作关系型数据库

SQL 语句主要可以分成 4 大类：

*   **DDL (Data Definition Language)**  
    数据定义语言  
    创建 (`CREATE`)、修改 (`ALTER`)、删除 (`DROP`) **数据库 \ 表**
*   **DQL (Data Query Language)**  
    数据查询语言  
    查询 **记录** (`SELECT`)
*   **DML (Data Manipulation Language)**  
    数据操纵语言  
    增加 (`INSERT`)、删除 (`DELETE`)、修改 (`UPDATE`) **记录**
*   **DCL (Data Control Language)**  
    数据控制语言  
    控制**访问权限** (`GRANT`、`REVOKE`)

SQL 语句注意点：

*   每一条语句是分号 `;` 结束
*   不区分大小写，建议：关键字使用大写，其他使用小写，单词之间用下划线连接  
    比如 `my_name`、`my_first_name`
*   单行注释
    
    ```
    -- 注释内容
    #注释内容
    ```
    
*   多行注释
    
    ```
    /* 注释内容 */
    ```
    

参考资料：[【官方文档】第 13 章 SQL 语句语法](http://www.searchdoc.cn/rdbms/mysql/dev.mysql.com/doc/refman/5.7/en/sql-syntax.com.coder114.cn.html)

DDL 语句
------

### DDL 语句 - 数据库

创建：

```
CREATE DATABASE 数据库名 # 创建数据库(使用默认的字符编码)
CREATE DATABASE 数据库名 CHARACTER SET 字符编码 # 创建数据库(使用指定的字符编码)
CREATE DATABASE IF NOT EXISTS 数据库名 # 如果这个数据库不存在,才创建它
CREATE DATABASE IF NOT EXISTS 数据库名 CHARACTER SET 字符编码
```

查询：

```
SHOW DATABASES # 查询所有的数据库
SHOW CREATE DATABASE 数据库名 # 查询数据库的创建语句
USE 数据库名 # 使用数据库
SELECT DATABASE() # 查询正在使用的数据库
```

修改：

```
ALTER DATABASE 数据库名 CHARACTER SET 字符编码 # 修改数据库的字符编码
```

删除：

```
DROP DATABASE 数据库名
DROP DATABASE IF EXISTS 数据库名 # 如果这个数据库存在,才删除它
```

### DDL 语句 - 表

创建（基本语法）：

```
CREATE TABLE 表名 {
	列名1 数据类型1,
	列名2 数据类型2,
	列名3 数据类型3,
	...
	列名n 数据类型n
}
```

查询：

```
SHOW TABLES # 查询当前数据库的所有表
DESC 表名 # 查看表结构
```

删除：

```
DROP TABLE 表名
DROP TABLE IF EXISTS 表名 # 如果这个表存在,才删除它
```

修改：

```
ALTER TABLE 表名 RENAME TO 新表名 # 修改表名
ALTER TABLE 表名 CHARACTER SET 字符编码 # 修改表格的字符编码
ALTER TABLE 表名 ADD 列名 数据类型 # 增加新的一列
ALTER TABLE 表名 MODIFY 列名 新数据类型 # 修改某一列的数据类型
ALTER TABLE 表名 CHANGE 列名 新列名 新数据类型 # 修改某一列的列名、数据类型
ALTER TABLE 表名 DROP 列名 # 删除某一列
```

### 常用数据类型 - 数字类型

参考资料：[【官方文档】11.1.1 数字类型概述](http://www.searchdoc.cn/rdbms/mysql/dev.mysql.com/doc/refman/5.7/en/numeric-type-overview.com.coder114.cn.html)、[【官方文档】11.8 数据类型存储要求](http://www.searchdoc.cn/rdbms/mysql/dev.mysql.com/doc/refman/5.7/en/storage-requirements.com.coder114.cn.html)  
![](https://img-blog.csdnimg.cn/42322f64b5a046a58463a9fb7edce123.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
上述整数类型的使用格式：`TYPE[(M)] [UNSIGNED] [ZEROFILL]`

*   `UNSIGNED`：无符号
*   `ZEROFILL`：等价于 `UNSIGNED ZEROFILL`，显示数值时，若数字不足 M 位，在前面用 0 填充

![](https://img-blog.csdnimg.cn/fa2165f5c2d146b39a2251ebc71f8267.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

### 常用数据类型 - 字符串类型

参考资料：[【官方文档】11.4.1 CHAR 和 VARCHAR 类型](http://www.searchdoc.cn/rdbms/mysql/dev.mysql.com/doc/refman/5.7/en/char.com.coder114.cn.html)  
![](https://img-blog.csdnimg.cn/61144a76a8c2487fa3d63e33cf8c4645.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)

### 常用类型 - 日期和时间类型

![](https://img-blog.csdnimg.cn/212ecc01a1214b53a84aca781cce748e.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)  

| 类型      | 字节<br />MySQL 5.6.4之前 | 字节<br />MySQL 5.6.4开始 | 显示格式                       |
| --------- | ------------------------- | ------------------------- | ------------------------------ |
| YEAR      |                           |                           | YYYY                           |
| DATA      |                           |                           | YYYY-MM-DD                     |
| TIME      |                           |                           | HH:MM:SS[.fraction]            |
| DATATIME  |                           |                           | YYYY-MM-DD HH:MM:SS[.fraction] |
| TIMESTAMP |                           |                           |                                |

从 MySQL 5.6.4 开始，允许 `TIME` \ `DATETIME` \ `TIMESTAMP` 有小数部分，需要 0 ~ 3 字节的存储

*   DATETIME 支持范围：`1001-01-01 00:00:00.000000` 到 `9999-12-31 23:59:59.999999`
*   TIMESTAMP 支持范围：`1970-01-01 00:00:01.000000` 到 `2038-01-19 03:14:07.999999`

DATETIME \ TIMESTAMP 的自动设置：

*   `DEFAULT CURRENT_TIMESTAMP`  
    当**插入**记录时，如果没有指定时间值，就设置时间为当前的系统时间
*   `ON UPDATE CURRENT_TIMESTAMP`  
    当**修改**记录时，如果没有指定时间值，就设置时间为当前的系统时间

### TRUNCATE

如果要删除表中的所有数据（保留表结构），有 2 种常见做法：

```
DELETE FROM 表名 # 逐行删除每一条记录
```

```
TRUNCATE [TABLE] 表名 # 先删除后重新创建表(效率更高)
```

*   TRUNCATE 归类于 **DDL** 语句，而非 **DML** 语句
*   为了实现高性能，TRUNCATE 绕过了删除数据的 DML 方法。因此**它不能被回滚**，不会导致 ON DELETE 触发器触发，并且不能对 InnoDB 具有父子外键关系的表执行

> 参考资料：[【官方文档】13.1.34 TRUNCATE TABLE 语法](http://www.searchdoc.cn/rdbms/mysql/dev.mysql.com/doc/refman/5.7/en/truncate-table.com.coder114.cn.html)

### 表的复制

创建一张拥有相同表结构的空表（只复制表结构，不复制记录）

```
CREATE TABLE new_table LIKE old_table
```

创建一张拥有相同表结构、相同记录的表（复制表结构、复制记录）

```
CREATE TABLE new_table AS (SELECT * FROM old_table) # 可以省略 AS
```

列的常用属性
------

`NOT NULL`：不能设置为 NULL 值

`COMMENT`：注释

`DEFAULT`：默认值

> `BLOB`、`TEXT`、`GEOMETRY`、`JSON` 类型 不能有默认值

`AUTO_INCREMENT`：自动增长

*   适用于 `INT`、`FLOAT`、`DOUBLE` 类型
*   在插入记录时，如果不指定此列的值或者设置为 NULL，会在此前的基础上自动增长 1
*   不能有默认值（不能使用 `DEFAULT`）
*   在一个表格中，最多只能有一列被设置为 `AUTO_INCREMENT`  
    这一列必须被索引（`UNIQUE`、`PRIMARY KEY`、`FOREIGN KEY` 等）

DML 语句
------

增加：

```sql
INSERT INTO 表名(列名1, 列名2, ..., 列名n) VALUES (值1, 值2, ..., 值n)
```

> 非数字类型的**值**，一般需要用引号括住（单引号或双引号，建议使用单引号）

```sql
INSERT INTO 表名 VALUES (值1, 值2, ..., 值n) # 从左至右按顺序给所有列添加值
```

修改：如果没有添加条件，将会修改表中的所有记录

```
UPDATE 表名 SET 列名1=值1, 列名2=值2, ..., 列名n=值n [WHERE条件]
```

删除：如果没有添加条件，将会删除表中的所有记录

```
DELETE FROM 表名 [WHERE 条件]
```

DQL 语句
------

```
SELECT [DISTINCT] 列名1, 列名2, ..., 列名n
FROM 表名
[WHERE ...]
[GROUP BY ...]
[HAVING ...]
[ORDER BY ...]
[LIMIT ...]
```

```
SELECT * FROM customer # 查询表中的所有记录
SELECT DISTINCT * FROM customer # 查询表中的所有记录(去除了重复的记录)
```

### [聚合函数](https://so.csdn.net/so/search?q=%E8%81%9A%E5%90%88%E5%87%BD%E6%95%B0&spm=1001.2101.3001.7020) (Aggregate Function)

参考资料：[【官方文档】12.19.1 聚合函数描述](http://www.searchdoc.cn/rdbms/mysql/dev.mysql.com/doc/refman/5.7/en/group-by-functions.com.coder114.cn.html)

```
SELECT COUNT(*) FROM customer # 查询表中的记录总数
SELECT COUNT(phone) FROM customer # 查询表中phone的总数(不包括NULL)

SELECT COUNT(DISTINCT phone) FROM customer 
# 查询表中phone的总数(不包括NULL,去除重复的记录)
```

```
SELECT SUM(salary) FROM customer # 计算所有salary的总和
SELECT MIN(age) FROM customer # 查询最小的age
SELECT MAX(age) FROM customer # 查询最大的age
SELECT AVG(salary) FROM customer # 计算所有salary的平均值
```

### 常见的 WHERE 字句

比较运算：

```
WHERE age > 18 # age大于18
WHERE age <= 30 # age小于等于30
WHERE age = 20 # age等于20
WHERE name = '张三' # name是张三
WHERE age != 25 # age不等于25
WHERE age <> 25 # age不等于25
```

**NULL 值判断**：不能使用 `=`、`!=`、`<>`

```
WHERE phone IS NULL # phone的值为NULL
WHERE phone IS NOT NULL # phone的值不为NULL
```

逻辑运算：

```
# age大于18并且小于等于30
WHERE age > 18 AND age <= 30
WHERE age > 18 && age <= 30

WHERE age BETWEEN 20 AND 30 # age大于等于20并且小于等于30

# age等于18或者等于20或者等于22
WHERE age = 18 OR age = 20 OR age = 22
WHERE age in (18, 20, 22)

# age大于等于18
WHERE NOT (age < 18)
WHERE !(age < 18)
```

模糊查询：`_` 代表单个任意字符，`%` 代表任意个任意字符

```
WHERE name LIKE '___' # name是3个字符
WHERE name LIKE '_码_' # name是3个字符并且中间是'码'
WHERE name LIKE '李%' # name是以'李'字开头
WHERE name LIKE '_码%' # name的第2个字符是'码'字
WHERE name LIKE '%码%' # name中包含'码'字
```

[UNIQUE](https://so.csdn.net/so/search?q=UNIQUE&spm=1001.2101.3001.7020) 索引
---------------------------------------------------------------------------

一旦某一列被设置了 `UNIQUE` 索引

*   所有值必须是唯一的
*   允许存在多个 NULL 值

2 种常见写法：

```
列名 数据类型 UNIQUE [KEY]
```

```
UNIQUE [KEY] (列名)
```

使用示例：

```
CREATE TABLE student {
	id INT UNIQUE, # 写法1
	name VARCHAR(20),
	UNIQUE(name) # 写法2
}
```

主键 (PRIMARY KEY)
----------------

主键的作用：可以保证在一张表中的每一条记录都是唯一的

*   如果将某一列设置为主键，那么这一列相当于加上了 `NOT NULL UNIQUE`
*   建议每一张表都有主键
*   主键最好跟业务无关，尝设置为 **INT** **AUTO_INCREMENT**

2 种常见写法：

```
列名 数据类型 PRIMARY KEY
```

```
PRIMARY KEY (列名)
```

使用示例：

```
CREATE TABLE company (
	id INT AUTO_INCREMENT PRIMARY KEY, # 写法1
	name VARCHAR(20) NOT NULL UNIQUE
);
```

```
CREATE TABLE company (
	id INT AUTO_INCREMENT,
	name VARCHAR(20) NOT NULL UNIQUE,
	PRIMARY KEY(id) # 写法2
);
```

外键 (FOREIGN KEY)
----------------

一般用外键来引用其他表的主键

常见写法：

```
FOREIGN KEY (列名) REFERENCES 表名(列名)
```

示例代码：

```
CREATE TABLE company(
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(20) NOT NULL UNIQUE
);
```

```
CREATE TABLE customer(
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(20) NOT NULL,
	company_id INT NOT NULL,
	FOREIGN KEY (company_id) REFERENCES company(id)
);
```

级联 (CASCADE)
------------

定义外键时，可以设置级联

```
ON DELETE CASCADE
```

*   当**删除****被引用**的记录时，引用了此记录的其他所有记录都会被自动删除

```
ON UPDATE CASCADE
```

*   当**修改****被引用**的记录时，引用了此记录的其他所有记录都会被自动更新