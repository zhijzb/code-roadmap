> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [blog.csdn.net](https://blog.csdn.net/weixin_43734095/article/details/119300337)

#### [MyBatis](https://so.csdn.net/so/search?q=MyBatis&spm=1001.2101.3001.7020) - 增删改、动态 SQL

*   [动态 SQL](#_SQL_5)
*   *   [if 标签](#if__10)
    *   [where 标签](#where__25)
    *   [sql 标签](#sql__42)
    *   [foreach 标签](#foreach__58)
*   [添加](#_61)
*   *   [主键设置](#_75)
    *   [批量添加 - 利用 foreach 标签](#___foreach__96)
*   [更新](#_125)
*   [删除](#_137)
*   *   [批量删除 - 利用 foreach 标签](#___foreach__146)
*   [typeAliases 标签](#typeAliases__182)

> Java 从 0 到[架构](https://so.csdn.net/so/search?q=%E6%9E%B6%E6%9E%84&spm=1001.2101.3001.7020)师目录：[【Java 从 0 到架构师】学习记录](https://blog.csdn.net/weixin_43734095/article/details/119067034)

动态 SQL
------

动态 SQL 参考文档：[https://mybatis.org/mybatis-3/zh/dynamic-sql.html](https://mybatis.org/mybatis-3/zh/dynamic-sql.html)

以下标签都是在 mapper 中使用，实现动态 SQL

### if <mark style="background: #FF5582A6;">标签</mark>

```java
<select id="dynamicSQL" parameterType="Map" resultType="com.mj.bean.Skill">
    SELECT * FROM contact WHERE 1 = 1
    <if test="keyword != null">
        AND name LIKE #{keyword}
    </if>
    <if test="beginDay != null">
        AND created_time > #{beginDay}
    </if>
    <if test="endDay != null">
        AND created_time < #{endDay}
    </if>
</select>
```

### where 标签

```
<select id="dynamicSQL" parameterType="Map" resultType="com.mj.bean.Skill">
    SELECT * FROM contact
    <where>
	    <if test="keyword != null">
	        AND name LIKE #{keyword}
	    </if>
	    <if test="beginDay != null">
	        AND created_time > #{beginDay}
	    </if>
	    <if test="endDay != null">
	        AND created_time < #{endDay}
	    </if>
    </where>
</select>
```

### sql 标签

sql 标签用于抽取公共的 sql 语句

```
<sql id="sqlListAll">
	SELECT * FROM skill
</sql>

<select id="list" resultType="com.mj.bean.Skill">
	<include refid="sqlListAll">
</select>

<select id="get" parameterType="int" resultType="com.mj.bean.Skill">
	<include refid="sqlListAll" /> WHERE id = #{id}
</select>
```

### [foreach](https://so.csdn.net/so/search?q=foreach&spm=1001.2101.3001.7020) 标签

可见下面的批量添加、批量删除

添加
--

```
<insert id="insert" parameterType="com.mj.bean.Skill">
    INSERT INTO skill(name, level) VALUES(#{name}, #{level})
</insert>
```

注意：openSession 的参数默认值是 false，不自动提交事务

```
Skill skill = new Skill();
skill.setLevel(3);
skill.setName("打架");
session.insert("skill.insert", skill);
session.commit();
```

### [主键](https://so.csdn.net/so/search?q=%E4%B8%BB%E9%94%AE&spm=1001.2101.3001.7020)设置

设置新插入记录的主键 (id) 到参数对象中：

```
<insert id="insert" parameterType="com.mj.bean.Skill">
    INSERT INTO skill(name, level) VALUES (#{name}, #{level})
    <selectKey resultType="int" keyProperty="id" order="AFTER">
        SELECT LAST_INSERT_ID()
    </selectKey>
</insert>
```

还有一种方法：使用 useGeneratedKeys、keyProperty 属性

*   **需要数据库驱动支持**，比如可以适用于 MySQL，不适用于 Oracle

```
<insert id="insert" 
	useGeneratedKeys="true"
	keyProperty ="id"
	parameterType="com.mj.bean.Skill">
    INSERT INTO skill(name, level) VALUES (#{name}, #{level})
</insert>
```

### 批量添加 - 利用 foreach 标签

```java
<insert id="batchInsert"
        useGeneratedKeys="true"
        keyProperty="id"
        parameterType="List">
    INSERT INTO skill(name, level) VALUES
    <foreach collection="list" item="skill" separator=",">
        (#{skill.name}, #{skill.level})
    </foreach>
</insert>
```

```java
List<Skill> skills = new ArrayList<>();
skills.add(new Skill("Java1", 111));
skills.add(new Skill("Java2", 222));
session.insert("skill.batchInsert", skills);
```

批量添加的效率比【多次单个添加】要高，但是它无法使用 `<selectedKey>` 获取新插入记录的主键

*   可以使用 useGeneratedKeys 获取主键

批量操作生成的 SQL 语句可能会比较长，有可能会超过数据库的限制

如果传进来的**参数是 List**，collection 属性值为 list 就可以遍历这个 List

如果传进来的**参数是数组**，collection 属性值为 array 就可以遍历这个数组

更新
--

```
<update id="update" parameterType="com.mj.bean.Skill">
    UPDATE skill SET name = #{name}, level = #{level} WHERE id = #{id}
</update>
```

```
Skill skill = new Skill("Java", 666);
skill.setId(21);
session.update("skill.update", skill);
```

删除
--

```
<delete id="delete" parameterType="int">
	DELETE FROM skill WHERE id = #{id}
</delete>
```

```
session.delete("skill.delete", 10;
```

### 批量删除 - 利用 foreach 标签

利用 foreach 和 `collection=":list"` 批量删除：Java 代码中传入 List

```java
<delete id="batchDelete" parameterType="List">
    DELETE FROM skill WHERE id IN (
    <foreach collection="list" item="id" separator=",">
        #{id}
    </foreach>
    )
</delete>
```

```
List<Integer> ids = new ArrayList<>();
ids.add(23);
ids.add(24);
session.insert("skill.batchDelete", ids);
```

利用 foreach 和 `collection="array"` 批量删除：Java 代码中传入数组

```
<delete id="batchDelete" parameterType="List">
    DELETE FROM skill WHERE id IN
    <foreach collection="array"
             item="id"
             open="("
             close=")"
             separator=",">
        #{id}
    </foreach>
</delete>
```

```
Integer[] ids = {23, 24, 25, 26};
session.insert("skill.batchDelete", ids);
```

typeAliases 标签
--------------

添加到 mybatis-config.xml 的 configuration 标签中

*   用于设置类型的别名（不区分大小写）

```
<!-- 别名 -->
<typeAliases>
    <!-- 一旦设置了别名，它是不区分大小写的 -->
    <typeAlias type="com.mj.bean.Skill" alias="skill" />
    <typeAlias type="com.mj.common.DruidDataSourceFactory" alias="druid" />
</typeAliases>
```

```
<!-- 别名 -->
<typeAliases>
    <!-- 这个包下的所有类, 都会起一个别名: 全类名的最后一个单词 -->
    <package  />
</typeAliases>
```

别名相关文档：[https://mybatis.org/mybatis-3/zh/configuration.html#typeAliases](https://mybatis.org/mybatis-3/zh/configuration.html#typeAliases)

*   mybatis 中自带了很多别名，例如：string、int、integer 等  
    因此实际上使用这些类型时是不区分大小写的，建议按标准写  
    ![](https://img-blog.csdnimg.cn/6a681a9a39bf4ea4987e185c284255c2.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)