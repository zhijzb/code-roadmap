
> Hutool 作为后起之秀，功能上也比较全。但是要注意一点，它的开源协议是：中国第一个开源协议[木兰宽松许可证, 第 1 版在新窗口打开](http://license.coscl.org.cn/MulanPSL)，对此在商业项目中需要谨慎些，在个人项目无所谓。@pdai

简介
-----------

Hutool 是一个小而全的 Java 工具类库，通过静态方法封装，降低相关 API 的学习成本，提高工作效率，使 Java 拥有函数式语言般的优雅，让 Java 语言也可以 “甜甜的”。

Hutool 中的工具方法来自于每个用户的精雕细琢，它涵盖了 Java 开发底层代码中的方方面面，它既是大型项目开发中解决小问题的利器，也是小型项目中的效率担当；

Hutool 是项目中 “util” 包友好的替代，它节省了开发人员对项目中公用类和公用工具方法的封装时间，使开发专注于业务，同时可以最大限度的避免封装不完善带来的 bug。

### Hutool 名称的由来

Hutool = Hu + tool，是原公司项目底层代码剥离后的开源库，“Hu” 是公司名称的表示，tool 表示工具。Hutool 谐音 “糊涂”，一方面简洁易懂，一方面寓意 “难得糊涂”。

### Hutool 如何改变我们的 coding 方式

Hutool 的目标是使用一个工具方法代替一段复杂代码，从而最大限度的避免 “复制粘贴” 代码的问题，彻底改变我们写代码的方式。

以计算 MD5 为例：

*   【以前】打开搜索引擎 -> 搜 “Java MD5 加密” -> 打开某篇博客 -> 复制粘贴 -> 改改好用
*   【现在】引入 Hutool -> SecureUtil.md5()

Hutool 的存在就是为了减少代码搜索成本，避免网络上参差不齐的代码出现导致的 bug。

包含组件
---------------

一个 Java 基础工具类，对文件、流、加密解密、转码、正则、线程、XML 等 JDK 方法进行封装，组成各种 Util 工具类，同时提供以下组件：

<table><thead><tr><th>模块</th><th>介绍</th></tr></thead><tbody><tr><td>hutool-aop</td><td>JDK 动态代理封装，提供非 IOC 下的切面支持</td></tr><tr><td>hutool-bloomFilter</td><td>布隆过滤，提供一些 Hash 算法的布隆过滤</td></tr><tr><td>hutool-cache</td><td>简单缓存实现</td></tr><tr><td>hutool-core</td><td>核心，包括 Bean 操作、日期、各种 Util 等</td></tr><tr><td>hutool-cron</td><td>定时任务模块，提供类 Crontab 表达式的定时任务</td></tr><tr><td>hutool-crypto</td><td>加密解密模块，提供对称、非对称和摘要算法封装</td></tr><tr><td>hutool-db</td><td>JDBC 封装后的数据操作，基于 ActiveRecord 思想</td></tr><tr><td>hutool-dfa</td><td>基于 DFA 模型的多关键字查找</td></tr><tr><td>hutool-extra</td><td>扩展模块，对第三方封装（模板引擎、邮件、Servlet、二维码、Emoji、FTP、分词等）</td></tr><tr><td>hutool-http</td><td>基于 HttpUrlConnection 的 Http 客户端封装</td></tr><tr><td>hutool-log</td><td>自动识别日志实现的日志门面</td></tr><tr><td>hutool-script</td><td>脚本执行封装，例如 Javascript</td></tr><tr><td>hutool-setting</td><td>功能更强大的 Setting 配置文件和 Properties 封装</td></tr><tr><td>hutool-system</td><td>系统参数调用封装（JVM 信息等）</td></tr><tr><td>hutool-json</td><td>JSON 实现</td></tr><tr><td>hutool-captcha</td><td>图片验证码实现</td></tr><tr><td>hutool-poi</td><td>针对 POI 中 Excel 的封装</td></tr><tr><td>hutool-socket</td><td>基于 Java 的 NIO 和 AIO 的 Socket 封装</td></tr></tbody></table>

可以根据需求对每个模块单独引入，也可以通过引入`hutool-all`方式引入所有模块。

文档
-----------

*   [中文文档在新窗口打开](https://www.hutool.cn/docs/) 看这里
*   [中文文档（备用）在新窗口打开](https://www.hutool.club/docs/)
*   [参考 API 在新窗口打开](https://apidoc.gitee.com/loolly/hutool/)

安装
-----------

### Maven

在项目的 pom.xml 的 dependencies 中加入以下内容:

```xml
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-all</artifactId>
    <version>5.1.0</version>
</dependency>
```

### Gradle

```
compile 'cn.hutool:hutool-all:5.1.0'
```

### 非 Maven 项目

点击以下任一链接，下载`hutool-all-X.X.X.jar`即可：

*   [Maven 中央库 1](https://repo1.maven.org/maven2/cn/hutool/hutool-all/5.1.0/)
*   [Maven 中央库 2](http://repo2.maven.org/maven2/cn/hutool/hutool-all/5.1.0/)

> 注意 Hutool 5.x 支持 JDK8+，对 Android 平台没有测试，不能保证所有工具类获工具方法可用。 如果你的项目使用 JDK7，请使用 Hutool 4.x 版本

### 编译安装

访问 Hutool 的码云主页：[https://gitee.com/loolly/hutool](https://gitee.com/loolly/hutool)下载整个项目源码（v5-master 或 v5-dev 分支都可）然后进入 Hutool 项目目录执行：

然后就可以使用 Maven 引入了。

