

# 项目实战



> 项目 Gitee 地址：[https://gitee.com/szluyu99/xmg-resume](https://gitee.com/szluyu99/xmg-resume)

功能性
---

### 登陆流程

![](http://121.41.87.138:9001/picgo/img/7c8911a81be3497e99d561ebf091942a.png)  
JS 的 MD5 库：[https://blueimp.github.io/JavaScript-MD5/](https://blueimp.github.io/JavaScript-MD5/)

MD5 在线生成：[https://www.cmd5.com/](https://www.cmd5.com/)

### 验证码

验证码 (CAPTCHA)，可以用于防止大规模注册、暴力破解密码、刷票、论坛灌水等

> *   Completely Automated Public Turing test to tell Computers and Humans Apart 的缩写
> *   全自动区分计算机和人类的图灵测试

传统的验证码：由扭曲倾斜的文字、干扰线组成

*   由服务器端生成验证码图片，返回给客户端展示  
    ![](http://121.41.87.138:9001/picgo/img/76b17795b9854c63ad54ebfc178ccb40.png)

在 Java 中，可以使用 [Kaptcha](https://mvnrepository.com/artifact/com.google.code/kaptcha/2.3.0) 库生成验证码

> Kaptcha 产自 Google

常用的配置有：字体、内容的范围、尺寸、边框、干扰线、样式等

```xml
<dependency>
	<groupId>com.github.penggle</groupId>
	<artificatId>kaptcha</artificatId>
	<version>2.3.2</version>
</dependency>
```

JavaWeb
-------

### Service、Dao 方法名规范

*   获取单个对象的方法用 **get** 做前缀
*   获取多个对象的方法用 **list** 做前缀
*   获取统计值的方法用 count 做前缀
*   插入的方法用 **save**（推荐）或 **insert** 做前缀
*   删除的方法用 **remove**（推荐）或 **delete** 做前缀
*   修改的方法用 **update** 做前缀

### form 文件上传

知识点:

- 基于Apache的[commons-fileupload](http://commons.apache.org/proper/commons-fileupload/)组件完成文件的上传操作
- 文件上传做控制
    1)文件名处理
    2)上传文件的类型约束
    3)上传文件的大小限制

#### 前端

form 如果要支持文件上传，必须设置 2 个属性：

*   `method="POST"`
*   `enctype="multipart/form-data"`

```html
<-->示例</-->
<form method="POST" enctype="multipart/form-data" action="fup.cgi">
  File to upload: <input type="file" name="upfile"><br/>
  Notes about the file: <input type="text" name="note"><br/>
  <br/>
  <input type="submit" value="Press"> to upload the file!
</form>
```

> 即带文件数据的Post请求

使用[Firefox 火狐浏览器 - 全新、安全、快速 | 官方最新下载](https://www.firefox.com.cn/)查看更多信息

![image-20240129142643332](http://121.41.87.138:9001/picgo/img/image-20240129142643332.png)

> 其中
>
> 分隔线分隔参数，便于获取各部分的值

#### 后端

Java 后台中常用 [commons-fileupload](http://commons.apache.org/proper/commons-fileupload/) 来接收客户端上传的文件

*   解析 request 为 `List<FilteItem>`

```java
ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
// 一个FileItem就代表一个请求参数（包括文件参数、非文件数据）
List<FileItem> items = upload.parseRequest(request);
// 遍历文件参数
for (FilteItem item : items) {
	String filedName = item.getFiledName(); // 共有参数
	if (item.isFormField()) { // 非文件参数

	} else { // 文件参数

	}
}
```

> 说明
>
> DiskFileItemFactory 磁盘文件工厂

*   解决中文文件名乱码问题

```java
upload.setHeaderEncoding("UTF-8");
```

- 使用`输入流读取`客户端发送的文件数据，使用`输出流写入`服务器硬盘

![image-20240129154855693](http://121.41.87.138:9001/picgo/img/image-20240129154855693.png)

#### 实时预览

刚上传完毕的图片，有可能会出现无法实时预览的问题（要等一会才能预览成功）
![](http://121.41.87.138:9001/picgo/img/7ada602f0677459cbfd4c49837a26924.png)  
想要实现实时预览，把 Tomcat 的缓存资源功能关掉即可，在 `%TOMCAT_HOME%/conf/context.xml` 中增加 `Resources` 标签：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Context>
	<WatchedResource>WEB-INF/web.xml</WatchedResource>
	<WatchedResource>WEB-INF/tomcat-web.xml</WatchedResource>
	<WatchedResource>${catalina.base}/conf/web.xml</WatchedResource>
	<Resources cachingAllowed="false" cacheMaxSize="0" />
</Context>
```



> upload 封装为方法

### 页面可见性

正常不应该可以直接访问 `xx.jsp` 页面，因为这是静态页面，必须通过 Servlet 转发访问才能访问到有数据的页面。

为了防止 `.jsp` 文件被访问到，可以将其放置到 `WEB-INF` 文件夹下 
![](https://img-blog.csdnimg.cn/dc91058b16004dfe903ecaeade4541a5.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_1,color_FFFFFF,t_70)  
网页中的静态资源（.css、img 等）一般是可以直接访问到的，因此可以不用放到 WEB-INF 中

### Service 层

*   **Servlet**：控制器层
*   **Service**：业务层，用来完成具体的业务
*   **Dao**：数据访问层

Service 层存在的原因是：有时候某个业务可能会多次调用数据库相关的操作，在项目很简单的情况下（Service 层所做的事情就是直接调用 Dao 层），Service 存在的意义不明显，但是当项目变大以后，Service 所处理的一些业务操作就需要抽取出来比较清晰。

### 面向接口编程

为什么需要面向接口编程？

*   接口只是定义了一种规范，比如 Dao 接口规定了访问数据库有哪些操作，具体具体如何实现这些操作是不固定的，例如使用 Hibernate、Mybatis、Spring-JDBC 等，如果没有接口的约束，当切换了数据库的实现方案后需要改变大量的代码；但是如果有接口的约束，只需要去实现各个方案对数据库的具体操作即可。

后端技巧
----

### 利用 Jackson 将 Java 对象转成 Json 字符串

```
Person person = new Person(1, "张三", 12);
ObjectMapper mapper = new ObjectMapper();
String jsonString = mapper.writeValueAsString(person);
// {"id":"1", "name":"张三", "age":"12"}
```

### 利用反射获取泛型的类型

使用场景是**自动生成表名**，根据泛型的类型获取类名，然后转为数据库的表名

```
public class Student extends Person<String, Integer>
implements Test1<Integer, Double>, Test2<Double, Long, StringBuilder> {

    // 获取泛型的类型
    public void printGenericType() {
        // 父类
        ParameterizedType superClassType  = (ParameterizedType) getClass().getGenericSuperclass();
        Type[] args = superClassType.getActualTypeArguments();
        for (Type arg : args) {
            System.out.println(arg);
        }

        System.out.println("-----------------------------");

        // 接口
        Type[] interfaceTypes = (Type[]) getClass().getGenericInterfaces();
        for (Type type : interfaceTypes) {
            ParameterizedType interfaceType = (ParameterizedType) type;
            Type[] interfaceArgs = interfaceType.getActualTypeArguments();
            for (Type arg : interfaceArgs) {
                System.out.println(arg);
            }
        }
        
    }
}
```

```
java.lang.String
java.lang.Integer
-----------------------------
java.lang.Integer
java.lang.Double
java.lang.Double
java.lang.Long
java.lang.StringBuilder
```

**大驼峰 (MyAge)**、**小驼峰 (myAge)** 转为 **下划线 (my_age)**：

```
/**
 * 将大驼峰(MyAge)、小驼峰(myAge) 转为 下划线(my_age)
 **/
public static String underlineCased(String str) {
    if (str == null) return null;
    int len = str.length();
    if (len == 0) return str;

    StringBuilder sb = new StringBuilder();
    sb.append(Character.toLowerCase(str.charAt(0)));
    for (int i = 1; i < len; i++) {
        char c = str.charAt(i);

        if (Character.isUpperCase(c)) {
            sb.append("_");
            sb.append(Character.toLowerCase(c));
        } else {
            sb.append(c);
        }
    }
    return sb.toString();
}
```

前端技巧
----

### 利用 [reset](https://so.csdn.net/so/search?q=reset&spm=1001.2101.3001.7020) 清空 form 表单

jQuery 获取的并不是原生的 DOM 对象，通过 jQuery 拿到表单无法调用 reset，需要转成原生 DOM：

```
document.querySelector('#add-form-box form').reset() // 原生的表单DOM对象有reset方法
```

```
// $('#add-form-box form').reset() // 报错,jQuery获取的不是原生DOM,没有reset方法
$('#add-form-box form')[0].reset()
```

注意：调用 reset 方法不会重置 `type="hidden"` 的 input 输入框，需要使用 js 单独处理

### 图片的 MIMEType

选择文件的 input 可以通过 accept 来控制接收的文件类型，`image/*` 即所有图片类型

```
<input type="file"  />
```

### 验证码功能发送不同的参数防止缓存

点击验证码应当会刷新图片，但是如果每次请求的地址是相同的，那么浏览器会从缓存中取而不会实现刷新，通过在后面拼接一些不同的参数使得每次发送的请求不同。

```
$("#captcha").click(function() {
	$(this).attr('src', '${ctx}/user/catcha?time=' + new Date().getTime())
})
```

### 登陆 / 修改密码功能，利用隐藏域发送加密后的密码

一般不会给输入的新密码的 input 添加 name 使得用户输入的密码直接发送到服务器，而是在前端对用户输入的密码进行一个加密，将加密后的值设置到隐藏域`<input type="hidden"` ，通过隐藏域发送加密后的密码。

```
<!-- 隐藏域用于发送加密后的密码 -->
<input type="hidden"  />
<!-- 用户输入的ipnut不设置name则不会发送 -->
请输入密码：<input type="password" id='password' />
```

```
$('[name=password]').val(md5($('#password').val()))
```

> 有些网址在输入完密码点击登陆后，密码栏的字符会突然变多，实际上这就是进行了加密再发送，采用上面的方法可以避免点击登陆后密码栏字符变多的情况