> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [zhuanlan.zhihu.com](https://zhuanlan.zhihu.com/p/362849510)

一、获取取镜像
-------

执行命令：

```shell
docker pull registry.cn-hangzhou.aliyuncs.com/helowin/oracle_11g
```

下载的过程少长, 镜像约为 6.8G

下载完成后 查看镜像： 

```shell
docker images
```



![](https://pic3.zhimg.com/v2-eab9595e655a8e5494a7d550b17206ca_r.jpg)

二、镜像重新命名
--------

```
[root@whdata3 ~]# docker tag 3fa112fd3642 oracle:11g
```

![](https://pic3.zhimg.com/v2-41a5bcc6ac2547e8d364c51f59227eba_r.jpg)![](https://pic4.zhimg.com/v2-59a47c294a641c9e13934f5596328fff_r.jpg)

三、创建容器
------

```
[root@whdata3 ~]# docker run -d -p 1521:1521 --name oracle11g 3fa112fd3642
```

![](https://pic2.zhimg.com/v2-0b95334eb1e468219fd537a9d9697c6d_r.jpg)

四、登录容器并连接 oracle 数据库
--------------------

进入容器

```
[root@whdata3 ~]# docker exec -it oracle11g /bin/bash
```

连接数据库

```
sqlplus /nolog
```

![](https://pic3.zhimg.com/v2-da49612486c0a0d6f11c2d3fcfcec0fe_r.jpg)

切换到 root 用户下

su - root

密码：helowin

![](https://pic3.zhimg.com/v2-2689c9325bf4833eac1e7ed5e59855d2_r.jpg)

配置环境变量

编辑 profile 文件配置 ORACLE 环境变量

```
[root@a4092c8e64c5 ~]# vi /etc/profile

export ORACLE_HOME=/home/oracle/app/oracle/product/11.2.0/dbhome_2
export ORACLE_SID=helowin
export PATH=$ORACLE_HOME/bin:$PATH
```

环境变量生效

```
[root@a4092c8e64c5 ~]# source /etc/profile
```

创建软连接

```
[root@a4092c8e64c5 ~]# ln -s $ORACLE_HOME/bin/sqlplus /usr/bin
```

切换到 oracle 用户

注意：一定要加上中间的内条 "-" 必须要, 否则软连接无效.

```
[root@a4092c8e64c5 ~]# su - oracle
```

![](https://pic4.zhimg.com/v2-e943c9da9871b282cf874e3c1ed02477_r.jpg)

五、登录 sqlplus 并修改 sys、system 用户密码
--------------------------------

```
sqlplus /nolog
```

![](https://pic1.zhimg.com/v2-2b1d2cbbe99a6734d141ccdfc4f42fac_r.jpg)

```
conn /as sysdba
```

![](https://pic1.zhimg.com/v2-3377365f0a3a31abc17c32dd86e69fe4_r.jpg)

执行下面命令

```
alter user system identified by system;
        alter user sys identified by sys;
```

![](https://pic1.zhimg.com/v2-59eab1c885e41519d8fc36d61636dd7c_r.jpg)

也可以创建用户

```
create user zhaosj identified by zhaosj;
```

![](https://pic3.zhimg.com/v2-1b9cff741a93dc73379ae767469334fa_r.jpg)

并给用户赋予权限

```
grant connect,resource,dba to test;
```

![](https://pic4.zhimg.com/v2-b93ca530dc5d643fa40c08aaf3b5dd47_r.jpg)

六、修改数据密令报错解决
------------

当执行修改密码的时候出现 ： database not open

提示数据库没有打开, 按如下操作:

```
输入：alter database open;
```

**注意了：这里也许还会提示 ： ORA-01507: database not mounted**

![](https://pic3.zhimg.com/v2-e53277f11d5714567d9ebf1d990f6aca_r.jpg)

**=========== 解决方法 ===========**

```
输入：alter database mount;
 输入 ：alter database open;
```

![](https://pic2.zhimg.com/v2-6edc9a5f31e306544ff2246676d7d899_b.jpg)

然后就可执行修改数据库密码的命令了

```
改完之后输入：

ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;
```

刷新下表

exit 是退休 sql 软连接

![](https://pic4.zhimg.com/v2-166779a4fc780e5f68cade9e8eab1deb_r.jpg)

七、安装 PLSQL Developer 12
-----------------------

**PLSQL Developer 12** 是一款非常专业的 PL/SQL 数据库管理工具，拥有语法加强、SQL 和 PL/SQL 帮助、对象描述、代码助手、编译器提示、PL/SQL 完善、代码内容、代码分级、浏览器按钮、超链接导航、宏库等强大的数据库开发功能，可以为开发人员提供一个良好的 Oracle 数据库开发存储程序单元的开发环境。非常适合数据库开发人员使用。

双击 “plsqldev1207x64.msi” 安装软件，点击 next

![](https://pic4.zhimg.com/v2-5216549b8458e670c249079804a43ecf_r.jpg)

许可协议界面，选择 I accept 接受协议。

![](https://pic1.zhimg.com/v2-22ed01973e84ca41885f6313aeee5168_r.jpg)

设置软件安装路径，注册选项中，我们选择第二项，填入注册信息。

![](https://pic3.zhimg.com/v2-2d2501a88cba10023200c45c4d43ce76_r.jpg)

在注册框中分别填入下载的信息

```
product code： 4vkjwhfeh3ufnqnmpr9brvcuyujrx3n3le
serial Number：226959
password： xs374ca
```

![](https://pic2.zhimg.com/v2-e2a022d63bfb7878a4054629f89221b5_r.jpg)

选择安装类型，选择 complete 完整安装。

![](https://pic4.zhimg.com/v2-b6259b95ad59b5242cff873f2b20ad73_r.jpg)

点击 install 安装软件。

![](https://pic4.zhimg.com/v2-636bef3c8074359f7d673b5de273d99b_r.jpg)![](https://pic2.zhimg.com/v2-89bea2ea2e75517a9682b7b5ff2803e9_r.jpg)

汉化教程
----

打开软件包，运行 “chinese.exe” 安装汉化补丁。

![](https://pic2.zhimg.com/v2-2d897957dbe875361af3d79668235279_r.jpg)![](https://pic3.zhimg.com/v2-c3ac9cf5c59ab939e7807a29d1292aee_r.jpg)![](https://pic3.zhimg.com/v2-121e7756ee095fe04a83fb71399dddfa_r.jpg)![](https://pic4.zhimg.com/v2-78c4cfc4783708ffc3821cef131951ab_r.jpg)![](https://pic3.zhimg.com/v2-1037bd7d04344d20756d7914da369506_r.jpg)![](https://pic1.zhimg.com/v2-29cca8c46d4d85e54325e104ef63be84_r.jpg)

打开 pl/sql 进行登录 ：提示监听程序当前无法识别连接描述符中请求的服务

![](https://pic2.zhimg.com/v2-eb77bcbcf8980e50fcc52c79c5e4713d_r.jpg)![](https://pic2.zhimg.com/v2-eecf145be46d9ffb93056049103a97f5_r.jpg)

这时需要去看一下 oracle 的 lsnrctl 服务

```
[oracle@a4092c8e64c5 ~]$ lsnrctl status
```

![](https://pic1.zhimg.com/v2-38f1f949d7180902b2c37763c19092f8_r.jpg)

【"helowin"】【"helowinXDB"】任选其一，修改 **tnsnames.ora 的 service_name=helowinXDB**

**E:\PLSQL Developer 12\instantclient_12_2\NETWORK\ADMIN**

![](https://pic3.zhimg.com/v2-9cb63f957d0bab091c38edb7b340138e_r.jpg)![](https://pic2.zhimg.com/v2-f8d4c685de34036331821ef82bc02125_r.jpg)

```
docker_oracle11 =
 (DESCRIPTION =
   (ADDRESS_LIST =
     (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.1.73)(PORT =1521))
   )
   (CONNECT_DATA =
     (SERVICE_NAME = helowinXDB)
   )
)
```

数据安全管控平台添加 ORACLE 数据资源

![](https://pic2.zhimg.com/v2-29890a21977be3eb82a8ea3eb8f5521d_r.jpg)![](https://pic2.zhimg.com/v2-9249a01e5c55ab63a5f47562d375c9b1_r.jpg)

SID：helowin