> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [blog.csdn.net](https://blog.csdn.net/weixin_43734095/article/details/119859020)

#### 项目实战 - 权限管理


驾考系统 [Gitee](https://so.csdn.net/so/search?q=Gitee&spm=1001.2101.3001.7020) 代码（前端 + 后端）：[https://gitee.com/szluyu99/jiakao](https://gitee.com/szluyu99/jiakao)

会话管理


### 客户端身份认证 - 基于 Cookie、Session

执行流程

1.  **客户端**：发送用户名密码
2.  **服务器**：验证成功后，创建并保留一个跟客户端相关联的 Session，`返回 Session_ID` 给客户端
3.  **客户端**：将 Session_ID 保存到 Cookie 中
4.  **客户端**：后续的请求都带上包含了 Session_ID 的 Cookie
5.  **服务器**：通过验证 Cookie 中的 Session_ID 确认用户身份

> ![](../../03-%E5%9B%BE%E7%89%87/typora/04e7cbe6d3de4aeba0cc7465afecf575.png)

优点：服务器、客户端基本自动完成一系列的流程，不用编写太多额外的代码

缺点：

*   默认不支持分布式架构，在分布式架构下，需要解决分布式 Session 共享的问题
    
    > 比如存放到 Redis 中
    
*   默认不支持非浏览器环境（没有 Cookie 机制的环境）

简单的分布式架构：  
![](../../03-%E5%9B%BE%E7%89%87/typora/96aa8a7f30064ec08f483ff97f348453.png)

### 客户端身份验证 - 基于 token

执行流程：

1.  **客户端**：发送用户名密码
2.  **服务器**：验证成功后，生成一个跟客户端相关联的 token，返回 token 给客户端
3.  **客户端**：存储 token 到本地
4.  **客户端**：后续的请求都带上 token
5.  **服务器**：通过验证 token 确认用户身份  
    ![](https://img-blog.csdnimg.cn/71fcdbdc08f648aa9b92eaed303eeece.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

> 分布式环境中使用：  
> ![](https://img-blog.csdnimg.cn/d1c8d5b0fa0c462aa841ae2230c409ce.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

优点：支持分布式架构，支持非浏览器环境（没有 Cookie 机制的环境）

缺点：

*   需要客户端手动存储，发送 token
*   有些 token 会比 session_id 大，需要消耗更多流量
*   有些 token 方案，默认无法在服务器主动销毁 token

> 为了保证 token 的安全性，有的公司会采取 **双 token** 方案：
> 
> *   accssToken 用来访问；（有效期尽量短一点）
> *   refreshToken 用来刷新 accseeToken；（有效期可以稍微长一点）![](https://img-blog.csdnimg.cn/e7b4da8c8ea64d0f9ccbc8e232768675.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

EhCache - 简单的缓存框架
-----------------

[EhCache](https://www.ehcache.org/) 是一款简单易用的缓存框架

*   支持 3 层缓存：**Heap（JVM 的堆内存）**、**Off-Heap（堆外内存）**、**Disk（磁盘）**

> Off-Heap、Disk 要求对象支持序列化和反序列化

```
<dependency>
    <groupId>org.ehcache</groupId>
    <artifactId>ehcache</artifactId>
</dependency>
```

使用示例：

*   **配置文件**：[示例代码](https://gitee.com/szluyu99/jiakao/blob/master/JiaKao/src/main/resources/ehcache.xml)
*   **工具类**：[示例代码](https://gitee.com/szluyu99/jiakao/blob/master/JiaKao/src/main/java/com/mj/jk/common/cache/Caches.java)

JWT - 基于 JSON 的 token 标准
------------------------

[JWT](https://jwt.io/)：一种基于 JSON 的 token 标准（[RFC 7519](https://tools.ietf.org/html/rfc7519)）

JWT 最终是一个字符串，由 3 部分组成：header**.**payload**.**signature

> 使用点 **.** 进行拼接

![](https://img-blog.csdnimg.cn/2e6f05810d79492fb6e1b6fba7a0b3f1.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)  
通常会将 JWT 放到请求头 Authorization 中

### header

通常由 2 部分组成：

*   令牌的类型（即 JWT）
*   所使用的签名算法（比如 HMAC、SHA256、RSA）

比如：

*   {“alg”: “HS256”, “typ”: “JWT”}
*   这个 JSON 被 [Base64Url](https://thrysoee.dk/base64url/) 编码后，作为 JWT 的第 1 部分：  
    eyJhbGcioiJIUzI1NiIsInR5cCI6IkpXVCJ9
*   Base64Url 是可逆的，可编码解码的

### payload

一般是用户相关的一些数据（比如用户 id、用户名、角色信息等）

比如：

*   {“sub”: “1234567890”, “name”: “John Doe”, “admin”: “true” }
*   这个 JSON 被 [Base64Url](https://thrysoee.dk/base64url/) 编码后，作为 JWT 的第 2 部分  
    eyJzdWli0ilxMjMONTY30DkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9
*   由于 Base64Url 是可逆的，所以 payload 中不要存放敏感信息（比如密码）

### signature

签名（假设使用的是 HMAC SHA256 算法）

```
HMACSHA256(
	base64UrlEncode(header) + "." + base64UrlEncode(payload),
	secret
)
```

*   secret 是私钥，不能公开
*   签名的目的是防篡改、防伪造

权限管理 - RBAC
-----------

权限管理：对已经**认证成功**的用户进行**授权**

*   **认证成功**：比如使用用户名、密码登录成功
*   **授权**：比如赋值相应的操作权限（资源）

最简单粗暴的实现方案：用户直接跟资源（权限）挂钩，设计成 3 张表

*   用户表 (sys_user)
*   资源表 (sys_resource)
*   用户资源表 (sys_user_resource)

以上方法在每次新加入用户时，都需要在用户资源表中维护大量数据，不推荐。

```
sys_user
- id	name
  1		张三
  2		李四

sys_resource
- id	name
  1		查询权限
  2		添加权限
  3		删除权限

sys_user_resource
- user_id	resource_id
  1			1
  1			2
  2			1
  2			2
  2			3

# 每次添加user,都需要在sys_user_resource中添加大量数据
```

建议采用：**RBAC (Role-based Access Control)**，以角色为基础的访问控制，用户跟角色挂钩，角色和资源挂钩，设计成 5 张表：

*   用户表 (sys_user)
*   角色表 (sys_role)
*   资源 (sys_resource)
*   用户角色表 (sys_user_role)
*   角色资源表 (sys_role_resource)

> 前缀添加 sys 是表示这些表是系统相关的表，即是控制后台人员的权限，而非是客户端客户的权限，因为客户根本无法进入后台，因此无需进行权限控制。

![](https://img-blog.csdnimg.cn/35b3fdea77a9453988dbd71b1e240852.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzczNDA5NQ==,size_16,color_FFFFFF,t_70)

```
sys_user
- id	name
  1		张三
  2		李四
  3		王五

sys_role
- id 	name
  1		总经理
  2		副经理
  3		前台

sys_resource
- id	name
  1		查询权限
  2		添加权限
  3		删除权限

sys_role_resource
- role_id	resource_id
  1			1
  1			2
  1			3
  2			1
  2			2
  3			1


sys_user_role
- user_id	role_id
  1			3
  2			2
  3			1
  
# 虽然表比较多，但是维护好其余的表，使得新添加user后,只需要维护user与role的关系
```

[Shiro](https://so.csdn.net/so/search?q=Shiro&spm=1001.2101.3001.7020)
----------------------------------------------------------------------

[Shiro](https://shiro.apache.org/) 是 apache 推出的安全管理框架

*   比 SpringSecurity 更加简单易用
*   官方手册：[http://shiro.apache.org/reference.html](http://shiro.apache.org/reference.html)

Shiro 的 2 大核心功能：

*   **认证**：比如登陆认证，只有合法用户才能登陆进入系统  
    ![](https://img-blog.csdnimg.cn/08f58fdfb2774f0a99d4ebaf65545233.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)
*   **授权**：比如设置每个合法用户的权限范围，可以对哪些资源进行操作（C？R？U？D？）  
    ![](https://img-blog.csdnimg.cn/069876065bcb4ef2b5eff898af6dbcce.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_20,color_FFFFFF,t_70,g_se,x_16)

核心类型：

*   **SecurityManager**：安全管理器，Shiro 最核心的类型之一
*   **Subject**：需要进行认证和授权的主体，比如用户
*   **Authenticator**：认证器
*   **Authorizer**：授权器
*   **Realm**：相当于数据源，可以用于获取主体的权限信息

> 数据库可以作为 Realm，.ini 文件可以作为 Realm 等…

![](../../03-%E5%9B%BE%E7%89%87/typora/77df2cfa7347459d8d5ceae3993b15d3.png)

### 入门项目

项目源码：[https://gitee.com/szluyu99/mj_java_frame/tree/master/05_Project/TestShiro](https://gitee.com/szluyu99/mj_java_frame/tree/master/05_Project/TestShiro)

```xml
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring-boot-web-starter</artifactId>
    <version>1.7.0</version>
</dependency>

<!-- 可以根据自身需要选择SLF4J的实现 -->
<dependency>
    <groupId>ch.qos.logback</groupId>
    <artifactId>logback-classic</artifactId>
    <version>1.2.3</version>
</dependency>
```

![](https://img-blog.csdnimg.cn/05fd09f359ef4fc1913bb7c04e99d75e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6JCM5a6F6bm_,size_1,color_FFFFFF,t_70,g_se,x_16)  
简单认证代码：

```java
// 安全管理器: DefaultSecurityManager是SecurityManager的实现类型
DefaultSecurityManager mgr = new DefaultSecurityManager();
// 告知SecurityUtils使用哪个安全管理器
SecurityUtils.setSecurityManager(mgr);
// 设置Realm
mgr.setRealm(new IniRealm("classpath:realm.ini"));

// 主体: Subject
Subject subject = SecurityUtils.getSubject();

// 登录
String username = "mj";
String password = "123456";
UsernamePasswordToken token = new UsernamePasswordToken(username, password);

try {
	// false
    System.out.println("【登录之前】isAuthenticated -> " + subject.isAuthenticated());

    subject.login(token);
	
	// true
    System.out.println("【登录之后】isAuthenticated -> " + subject.isAuthenticated());
	
    System.out.println("【权限】user:read -> " + subject.isPermitted("user:read"));
    System.out.println("【权限】user:update -> " + subject.isPermitted("user:update"));
    System.out.println("【权限】user:delete -> " + subject.isPermitted("user:delete"));
    System.out.println("【权限】user:create -> " + subject.isPermitted("user:create"));
    System.out.println("【角色】admin -> " + subject.hasRole("admin"));
    System.out.println("【角色】normal -> " + subject.hasRole("normal"));

    // 退出登录
    subject.logout();

    System.out.println("【退出登录】isAuthenticated -> " + subject.isAuthenticated());
    System.out.println("【角色】normal -> " + subject.hasRole("normal"));
} catch (UnknownAccountException e) {
    System.out.println("用户名不存在");
} catch (IncorrectCredentialsException e) {
    System.out.println("密码不正确");
} catch (AuthenticationException e) {
    System.out.println("认证失败");
}
```

### SpringBoot 集成 Shiro

### Shiro 执行流程

*   TokenFilter - isAccessAllowed
*   TokenFIlter - onAccessDenied
*   TokenRealm - supports
*   TokenRealm - doGetAuthenticationInfo 认证
*   TokenMatcher- doCredentialsMatch
*   TokenRealm - doGetAuthorizationInfo 授权

项目打包部署
------

打包方式：jar 包、war 包

不管打包成哪种包，都需要添加以下依赖：

```xml
<build>
	<plugins>
		<plugin>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-maven-plugin</artifactId>
		</plugin>
	</plugins>
</build>
```

### 打包部署 - jar

当采取 jar 包方式部署时，设置 packing 为 jar（默认就是 jar）

```xml
<packaging>jar</packaging>
```

如何局部覆盖 jar 包中的配置内容？

*   在优先级更高的位置存放新的配置文件

> 优先级参考博客 [SpringBoot - 配置文件](https://blog.csdn.net/weixin_43734095/article/details/119578806#_115)

*   命令行启动项目时 `java -jar --spring.config.location` 指定

### 打包部署 - war

设置 packing 为 war

```xml
<packaging>war</packaging>
```

添加 servlet 依赖，排除 SpringBoot 内置的 tomcat：

```xml
<dependency>
	<groupId>javax.servlet</groupId>
	<artifactId>javax.servlet-api</artifactId>
</dependency>

<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
	<exclusion>
		<exclusion>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-tomcat</artifactId>
		</exclusion>
	</exclusion>
</dependency>
```

修改入口类：

```java
@SpringBootApplication
public class JiaKaoApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(JiaKaoApplication.class, args);
    }

	@Override
	protected SpringApplication Builder configure(SpringApplicationBuilder builder) {
		return builder.sources(Application.class);
	}
}
```

