# 容器化与DevOps
## 1、容器化-Docker精髓
### 1、容器化演进之路，为什么是Docker？
### 2、Docker环境搭建、架构解析
### 3、Docker概念理解-容器、镜像、仓库操作实战
### 4、Docker集群化部署MySQL、Redis、RabbitMQ等实战
### 5、掌握核心-如何使用Dockerfile解耦应用和部署
### 5、Docker容器网络与通信原理、Veth-Pair深度解析
### 6、打通Docker持久化-容器卷机制
### 7、大规模服务编排-Docker Compose实战
### 8、集群化-Docker Swarm
### 9、容器编排第一代思想-Rancher集群化编排实战
### 10、Docker构建企业Jenkins CI平台
## 2、容器编排-Kubernetes
### 1、Docker、K8S、Rancher都是什么关系？K8S的背景、组件
### 2、k8s基础概念-Pod，网络通讯机制
### 3、生产环境搭建k8s集群
### 4、k8s自动扩缩容、容灾、负载均衡实战演练
### 5、k8s内功心法-资源清单、控制器、RS、Deployment、DaemonSet、Job、CronJob详解
### 6、k8s内功心法-集群Service、Ingress、配置集ConfigMap、Secret秘钥技术
### 7、k8s内功心法-Volume、PV、PVC
### 8、k8s集群原理-集群调度原理、污点和容忍、认证和鉴权
### 10、Helm使用&部署Dashboard控制面板
### 11、集成Prometheus+Grafana系统监控平台
### 12、集成EFK日志系统
### 13、实战-k8s部署分布式ocp能力开放管理平台
## 3、构建企业CI/CD
### 1、CI/CD技术栈分析
### 2、Jenkins环境配置整合
### 3、Jenkins-Pipeline和Blue Ocean&多分支流水线
### 4、Jenkins中集成Kubernetes
### 5、整合Sonar代码审查，规范化代码开发
### 6、案例一：Git代码提交触发+Maven构建+代码扫描+邮件通知
### 7、案例二：Git源码下拉+参数化构建+多环境部署
### 8、Rancher打通大规模容器编排技能栈
### 9、KubeSphere新秀崛起：平台监控、灰度发布、可视化CICD
### 10、展望下一代Service Mesh